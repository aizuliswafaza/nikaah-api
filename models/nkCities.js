const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_cities', {
    city_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    prov_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    kemendagri_code: {
      type: DataTypes.STRING(13),
      allowNull: true
    },
    city_name: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'nk_cities',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "city_id" },
        ]
      },
      {
        name: "prov_id",
        using: "BTREE",
        fields: [
          { name: "prov_id" },
        ]
      },
    ]
  });
};
