const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_section_custom', {
    sccm_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    order_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'nk_order',
        key: 'order_id'
      }
    },
    sccm_type: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    sccm_title: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    sccm_description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    sccm_is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    sccm_icon: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    sccm_url: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    sccm_button_name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    sccm_metadata: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    sccm_created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    sccm_updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'nk_section_custom',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "sccm_id" },
        ]
      },
      {
        name: "FK_SCCM_ORDER_ID",
        using: "BTREE",
        fields: [
          { name: "order_id" },
        ]
      },
    ]
  });
};
