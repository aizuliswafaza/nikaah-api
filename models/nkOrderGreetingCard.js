const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_order_greeting_card', {
    ogc_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    order_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    ogc_author: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    ogc_is_attend: {
      type: DataTypes.BOOLEAN,
      allowNull: true
    },
    ogc_message: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    ogc_created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    ogc_deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'nk_order_greeting_card',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ogc_id" },
        ]
      },
    ]
  });
};
