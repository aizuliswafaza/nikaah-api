const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_order_bride', {
    order_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'nk_order',
        key: 'order_id'
      }
    },
    ob_type: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    ob_nickname: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ob_fullname: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ob_father_name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ob_mother_name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ob_birth_order: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ob_order: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    ob_description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ob_instagram_url: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ob_instagram_username: {
      type: DataTypes.STRING(100),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'nk_order_bride',
    timestamps: false,
    indexes: [
      {
        name: "oc_order_id",
        using: "BTREE",
        fields: [
          { name: "order_id" },
        ]
      },
    ]
  });
};
