const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_payments', {
    pay_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    pay_approved_by: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    pay_code: {
      type: DataTypes.STRING(150),
      allowNull: false,
      unique: "pay_code"
    },
    pay_method: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    pay_type: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    pay_status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    pay_price: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    pay_discount: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    pay_total: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    pay_receipt_url: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    pay_approved_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    pay_paid_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    pay_created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    pay_updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'nk_payments',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "pay_id" },
        ]
      },
      {
        name: "pay_code",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "pay_code" },
        ]
      },
      {
        name: "user_id",
        using: "BTREE",
        fields: [
          { name: "user_id" },
        ]
      },
      {
        name: "pay_approved_by",
        using: "BTREE",
        fields: [
          { name: "pay_approved_by" },
        ]
      },
    ]
  });
};
