const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_users', {
    user_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    user_fullname: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    user_email: {
      type: DataTypes.STRING(150),
      allowNull: false,
      unique: "UNIQUE_USER_EMAIL"
    },
    user_phone: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    user_status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0
    },
    user_source: {
      type: DataTypes.TINYINT,
      allowNull: true
    },
    user_source_other: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    city_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    prov_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    user_password: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    user_onboarding_state: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    user_created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    user_updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    user_verified_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'nk_users',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "user_id" },
        ]
      },
      {
        name: "UNIQUE_USER_EMAIL",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "user_email" },
        ]
      },
      {
        name: "city_id",
        using: "BTREE",
        fields: [
          { name: "city_id" },
        ]
      },
      {
        name: "prov_id",
        using: "BTREE",
        fields: [
          { name: "prov_id" },
        ]
      },
    ]
  });
};
