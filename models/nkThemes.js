const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_themes', {
    themes_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    themes_code: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    themes_name: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    themes_created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    themes_updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    themes_deleted_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'nk_themes',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "themes_id" },
        ]
      },
    ]
  });
};
