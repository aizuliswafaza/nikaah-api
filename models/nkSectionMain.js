const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_section_main', {
    order_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    scm_type: {
      type: DataTypes.STRING(100),
      allowNull: false,
      primaryKey: true
    },
    scm_title: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    scm_sub_title: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    scm_description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    scm_sub_description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    scm_total_items: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    scm_is_active: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    },
    scm_datetime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    scm_metadata: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    scm_order: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    scm_created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    scm_updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'nk_section_main',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "order_id" },
          { name: "scm_type" },
        ]
      },
    ]
  });
};
