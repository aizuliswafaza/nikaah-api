const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_musics', {
    music_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    music_title: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    music_url: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    music_created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    music_updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'nk_musics',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "music_id" },
        ]
      },
    ]
  });
};
