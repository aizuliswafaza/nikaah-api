const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_order_detail', {
    order_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'nk_order',
        key: 'order_id'
      }
    },
    od_location_url: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    od_calendar_url: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    od_stream_url: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    od_audio_url: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    od_audio_title: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    od_youtube_url: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'nk_order_detail',
    timestamps: false,
    indexes: [
      {
        name: "OD_ORDER_ID",
        using: "BTREE",
        fields: [
          { name: "order_id" },
        ]
      },
    ]
  });
};
