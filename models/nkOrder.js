const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_order', {
    order_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'nk_users',
        key: 'user_id'
      },
      unique: "FK_ORDER_USER_ID"
    },
    order_code: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    order_public_id: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    order_package: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    order_theme: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    order_theme_color: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    order_name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    order_religion: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    order_wear_hijab: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
      defaultValue: 1
    },
    order_cp: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    order_email: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    order_status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    order_payment_status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    order_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    order_expired_date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    order_sub_domain: {
      type: DataTypes.STRING(150),
      allowNull: true,
      unique: "order_sub_domain"
    },
    order_main_caption: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    order_bride_caption: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    order_event_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    order_metadata: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    order_created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    order_updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    order_deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'nk_order',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "order_id" },
        ]
      },
      {
        name: "order_sub_domain",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "order_sub_domain" },
        ]
      },
      {
        name: "user_id_2",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "user_id" },
        ]
      },
      {
        name: "ORDER_PUBLIC_ID",
        using: "BTREE",
        fields: [
          { name: "order_public_id" },
        ]
      },
      {
        name: "user_id",
        using: "BTREE",
        fields: [
          { name: "user_id" },
        ]
      },
    ]
  });
};
