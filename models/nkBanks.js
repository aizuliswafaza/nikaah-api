const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_banks', {
    bank_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    bank_type: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    bank_name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    bank_status: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: 0
    },
    bank_icon_url: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    bank_config: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    bank_created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    bank_updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'nk_banks',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "bank_id" },
        ]
      },
      {
        name: "bank_type",
        using: "BTREE",
        fields: [
          { name: "bank_type" },
        ]
      },
      {
        name: "bank_status",
        using: "BTREE",
        fields: [
          { name: "bank_status" },
        ]
      },
    ]
  });
};
