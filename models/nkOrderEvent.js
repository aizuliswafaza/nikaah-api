const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_order_event', {
    oe_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    order_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'nk_order',
        key: 'order_id'
      }
    },
    oe_name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    oe_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    oe_dayname: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    oe_day: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    oe_month: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    oe_year: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    oe_start_time: {
      type: DataTypes.TIME,
      allowNull: true
    },
    oe_end_time: {
      type: DataTypes.TIME,
      allowNull: true
    },
    oe_timezone: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    oe_timezone_local: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    oe_time: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    oe_location: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    oe_location_url: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    oe_address: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    oe_monthname: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    oe_order: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'nk_order_event',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "oe_id" },
        ]
      },
      {
        name: "oe_order_id",
        using: "BTREE",
        fields: [
          { name: "order_id" },
        ]
      },
    ]
  });
};
