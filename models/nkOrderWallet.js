const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_order_wallet', {
    ow_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    order_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'nk_order',
        key: 'order_id'
      }
    },
    ow_bank_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'nk_banks',
        key: 'bank_id'
      }
    },
    ow_account_number: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ow_qr_code: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ow_account_name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    ow_order: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    ow_created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    ow_updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'nk_order_wallet',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ow_id" },
        ]
      },
      {
        name: "OW_ORDER_ID",
        using: "BTREE",
        fields: [
          { name: "order_id" },
        ]
      },
      {
        name: "FK_OW_BANK_ID",
        using: "BTREE",
        fields: [
          { name: "ow_bank_id" },
        ]
      },
    ]
  });
};
