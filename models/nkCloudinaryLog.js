const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_cloudinary_log', {
    cl_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    cl_payload: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    cl_error_webhook: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    cl_created_at: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.fn('current_timestamp')
    },
    cl_resend_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'nk_cloudinary_log',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "cl_id" },
        ]
      },
    ]
  });
};
