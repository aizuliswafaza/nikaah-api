const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_order_gallery', {
    og_id: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    order_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'nk_order',
        key: 'order_id'
      }
    },
    og_width: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    og_height: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    og_section: {
      type: DataTypes.STRING(150),
      allowNull: false
    },
    og_filename: {
      type: DataTypes.STRING(150),
      allowNull: true
    },
    og_url: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    og_url_cloudinary: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'nk_order_gallery',
    timestamps: false,
    indexes: [
      {
        name: "FK_OG_ORDER_ID",
        using: "BTREE",
        fields: [
          { name: "order_id" },
        ]
      },
      {
        name: "og_id",
        using: "BTREE",
        fields: [
          { name: "og_id" },
        ]
      },
    ]
  });
};
