const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_order_url', {
    ou_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    order_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'nk_order',
        key: 'order_id'
      }
    },
    ou_type: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    ou_url: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ou_config: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ou_created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    ou_updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'nk_order_url',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ou_id" },
        ]
      },
      {
        name: "FK_OU_ORDER_ID",
        using: "BTREE",
        fields: [
          { name: "order_id" },
        ]
      },
    ]
  });
};
