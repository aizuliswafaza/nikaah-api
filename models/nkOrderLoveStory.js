const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_order_love_story', {
    ols_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    order_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'nk_order',
        key: 'order_id'
      }
    },
    ols_title: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ols_image_url: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    ols_description: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    ols_order: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    ols_created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    ols_updated_at: {
      type: DataTypes.DATE,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'nk_order_love_story',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "ols_id" },
        ]
      },
      {
        name: "FK_OLS_ORDER_ID",
        using: "BTREE",
        fields: [
          { name: "order_id" },
        ]
      },
    ]
  });
};
