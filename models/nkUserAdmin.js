const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_user_admin', {
    user_admin_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    user_admin_email: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    user_admin_password: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    user_admin_status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'nk_user_admin',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "user_admin_id" },
        ]
      },
    ]
  });
};
