const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('nk_user_verification', {
    uvrf_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'nk_users',
        key: 'user_id'
      }
    },
    uvrf_type: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    uvrf_token: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    uvrf_status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    uvrf_created_at: {
      type: DataTypes.DATE,
      allowNull: false
    },
    uvrf_used_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    uvrf_expired_at: {
      type: DataTypes.DATE,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'nk_user_verification',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "uvrf_id" },
        ]
      },
      {
        name: "uvrf_token",
        using: "BTREE",
        fields: [
          { name: "uvrf_token" },
        ]
      },
      {
        name: "order_id",
        using: "BTREE",
        fields: [
          { name: "user_id" },
        ]
      },
    ]
  });
};
