var DataTypes = require("sequelize").DataTypes;
var _nk_banks = require("./nkBanks");
var _nk_cities = require("./nkCities");
var _nk_cloudinary_log = require("./nkCloudinaryLog");
var _nk_musics = require("./nkMusics");
var _nk_order = require("./nkOrder");
var _nk_order_bride = require("./nkOrderBride");
var _nk_order_detail = require("./nkOrderDetail");
var _nk_order_event = require("./nkOrderEvent");
var _nk_order_gallery = require("./nkOrderGallery");
var _nk_order_greeting_card = require("./nkOrderGreetingCard");
var _nk_order_love_story = require("./nkOrderLoveStory");
var _nk_order_url = require("./nkOrderUrl");
var _nk_order_wallet = require("./nkOrderWallet");
var _nk_payments = require("./nkPayments");
var _nk_provinces = require("./nkProvinces");
var _nk_section_custom = require("./nkSectionCustom");
var _nk_section_main = require("./nkSectionMain");
var _nk_themes = require("./nkThemes");
var _nk_user_admin = require("./nkUserAdmin");
var _nk_user_verification = require("./nkUserVerification");
var _nk_users = require("./nkUsers");

function initModels(sequelize) {
  var nk_banks = _nk_banks(sequelize, DataTypes);
  var nk_cities = _nk_cities(sequelize, DataTypes);
  var nk_cloudinary_log = _nk_cloudinary_log(sequelize, DataTypes);
  var nk_musics = _nk_musics(sequelize, DataTypes);
  var nk_order = _nk_order(sequelize, DataTypes);
  var nk_order_bride = _nk_order_bride(sequelize, DataTypes);
  var nk_order_detail = _nk_order_detail(sequelize, DataTypes);
  var nk_order_event = _nk_order_event(sequelize, DataTypes);
  var nk_order_gallery = _nk_order_gallery(sequelize, DataTypes);
  var nk_order_greeting_card = _nk_order_greeting_card(sequelize, DataTypes);
  var nk_order_love_story = _nk_order_love_story(sequelize, DataTypes);
  var nk_order_url = _nk_order_url(sequelize, DataTypes);
  var nk_order_wallet = _nk_order_wallet(sequelize, DataTypes);
  var nk_payments = _nk_payments(sequelize, DataTypes);
  var nk_provinces = _nk_provinces(sequelize, DataTypes);
  var nk_section_custom = _nk_section_custom(sequelize, DataTypes);
  var nk_section_main = _nk_section_main(sequelize, DataTypes);
  var nk_themes = _nk_themes(sequelize, DataTypes);
  var nk_user_admin = _nk_user_admin(sequelize, DataTypes);
  var nk_user_verification = _nk_user_verification(sequelize, DataTypes);
  var nk_users = _nk_users(sequelize, DataTypes);

  nk_order_wallet.belongsTo(nk_banks, { as: "ow_bank", foreignKey: "ow_bank_id"});
  nk_banks.hasMany(nk_order_wallet, { as: "nk_order_wallets", foreignKey: "ow_bank_id"});
  nk_order_bride.belongsTo(nk_order, { as: "order", foreignKey: "order_id"});
  nk_order.hasMany(nk_order_bride, { as: "nk_order_brides", foreignKey: "order_id"});
  nk_order_detail.belongsTo(nk_order, { as: "order", foreignKey: "order_id"});
  nk_order.hasMany(nk_order_detail, { as: "nk_order_details", foreignKey: "order_id"});
  nk_order_event.belongsTo(nk_order, { as: "order", foreignKey: "order_id"});
  nk_order.hasMany(nk_order_event, { as: "nk_order_events", foreignKey: "order_id"});
  nk_order_gallery.belongsTo(nk_order, { as: "order", foreignKey: "order_id"});
  nk_order.hasMany(nk_order_gallery, { as: "nk_order_galleries", foreignKey: "order_id"});
  nk_order_love_story.belongsTo(nk_order, { as: "order", foreignKey: "order_id"});
  nk_order.hasMany(nk_order_love_story, { as: "nk_order_love_stories", foreignKey: "order_id"});
  nk_order_url.belongsTo(nk_order, { as: "order", foreignKey: "order_id"});
  nk_order.hasMany(nk_order_url, { as: "nk_order_urls", foreignKey: "order_id"});
  nk_order_wallet.belongsTo(nk_order, { as: "order", foreignKey: "order_id"});
  nk_order.hasMany(nk_order_wallet, { as: "nk_order_wallets", foreignKey: "order_id"});
  nk_section_custom.belongsTo(nk_order, { as: "order", foreignKey: "order_id"});
  nk_order.hasMany(nk_section_custom, { as: "nk_section_customs", foreignKey: "order_id"});
  nk_order.belongsTo(nk_users, { as: "user", foreignKey: "user_id"});
  nk_users.hasOne(nk_order, { as: "nk_order", foreignKey: "user_id"});
  nk_user_verification.belongsTo(nk_users, { as: "user", foreignKey: "user_id"});
  nk_users.hasMany(nk_user_verification, { as: "nk_user_verifications", foreignKey: "user_id"});

  return {
    nk_banks,
    nk_cities,
    nk_cloudinary_log,
    nk_musics,
    nk_order,
    nk_order_bride,
    nk_order_detail,
    nk_order_event,
    nk_order_gallery,
    nk_order_greeting_card,
    nk_order_love_story,
    nk_order_url,
    nk_order_wallet,
    nk_payments,
    nk_provinces,
    nk_section_custom,
    nk_section_main,
    nk_themes,
    nk_user_admin,
    nk_user_verification,
    nk_users,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
