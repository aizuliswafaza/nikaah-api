# RELEASE V 0.0.5

---
What's new??
1. Middleware Login
2. Login with JWT Token
3. Update param wallet (bank_name => bank_id)
4. API Get List Order for Admin
---

## Auth User /auth/login
[Todo] Authentication for user
[Method] POST
Request:
```bash
{
	"username": "e9f4c6ee-9569-4326-8907-1bbb222f1c44",
	"password": "pika-char"
}
```
Note:
- username is orderId
- password is sub-domain

Response:
```bash
{
  "message": "Berhasil login",
  "data": {
    "type": "Bearer",
    "issued_at": 1627876157,
    "jti": "e9f4c6ee-9569-4326-8907-1bbb222f1c44365aa7dc-fd29-4e58-8f40-ecd426df196c",
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2Mjc4NzYxNTcsImp0aSI6ImU5ZjRjNmVlLTk1NjktNDMyNi04OTA3LTFiYmIyMjJmMWM0NDM2NWFhN2RjLWZkMjktNGU1OC04ZjQwLWVjZDQyNmRmMTk2YyIsImlzcyI6Imh0dHBzOi8vbmlrYWFoLmlkIiwiYXVkIjoiaHR0cHM6Ly9uaWthYWgtYXBpLmhlcm9rdWFwcC5jb20iLCJ1c2VyIjp7ImlkIjoiZTlmNGM2ZWUtOTU2OS00MzI2LTg5MDctMWJiYjIyMmYxYzQ0In0sImV4cCI6MTYyNzg5MDU1NywidHlwZSI6ImFjY2Vzc190b2tlbiJ9.pL63NTRS359OA2zkJBGwE9wP3HwHrCQTcSfP6G7slzs",
    "access_token_expired_at": 1627890557
  }
}
```

## Auth Admin /auth/admin/login
[Todo] Authentication for admin
[Method] POST
Request:
```bash
{
	"username": "admin@nikaah.id",
	"password": "123456"
}
```
Note:
- static username and password

Response:
```bash
{
  "message": "Berhasil login sebagai admin",
  "data": {
    "type": "Bearer",
    "issued_at": 1627875507,
    "jti": "3929d828-1778-4753-a716-e01c40bd68c46402ed6d-6d22-48d4-b9a5-e2af4170c732",
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2Mjc4NzU1MDcsImp0aSI6IjM5MjlkODI4LTE3NzgtNDc1My1hNzE2LWUwMWM0MGJkNjhjNDY0MDJlZDZkLTZkMjItNDhkNC1iOWE1LWUyYWY0MTcwYzczMiIsImlzcyI6Imh0dHBzOi8vbmlrYWFoLmlkIiwiYXVkIjoiaHR0cHM6Ly9uaWthYWgtYXBpLmhlcm9rdWFwcC5jb20iLCJ1c2VyIjp7ImlkIjoiMzkyOWQ4MjgtMTc3OC00NzUzLWE3MTYtZTAxYzQwYmQ2OGM0In0sImV4cCI6MTYyNzg4OTkwNywidHlwZSI6ImFjY2Vzc190b2tlbiJ9.HCO4EA6CTQily9jT-lpwMe2fv11dhELZQet8ZofC8-A",
    "access_token_expired_at": 1627889907
  }
}
```

## Auth Admin /order/list
[Todo] Get List Order
[Method] GET

Response:
```bash
{
  "message": "Berhasil mendapatkan daftar pesanan",
  "data": {
    "count": 7,
    "rows": [
      {
        "id": "e9f4c6ee-9569-4326-8907-1bbb222f1c44",
        "sub_domain": "pika-char1",
        "package": 1,
        "phone_number": "6281230080114",
        "email": "pikachu@pokemon.com",
        "event_date": "2021-07-09 00:00:00"
      },
      {
        "id": "b5065afd-6476-4cc0-9322-9b18d1352ed7",
        "sub_domain": "pika-char2",
        "package": 1,
        "phone_number": "6281230080114",
        "email": "pikachu@pokemon.com",
        "event_date": "2021-07-09 00:00:00"
      }
    ]
  }
}
```

## Register Order /order/check-domain-availability
[Todo] Check Sub Domain Availability (Optional)
[Method] POST
Request:
```bash
{
	"sub_domain": "pika-chu"
}
```
Note:
- optional ya, soalnya di /order/register sudah ada juga validasinya

## Register Order /order/register
[Todo] Only for Register Order User
[Method] POST
Request:
```bash
{
	"package": 1,
	"theme": 1,
	"name": "PIKACHU",
	"phone_number": "6281230080114",
	"email": "pikachu@pokemon.com",
	"event_date": "2021-07-09 00:00:00",
	"sub_domain": "pika-char",
	"main_caption": "",
	"bride_caption": "",
	"metadata": {}
}
```
Note:
- package: belum disesuaikan bisa dari WEB mau berapa aja angkanya (sementara hindari 0)
- theme: belum disesuaikan bisa dari WEB mau berapa aja angkanya (sementara hindari 0)
- sub_domain: bisa diisikan setelah atau sebelum memanggil /order/check-domain-availability
- phone_number: hindari angka `0` didepannya
- religion: hindari angka `0` didepannya, religion dimulai dari angka 1 [1, 2, 3 dst.]
- main_caption and bride_caption tipe text

Response
```bash
{
  "message": "Berhasil membuat pesanan undangan, kami akan segera menghubungi anda",
  "data": {
    "orderId": 4
  }
}
```

## Add data bride /order/add-wedding-detail
[Todo] Menambahkan detail data pemesan
[Method] POST
Request:
```bash
{
	"order_id": 4,
	"bride": {
		"name": "PIKACHU",
		"fullname": "PIKA PIKA CHUUUUUU",
		"father_name": "Doctor PI",
		"mother_name": "Doctor KA",
		"birth_order": "Pertama"
	},
	"groom": {
		"name": "CHAR",
		"fullname": "CHARIZARD",
		"father_name": "CHARIMAN",
		"mother_name": "IZAWOMAN",
		"birth_order": "Kedua"
	},
	"details": {
		"location_url": null,
		"calendar_url": null,
		"stream_url": null
	},
	"events": [
		{
			"name": "Akad",
			"date": "2021-07-09 00:00:00",
			"time": "19.00-20.00 WIB",
			"location": "unknown",
			"location_url": "url",
			"address": "unknown",
			"slot": 1
		}
	],
	"wallets": [
		{
			"bank_id": 1,
			"account_name": "PT PERTAMINI",
			"account_number": "6281230080114",
			"slot": 1
		}
	],
	"gallery": [
		{
			"section": "banner",
			"url": "check1"
		}
	]
}
```
NOTE:
- Bisa digunakan untuk menambahkan dan update data pemesan
- slot diperuntukan untuk nomor urutan (eg. ada 2 data walltets maka slot nanti ada 1 dan 2)
- account_number: hindari angka `0` didepannya 

Response
```bash
{
  "message": "Berhasil menambahkan detail informasi undangan"
}
```

## Get Order /order/details/:orderId
[Todo] Mendapatkan Informasi Undangan
[Method] GET

Response
```bash
{
  "message": "Berhasil mendapatkan data detail",
  "data": {
    "id": 5,
    "package": 1,
    "theme": 1,
    "phone_number": "6281230080114",
    "religion": 0,
    "email": "pipi@pokemon.com",
    "event_date": "2021-07-09 00:00:00",
    "main_caption": null,
    "bride_caption": null,
    "bride": {},
    "groom": {},
    "events": [],
    "wallets": [],
    "gallery": [],
    "metadata": null
  }
}
```

## Get Order /info/:sub_domain
[Todo] Mendapatkan Informasi Undangan
[Method] GET

Response
```bash
{
  "message": "Berhasil mendapatkan data pesanan",
  "data": {
    "id": 4,
    "package": 0,
    "theme": 1,
    "phone_number": "6281230080114",
    "email": "pikachu@pokemon.com",
    "event_date": "2021-07-09 00:00:00",
    "main_caption": null,
    "bride_caption": null,
    "bride": {
      "name": "PIKACHU",
      "fullname": "PIKA PIKA CHUUUUUU",
      "father_name": "Doctor PI",
      "mother_name": "Doctor KA"
    },
    "groom": {
      "name": "CHAR",
      "fullname": "CHARIZARD",
      "father_name": "CHARIMAN",
      "mother_name": "IZAWOMAN"
    },
    "events": [
      {
        "name": "Akad",
        "date": "2021-07-09 00:00:00",
        "dayname": "Jum'at",
        "monthname": "Juli",
        "time": "19.00-20.00 WIB",
        "location": "unknown",
        "location_url": null,
        "address": "unknown",
        "slot": 1
      }
    ],
    "wallets": [
      {
        "bank_id": 1,
        "account_number": "6281230080114",
        "account_name": "PT PERTAMINI",
        "slot": 1
      }
    ],
    "gallery": [
      {
        "section": "banner",
        "url": "check1"
      }
    ],
    "metadata": null
  }
}
```
