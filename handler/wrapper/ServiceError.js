'use strict'

class ServiceError extends Error {
    constructor (errorType, { meta } = {}) {
        super(errorType)
        this.errorType = errorType
        this.metadata = meta
    }
}

module.exports = ServiceError