'use strict'

const vsprintf = require('sprintf-js').vsprintf
const ErrorConstans = require('../../../config/constants').ERROR

function getMessage (type) {
  return ErrorConstans[type]
}

function composeMessage (type, args) {
  const message = getMessage(type)
  if (!message) {
    const messageNotDefined = 'Error message not defined (' + type + ')'
    return messageNotDefined
  }

  return vsprintf(message, args)
}

module.exports = function (type, { args = [], data }) {
  return {
    status: 422,
    message: composeMessage(type, args),
    type,
    data
  }
}
