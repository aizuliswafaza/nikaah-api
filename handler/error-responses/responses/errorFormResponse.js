'use strict'

const ErrorConstans = require('../../../config/constants').ERROR

module.exports = function (type, errors, { message = ErrorConstans.default_form_error }) {
  return {
    status: 422,
    errors: errors,
    message: message,
    type: type
  }
}
