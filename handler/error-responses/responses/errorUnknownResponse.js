'use strict'

module.exports = function (error) {
  console.trace('[x] Error::middleware::handler::unknown_error', error)

  return {
    status: 500,
    message: 'Something wrong, please try again',
    type: 'unknown_error'
  }
}
