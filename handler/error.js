'use strict'

const { ERROR: ErrorConstant } = require('../config/constants')

const { getErrorResponse } = require('./error-responses')

module.exports = app => {
    app.use((req, res, next) => {
        return res.status(404).json({
            message: 'What are you looking for?'
        })
    })

    app.use((err, req, res, next) => {
        console.log(err)
        const response = getErrorResponse(err)
        const httpStatus = response.status

        delete response.status

        return res.status(httpStatus).json(response)
    })
}