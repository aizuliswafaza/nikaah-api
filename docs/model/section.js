'use strict'

const generalData = {
  title: 'string',
  is_active: 'boolean',
  description: 'string',
}

const generalDataWithId = {
  id: 'number',
  title: 'string',
  is_active: 'boolean',
  description: 'string',
}

module.exports = {
  SectionDefault: generalData,
  SectionDefaultWithId: generalDataWithId,
  SectionHomepage: {
    ...generalData,
    sub_title: 'string',
    datetime: 'datetime',
    padding_top: 'number',
    padding_bottom: 'number'
  },
  SectionGreeting: {
    ...generalData,
    total_sessions: 'number'
  },
  SectionCountdown: {
    ...generalData,
    datetime: 'datetime'
  },
  SectionEvent: {
    ...generalData,
    total_events: 'number'
  },
  SectionGallery: {
    ...generalData,
    total_images: 'number'
  },
  SectionFelicitation: {
    ...generalData,
    show_result: 'boolean',
    show_confirmation: 'boolean'
  },
  SectionBride: {
    ...generalData,
    reverse_order: 'boolean',
    hide_image: 'boolean'
  }
}
