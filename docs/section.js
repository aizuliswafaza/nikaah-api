'use strict'

const { SectionModel } = require('./model')

module.exports = {
  ...SectionModel,
  SectionHomepageGet: {
    message: 'string',
    data: { $ref: '#/definitions/SectionHomepage' }
  },
  SectionGreetingGet: {
    message: 'string',
    data: { $ref: '#/definitions/SectionGreeting' }
  },
  SectionCountdownGet: {
    message: 'string',
    data: { $ref: '#/definitions/SectionCountdown' }
  },
  SectionEventGet: {
    message: 'string',
    data: { $ref: '#/definitions/SectionEvent' }
  },
  SectionGalleryGet: {
    message: 'string',
    data: { $ref: '#/definitions/SectionGallery' }
  },
  SectionFelicitationGet: {
    message: 'string',
    data: { $ref: '#/definitions/SectionFelicitation' }
  },
  SectionBrideGet: {
    message: 'string',
    data: { $ref: '#/definitions/SectionBride' }
  },
  SectionDefaultGet: {
    message: 'string',
    data: { $ref: '#/definitions/SectionDefault' }
  },
  SectionCustomGet: {
    message: 'string',
    data: [{ $ref: '#/definitions/SectionDefaultWithId' }]
  },
  SectionHomepageSubmit: {
    message: 'string',
    data: { $ref: '#/definitions/SectionHomepage' }
  },
  SectionGreetingSubmit: {
    message: 'string',
    data: { $ref: '#/definitions/SectionGreeting' }
  },
  SectionCountdownSubmit: {
    message: 'string',
    data: { $ref: '#/definitions/SectionCountdown' }
  },
  SectionEventSubmit: {
    message: 'string',
    data: { $ref: '#/definitions/SectionEvent' }
  },
  SectionGallerySubmit: {
    message: 'string',
    data: { $ref: '#/definitions/SectionGallery' }
  },
  SectionFelicitationSubmit: {
    message: 'string',
    data: { $ref: '#/definitions/SectionFelicitation' }
  },
  SectionBrideSubmit: {
    message: 'string',
    data: { $ref: '#/definitions/SectionBride' }
  },
  SectionDefaultSubmit: {
    message: 'string',
    data: { $ref: '#/definitions/SectionDefault' }
  }
}
