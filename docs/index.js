'use strict'

const { env, based_url: basedUrl } = require('../config/constants')
const SectionDocs = require('./section')

const swaggerAutogen = require('swagger-autogen')()

const outputFile = './docs/swagger-output.json'
const endpointsFiles = ['./app.js']
const doc = {
  info: {
    version: "1.0.0",
    title: "Nikaah API",
    description: "Documentation automatically generated by the <b>swagger-autogen</b> module."
  },
  host: basedUrl[env],
  basePath: "/",
  schemes: ['http', 'https'],
  consumes: ['application/json'],
  produces: ['application/json'],
  tags: [
    {
      "name": "Tools",
      "description": "Client tools API"
    },
    {
      "name": "Order - Registration",
      "description": "Client Complete Registration API"
    },
    {
      "name": "Order - Image Upload",
      "description": "Client Image Upload API"
    },
    {
      "name": "Order - Love Story",
      "description": "Client Love Story API"
    },
    {
      "name": "Order - Background Music",
      "description": "Client Background Music API"
    },
    {
      "name": "Order - Youtube Url",
      "description": "Client Youtube URL API"
    },
    {
      "name": "Order - Digital Wallet",
      "description": "Client Digital Wallet API"
    },
    {
      "name": "User Information",
      "description": "Client information API"
    },
    {
      "name": "Auth Login and Registration",
      "description": "Client Login and Registration API"
    }
  ],
  securityDefinitions: {
    "Bearer": {
      "type": "apiKey",
      "name": "Authorization",
      "in": "header",
      "description": "Enter your bearer token in the format **Bearer &lt;token>**"
    }
  },
  security: [{ "Bearer": [] }],
  definitions: {
    ...SectionDocs,
    DefaultResult: {
      message: 'string',
      data: {}
    },
    ClientProfile: {
      "message": "Berhasil mendapatkan data profil",
      "data": {
        "status": 2,
        "fullname": "Aizul Faiz",
        "email": "aizuliswafaza+7@gmail.com",
        "phone": "6281230080114",
        "source": 0,
        "source_other": "sourceOther",
        "registered_at": "2021-12-05 01:43:34",
        "verified_at": "2021-12-05 02:34:22",
        "order": {
          "order_status": 1,
          "public_id": "e594c765-a641-4612-88f6-62b5409d568f",
          "sub_domain": "riki-melita1",
          "package": 2,
          "theme": 1,
          "theme_color": 1,
          "event_date": "2021-10-16 07:00:00",
          "religion": 1,
          "is_hijab": null,
          "main_caption": "",
          "bride_caption": "<p></p>\r\n",
          "metadata": "metadata",
          "payment_status": 1,
          "expired_date": "2022-03-10",
          "details": {
            "location_url": null,
            "calendar_url": null,
            "stream_url": null,
            "audio_url": "https://tinyurl.com/yzut9fuf",
            "audio_title": "Sezairi - It's You (Official Music Video)",
            "youtube_url": null
          },
          "bride": {},
          "groom": {},
          "events": [
            {
              "name": "Resepsi",
              "date": "2021-09-18 07:00:00",
              "start_time": "08:00",
              "end_time": "10:00",
              "tz": "WIB",
              "time": "08:00 - 10:00 WIB",
              "location": "tsting nama lokasi",
              "location_url": "",
              "address": "manteup",
              "slot": 0,
              "dayname": "Sabtu",
              "monthname": "September"
            }
          ],
          "wallets": [
            {
              "bank_id": 1,
              "account_number": null,
              "account_name": "Chkiiiii",
              "slot": 0
            }
          ],
          "gallery": [
            {
              "filename": null,
              "section": "circle",
              "url": "url",
              "url_signed": "url",
              "width": null,
              "height": null
            }
          ],
          "is_expired": false
        }
      }
    },
    ClientLogin: {
      "message": "Berhasil login",
      "data": {
        "type": "Bearer",
        "issued_at": 1643379807,
        "jti": "14ab1655ff-7b3e-4d9d-b779-3c38016358b3",
        "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NDMzNzk4MDcsImp0aSI6IjE0YWIxNjU1ZmYtN2IzZS00ZDlkLWI3NzktM2MzODAxNjM1OGIzIiwiaXNzIjoiaHR0cHM6Ly9uaWthYWguaWQiLCJhdWQiOiJodHRwczovL25pa2FhaC1hcGkuaGVyb2t1YXBwLmNvbSIsInVzZXIiOnsiaWQiOjE0fSwiZXhwIjoxNjQ2MDU4MjA3LCJ0eXBlIjoiYWNjZXNzX3Rva2VuIn0.uUnybpY7cD5B0orILsxw53tt0iN1OE7RPZaU2hQz3XU",
        "access_token_expired_at": 1646058207
      }
    },
    OrderLoveStoryModel: {
      "id": 1,
      "title": null,
      "image_url": "https://is3.cloudhost.id/dev-ayonikah/image/365/story-20220130133630.jpg",
      "description": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
      "order": 1,
      "image_url_signed": "https://is3.cloudhost.id/dev-ayonikah/image/365/story-20220130133630.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=BXP9IN1XBYJUGCGLHNQZ%2F20220130%2Fdefault%2Fs3%2Faws4_request&X-Amz-Date=20220130T072007Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=1c670823a754ea9a6ccee98d8b2e00c81ad3f1d7fb20bf0f965e93412523f478"
    },
    ClientGetOrderLoveStory: {
      message: 'Berhasil mendapatkan daftar love story',
      data: [
        { $ref: '#/definitions/OrderLoveStoryModel' }
      ]
    },
    ClientSubmitOrderLoveStory: {
      message: 'Berhasil menambahkan love story',
      data: [
        { $ref: '#/definitions/OrderLoveStoryModel' }
      ]
    },
    ClientUpdateOrderLoveStory: {
      message: 'Berhasil memperbarui love story',
      data: [
        { $ref: '#/definitions/OrderLoveStoryModel' }
      ]
    },
    ClientUpdateOrderOrderLoveStory: {
      message: 'Berhasil memperbarui urutan love story',
      data: [
        { $ref: '#/definitions/OrderLoveStoryModel' }
      ]
    },
    ClientDeleteOrderLoveStory: {
      message: 'Berhasil menghapus love story',
      data: [
        { $ref: '#/definitions/OrderLoveStoryModel' }
      ]
    },
    DigitalWalletModel: {
      "id": 31,
      "account_number": "1234567890",
      "qr_code": "https://is3.cloudhost.id/dev-ayonikah/wallet/365/1-qr-code-20220123103616.jpg",
      "account_name": "Aizul Faiz Iswafaza",
      "order": null,
      "bank": {
        "id": 1,
        "name": "BCA",
        "type": "bank",
        "icon_url": "https://is3.cloudhost.id/dev-ayonikah/asset/bank/bca-icon.svg",
        "allow_qr": true,
        "allow_account_number": true,
        "icon_url_signed": "https://is3.cloudhost.id/dev-ayonikah/asset/bank/bca-icon.svg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=BXP9IN1XBYJUGCGLHNQZ%2F20220130%2Fdefault%2Fs3%2Faws4_request&X-Amz-Date=20220130T153619Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=96928b20882d7149a74cf6d57d21d2e00075c1bd9881b0b9d9384b60e5a47eaf"
      },
      "qr_code_signed": "https://is3.cloudhost.id/dev-ayonikah/wallet/365/1-qr-code-20220123103616.jpg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=BXP9IN1XBYJUGCGLHNQZ%2F20220130%2Fdefault%2Fs3%2Faws4_request&X-Amz-Date=20220130T153619Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=b3d61e26f266446870e32014582f59a0e47d2a824304b81f1631496bce0e7c57"
    },
    ClientGetDigitalWallet: {
      message: 'Berhasil mendapatkan daftar wallet',
      data: [
        { $ref: '#/definitions/DigitalWalletModel' }
      ]
    },
    ClientSubmitDigitalWallet: {
      message: 'Berhasil menambah dompet digital',
      data: [
        { $ref: '#/definitions/DigitalWalletModel' }
      ]
    },
    ClientUpdateDigitalWallet: {
      message: 'Berhasil memperbarui dompet digital',
      data: [
        { $ref: '#/definitions/DigitalWalletModel' }
      ]
    },
    ClientUpdateOrderDigitalWallet: {
      message: 'Berhasil memperbarui urutan dompet digital',
      data: [
        { $ref: '#/definitions/DigitalWalletModel' }
      ]
    },
    ClientRemoveDigitalWallet: {
      message: 'Berhasil menghapus dompet digital',
      data: [
        { $ref: '#/definitions/DigitalWalletModel' }
      ]
    },
    BackgroundMusicModel: {
      "url": "https://is3.cloudhost.id/dev-ayonikah/asset/music/default-1.mp3",
      "title": "Brian McKnight - Marry Your Daughter Saxophone",
      "is_active": false,
      "default_music_id": 1
    },
    ClientGetBackgroundMusic: {
      "message": "Berhasil mendapatkan detail musik",
      "data": { $ref: '#/definitions/BackgroundMusicModel' }
    },
    ClientSetBackgroundMusic: {
      "message": "Berhasil mengaktifkan musik latar",
      "data": { $ref: '#/definitions/BackgroundMusicModel' }
    },
    ClientSubmitBackgroundMusic: {
      "message": "Berhasil menyimpan musik latar",
      "data": { $ref: '#/definitions/BackgroundMusicModel' }
    },
    ClientGetYoutubeUrl: {
      "message": "Berhasil mendapatkan detail youtube",
      "data": {
        "url": "https://www.youtube.com/watch?v=N-1mCcWs2Jc&ab_channel=DeddyCorbuzier"
      }
    },
    ClientSubmitYoutubeUrl: {
      "message": "Berhasil memperbarui tautan youtube",
      "data": {
        "url": "https://www.youtube.com/watch?v=N-1mCcWs2Jc&ab_channel=DeddyCorbuzier"
      }
    },
    LivestreamModel: [
      {
        "id": "number",
        "url": "string",
        "icon": "number",
        "title": "string",
      }
    ],
    LivestreamResponses: {
      "message": "string",
      "data": { $ref: '#/definitions/LivestreamModel' }
    },
    ToolsProvince: {
      message: 'string',
      data: [
        {
          $id: 15,
          $name: 'JAWA TIMUR'
        }
      ]
    },
    ToolsCity: {
      message: 'string',
      data: [
        {
          $id: 230,
          $name: 'KAB. TRENGGALEK'
        }
      ]
    },
    ToolsBank: {
      message: 'string',
      data: [
        {
          "id": 8,
          "type": "bank",
          "name": "NAGARI",
          "icon_url": "https://is3.cloudhost.id/dev-ayonikah/asset/bank/nagari-icon.svg?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=BXP9IN1XBYJUGCGLHNQZ%2F20220123%2Fdefault%2Fs3%2Faws4_request&X-Amz-Date=20220123T162151Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=d03580ddd45f32f3f57c79961b99e080a238c57baf5f15b0d81a45d3df167838",
          "allow_qr": false,
          "allow_account_number": true
        }
      ]
    },
    ToolsDefaultMusic: {
      message: 'string',
      data: [
        {
          "id": 1,
          "title": "Brian McKnight - Marry Your Daughter Saxophone",
          "url": "https://is3.cloudhost.id/dev-ayonikah/asset/music/default-1.mp3",
          "url_signed": "https://is3.cloudhost.id/dev-ayonikah/asset/music/default-1.mp3?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=BXP9IN1XBYJUGCGLHNQZ%2F20220129%2Fdefault%2Fs3%2Faws4_request&X-Amz-Date=20220129T025407Z&X-Amz-Expires=3600&X-Amz-SignedHeaders=host&X-Amz-Signature=0a84e964e67b671ae093cd875ea0d78849a1a01b9484281b7d3e3246967c1961"
        }
      ]
    },
    ToolsSource: {
      message: 'string',
      data: [
        {
          $id: 0,
          $name: 'Lainnya'
        }
      ]
    },
    CoupleModel: {
      "name": "string",
      "fullname": "string",
      "father_name": "string",
      "mother_name": "string",
      "birth_order": "string"
    },
    ClientCoupleSubmit: {
      groom: {
        $ref: '#/definitions/CoupleModel'
      },
      bride: {
        $ref: '#/definitions/CoupleModel'
      }
    },
    Parents: {
      father: "Simon Doe",
      mother: "Marie Doe"
    },
    User: {
      name: "Jhon Doe",
      age: 29,
      parents: {
        $ref: '#/definitions/Parents'
      },
      diplomas: [
        {
          school: "XYZ University",
          year: 2020,
          completed: true,
          internship: {
            hours: 290,
            location: "XYZ Company"
          }
        }
      ]
    },
    AddUser: {
      $name: "Jhon Doe",
      $age: 29,
      about: ""
    }
  }
}

swaggerAutogen(outputFile, endpointsFiles, doc).then(() => {
  process.exit(0)
})