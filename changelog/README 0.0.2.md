# RELEASE V 0.0.2

## Register Order /order/check-domain-availability
[Todo] Check Sub Domain Availability (Optional)
[Method] POST
Request:
```bash
{
	"sub_domain": "pika-chu"
}
```
Note:
- optional ya, soalnya di /order/register sudah ada juga validasinya

## Register Order /order/register
[Todo] Only for Register Order User
[Method] POST
Request:
```bash
{
	"package": 1,
	"theme": 1,
	"name": "PIKACHU",
	"phone_number": "6281230080114",
	"email": "pikachu@pokemon.com",
	"event_date": "2021-07-09 00:00:00",
	"sub_domain": "pika-char"
}
```
Note:
- package: belum disesuaikan bisa dari WEB mau berapa aja angkanya (sementara hindari 0)
- theme: belum disesuaikan bisa dari WEB mau berapa aja angkanya (sementara hindari 0)
- sub_domain: bisa diisikan setelah atau sebelum memanggil /order/check-domain-availability
- phone_number: hindari angka `0` didepannya

Response
```bash
{
  "message": "Berhasil membuat pesanan undangan, kami akan segera menghubungi anda",
  "data": {
    "orderId": 4
  }
}
```

## Add data bride /order/add-wedding-detail
[Todo] Menambahkan detail data pemesan
[Method] POST
Request:
```bash
{
	"order_id": 4,
	"bride": {
		"name": "PIKACHU",
		"fullname": "PIKA PIKA CHUUUUUU",
		"father_name": "Doctor PI",
		"mother_name": "Doctor KA",
		"birth_order": "Pertama"
	},
	"groom": {
		"name": "CHAR",
		"fullname": "CHARIZARD",
		"father_name": "CHARIMAN",
		"mother_name": "IZAWOMAN",
		"birth_order": "Kedua"
	},
	"details": {
		"location_url": null,
		"calendar_url": null,
		"stream_url": null
	},
	"events": [
		{
			"name": "Akad",
			"date": "2021-07-09 00:00:00",
			"time": "19.00-20.00 WIB",
			"location": "unknown",
			"address": "unknown",
			"slot": 1
		}
	],
	"wallets": [
		{
			"bank_name": "OVO",
			"account_name": "PT PERTAMINI",
			"account_number": "6281230080114",
			"slot": 1
		}
	]
}
```
NOTE:
- Bisa digunakan untuk menambahkan dan update data pemesan
- slot diperuntukan untuk nomor urutan (eg. ada 2 data walltets maka slot nanti ada 1 dan 2)
- account_number: hindari angka `0` didepannya 

Response
```bash
{
  "message": "Berhasil menambahkan detail informasi undangan"
}
```

## Get Order /info/:sub_domain
[Todo] Mendapatkan Informasi Undangan
[Method] GET

Response
```bash
{
  "message": "Berhasil mendapatkan data pesanan",
  "data": {
    "id": 4,
    "package": 0,
    "theme": 1,
    "phone_number": "6281230080114",
    "email": "pikachu@pokemon.com",
    "event_date": "2021-07-09 00:00:00",
    "bride": {
      "name": "PIKACHU",
      "fullname": "PIKA PIKA CHUUUUUU",
      "father_name": "Doctor PI",
      "mother_name": "Doctor KA"
    },
    "groom": {
      "name": "CHAR",
      "fullname": "CHARIZARD",
      "father_name": "CHARIMAN",
      "mother_name": "IZAWOMAN"
    },
    "events": [
      {
        "name": "Akad",
        "date": "2021-07-09 00:00:00",
        "dayname": "Jum'at",
        "monthname": "Juli",
        "time": "19.00-20.00 WIB",
        "location": "unknown",
        "address": "unknown",
        "slot": 1
      },
      {
        "name": "Akad",
        "date": "2021-07-13 00:00:00",
        "dayname": "Selasa",
        "monthname": "Juli",
        "time": "19.00-20.00 WIB",
        "location": "unknown",
        "address": "unknown",
        "slot": 2
      }
    ],
    "wallets": [
      {
        "bank_name": "OVO",
        "account_number": "6281230080114",
        "account_name": "PT PERTAMINI",
        "slot": 1
      },
      {
        "bank_name": "T-Cash",
        "account_number": "6252134097311",
        "account_name": "BUDI",
        "slot": 2
      }
    ]
  }
}
```
