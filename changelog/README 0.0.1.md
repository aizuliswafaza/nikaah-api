## Register Order /order/register
[Todo] Initial data customer
[Method] POST
Request:
```bash
{
	"name": "aizul faiz iswafaza",
	"phone_number": "6281230080114",
	"email": "6281230080114"
}
```

Response
```bash
{
  "message": "Berhasil membuat pesanan",
  "data": {
    "order_id": 15,
    "order_name": "aizul faiz iswafaza",
    "order_cp": "6281230080114",
    "order_email": "6281230080114",
    "order_date": "2021-07-09T17:56:59.000Z"
  }
}
```

## Add data bride /order/add-bride
[Todo] Input data Bride
[Method] POST
Request:
```bash
{
	"order_id": 1,
	"bride": {
		"name": "User 1",
		"fullname": "User Kesatu",
		"father_name": "Bot A1",
		"mother_name": "Bot B1",
		"birth_order": "Pertama"
	},
	"groom": {
		"name": "User 2",
		"fullname": "User Kedua",
		"father_name": "Bot X1",
		"mother_name": "Bot Y1",
		"birth_order": "Kedua"
	},
	"details": {
		"location_url": null,
		"calendar_url": null
	}
}
```

Response
```bash
{
  "message": "Berhasil menambahkan data mempelai"
}
```

## Add data events /order/add-event
[Todo] Input data events
[Method] POST
Request:
```bash
{
	"order_id": 1,
	"events": [
		{
			"name": "Akad",
			"date": "2021-07-09 00:00:00",
			"time": "19.00-20.00 WIB",
			"location": "unknown",
			"address": "unknown",
			"order_number": 2
		},
		{
			"name": "Akad",
			"date": "AAAAAA",
			"time": "19.00-20.00 WIB",
			"location": "unknown",
			"address": "unknown",
			"order_number": 1
		}
	]
}
```

Response
```bash
{
  "message": "Berhasil menambahkan 2 acara"
}
```

## Get Order /order/list/:order_id
[Todo] Get By Order
[Method] GET
Request:
```bash
{
}
```

Response
```bash
{
  "message": "Berhasil mendapatkan data pesanan",
  "data": {
    "id": 1,
    "phone_number": "6281230080114",
    "email": "6281230080114",
    "order_date": "2021-07-09 21:33:24",
    "bride": {
      "name": "User 1",
      "fullname": "User Kesatu",
      "father_name": "Bot A1",
      "mother_name": "Bot B1"
    },
    "groom": {
      "name": "User 2",
      "fullname": "User Kedua",
      "father_name": "Bot X1",
      "mother_name": "Bot Y1"
    },
    "events": [
      {
        "name": "Akad",
        "date": "2021-07-09 00:00:00",
        "dayname": "Jum'at",
        "monthname": "Juli",
        "time": "19.00-20.00 WIB",
        "location": "unknown",
        "address": "unknown",
        "order_number": 0
      },
      {
        "name": "Akad",
        "date": "0000-00-00 00:00:00",
        "dayname": "-",
        "monthname": "-",
        "time": "19.00-20.00 WIB",
        "location": "unknown",
        "address": "unknown",
        "order_number": 0
      }
    ]
  }
}
```