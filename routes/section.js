'use strict'

const express = require('express')
const router = express.Router()

const {
  Main,
  Custom
} = require('../lib/section')

const { PlatinumOnly, GoldAndPlatinumOnly } = require('../middleware/restrictByPackage')
const UserAuthorization = require('../middleware/authentication')
const {
  general_set_activation: generalSetActiveRequirement,
  general_submit: generalSubmitRequirement,
  homepage: HomepageSectionRequirement,
  greeting: GreetingSectionRequirement,
  bride: BrideSectionRequirement,
  countdown: CountdownSectionRequirement,
  felicitation: FelicitationSectionRequirement,
  custom: CustomSectionRequirement,
} = require('../middleware/form_requirement').Section

router.get('/all', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Section']
  // #swagger.description = 'Get all configuration of section homepage'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { userdata } = req
  return Main.getAllSection(userdata)
    .then(result => res.json(result))
    .catch(error => next(error))
})

// ################################################# GET SECTION #################################################
router.get('/homepage', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Section (homepage)']
  // #swagger.description = 'Get configuration of homepage section'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionHomepageGet' } }

  const { userdata } = req
  return Main.getSection(userdata, 'homepage')
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/greeting', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Section (greeting)']
  // #swagger.description = 'Get configuration of greeting section'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionGreetingGet' } }

  const { userdata } = req
  return Main.getSection(userdata, 'greeting')
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/bride', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Section (bride)']
  // #swagger.description = 'Get configuration of bride section'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionBrideGet' } }

  const { userdata } = req
  return Main.getSection(userdata, 'bride')
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/countdown', UserAuthorization, GoldAndPlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (countdown)']
  // #swagger.description = 'Get configuration of countdown section'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionCountdownGet' } }

  const { userdata } = req
  return Main.getSection(userdata, 'countdown')
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/event', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Section (event)']
  // #swagger.description = 'Get configuration of event section'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionEventGet' } }

  const { userdata } = req
  return Main.getSection(userdata, 'event')
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/gallery', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Section (gallery)']
  // #swagger.description = 'Get configuration of gallery section'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionGalleryGet' } }

  const { userdata } = req
  return Main.getSection(userdata, 'gallery')
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/felicitation', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Section (felicitation)']
  // #swagger.description = 'Get configuration of felicitation section'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionFelicitationGet' } }

  const { userdata } = req
  return Main.getSection(userdata, 'felicitation')
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/livestream', UserAuthorization, GoldAndPlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (livestream)']
  // #swagger.description = 'Get configuration of livestream section'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionDefaultGet' } }

  const { userdata } = req
  return Main.getSection(userdata, 'livestream')
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/wallet', UserAuthorization, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (wallet)']
  // #swagger.description = 'Get configuration of wallet section'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionDefaultGet' } }

  const { userdata } = req
  return Main.getSection(userdata, 'wallet')
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/story', UserAuthorization, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (story)']
  // #swagger.description = 'Get configuration of story section'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionDefaultGet' } }

  const { userdata } = req
  return Main.getSection(userdata, 'story')
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/youtube', UserAuthorization, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (youtube)']
  // #swagger.description = 'Get configuration of youtube section'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionDefaultGet' } }

  const { userdata } = req
  return Main.getSection(userdata, 'youtube')
    .then(result => res.json(result))
    .catch(error => next(error))
})

// ################################################# SET SECTION #################################################
router.post('/countdown/set', UserAuthorization, generalSetActiveRequirement, GoldAndPlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (countdown)']
  // #swagger.description = 'Set status active of countdown section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["is_active"],
        "properties": {
          "is_active": { "type": "boolean" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionCountdownGet' } }

  const { userdata } = req
  const { is_active: isActive } = req.body
  return Main.setSection(userdata, 'countdown', isActive)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/livestream/set', UserAuthorization, generalSetActiveRequirement, GoldAndPlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (livestream)']
  // #swagger.description = 'Set status active of livestream section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["is_active"],
        "properties": {
          "is_active": { "type": "boolean" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionDefaultGet' } }

  const { userdata } = req
  const { is_active: isActive } = req.body
  return Main.setSection(userdata, 'livestream', isActive)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/wallet/set', UserAuthorization, generalSetActiveRequirement, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (wallet)']
  // #swagger.description = 'Set status active of wallet section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["is_active"],
        "properties": {
          "is_active": { "type": "boolean" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionDefaultGet' } }

  const { userdata } = req
  const { is_active: isActive } = req.body
  return Main.setSection(userdata, 'wallet', isActive)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/story/set', UserAuthorization, generalSetActiveRequirement, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (story)']
  // #swagger.description = 'Set status active of story section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["is_active"],
        "properties": {
          "is_active": { "type": "boolean" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionDefaultGet' } }

  const { userdata } = req
  const { is_active: isActive } = req.body
  return Main.setSection(userdata, 'story', isActive)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/youtube/set', UserAuthorization, generalSetActiveRequirement, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (youtube)']
  // #swagger.description = 'Set status active of youtube section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["is_active"],
        "properties": {
          "is_active": { "type": "boolean" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionDefaultGet' } }

  const { userdata } = req
  const { is_active: isActive } = req.body
  return Main.setSection(userdata, 'youtube', isActive)
    .then(result => res.json(result))
    .catch(error => next(error))
})

// ################################################# SUBMIT SECTION #################################################
router.post('/homepage/submit', UserAuthorization, HomepageSectionRequirement.submit, (req, res, next) => {
  // #swagger.tags = ['Section (homepage)']
  // #swagger.description = 'Update configuration of homepage section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": [],
        "properties": {
          "title": { "type": "string" },
          "description": { "type": "string" },
          "sub_title": { "type": "string" },
          "datetime": { "type": "string", "example": "2021-10-16 07:00:00" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionHomepageSubmit' } }

  const { body, userdata } = req
  const { title, description } = body
  return Main.submit(userdata, 'homepage', body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/greeting/submit', UserAuthorization, GreetingSectionRequirement.submit, (req, res, next) => {
  // #swagger.tags = ['Section (greeting)']
  // #swagger.description = 'Update configuration of greeting section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": [],
        "properties": {
          "title": { "type": "string" },
          "description": { "type": "string" },
          "total_sessions": { "type": "number" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionGreetingSubmit' } }

  const { body, userdata } = req
  return Main.submit(userdata, 'greeting', body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/bride/submit', UserAuthorization, BrideSectionRequirement.submit, (req, res, next) => {
  // #swagger.tags = ['Section (bride)']
  // #swagger.description = 'Update configuration of bride section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": [],
        "properties": {
          "title": { "type": "string" },
          "description": { "type": "string" },
          "reverse_order": { "type": "boolean" },
          "hide_image": { "type": "boolean" },
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionBrideSubmit' } }

  const { body, userdata } = req
  return Main.submit(userdata, 'bride', body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/countdown/submit', UserAuthorization, CountdownSectionRequirement.submit, GoldAndPlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (countdown)']
  // #swagger.description = 'Update configuration of countdown section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": [],
        "properties": {
          "title": { "type": "string" },
          "description": { "type": "string" },
          "datetime": { "type": "string", "example": "2021-10-16 07:00:00" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionCountdownSubmit' } }

  const { body, userdata } = req
  return Main.submit(userdata, 'countdown', body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/event/submit', UserAuthorization, generalSubmitRequirement, (req, res, next) => {
  // #swagger.tags = ['Section (event)']
  // #swagger.description = 'Update configuration of event section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": [],
        "properties": {
          "title": { "type": "string" },
          "description": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionDefaultGet' } }

  const { body, userdata } = req
  return Main.submit(userdata, 'event', body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/gallery/submit', UserAuthorization, generalSubmitRequirement, (req, res, next) => {
  // #swagger.tags = ['Section (gallery)']
  // #swagger.description = 'Update configuration of gallery section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": [],
        "properties": {
          "title": { "type": "string" },
          "description": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionDefaultGet' } }

  const { body, userdata } = req
  return Main.submit(userdata, 'gallery', body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/felicitation/submit', UserAuthorization, FelicitationSectionRequirement.submit, (req, res, next) => {
  // #swagger.tags = ['Section (felicitation)']
  // #swagger.description = 'Update configuration of felicitation section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": [],
        "properties": {
          "title": { "type": "string" },
          "description": { "type": "string" },
          "show_confirmation": { "type": "boolean" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionFelicitationSubmit' } }

  const { body, userdata } = req
  return Main.submit(userdata, 'felicitation', body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/livestream/submit', UserAuthorization, generalSubmitRequirement, GoldAndPlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (livestream)']
  // #swagger.description = 'Update configuration of livestream section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": [],
        "properties": {
          "title": { "type": "string" },
          "description": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionDefaultGet' } }

  const { body, userdata } = req
  return Main.submit(userdata, 'livestream', body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/wallet/submit', UserAuthorization, generalSubmitRequirement, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (wallet)']
  // #swagger.description = 'Update configuration of wallet section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": [],
        "properties": {
          "title": { "type": "string" },
          "description": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionDefaultGet' } }

  const { body, userdata } = req
  return Main.submit(userdata, 'wallet', body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/story/submit', UserAuthorization, generalSubmitRequirement, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (story)']
  // #swagger.description = 'Update configuration of story section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": [],
        "properties": {
          "title": { "type": "string" },
          "description": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionDefaultGet' } }

  const { body, userdata } = req
  return Main.submit(userdata, 'story', body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/youtube/submit', UserAuthorization, generalSubmitRequirement, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (youtube)']
  // #swagger.description = 'Update configuration of youtube section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": [],
        "properties": {
          "title": { "type": "string" },
          "description": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionDefaultGet' } }

  const { body, userdata } = req
  return Main.submit(userdata, 'youtube', body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

// ################################################# CUSTOM SECTION #################################################
router.get('/custom', UserAuthorization, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (custom)']
  // #swagger.description = 'Get configuration of custom section'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionCustomGet' } }

  const { userdata } = req
  return Custom.getAll(userdata)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/custom/submit', UserAuthorization, generalSubmitRequirement, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (custom)']
  // #swagger.description = 'Create configuration of custom section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": [],
        "properties": {
          "title": { "type": "string" },
          "description": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionCustomGet' } }

  const { body, userdata } = req
  const { title, description } = body
  return Custom.submit(userdata, title, description)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/custom/update', UserAuthorization, CustomSectionRequirement.update, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (custom)']
  // #swagger.description = 'Update configuration of custom section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["id"],
        "properties": {
          "id": { "type": "number" },
          "title": { "type": "string" },
          "description": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionCustomGet' } }

  const { body, userdata } = req
  const { id: sectionId, title, description } = body
  return Custom.update(userdata, sectionId, title, description)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/custom/remove', UserAuthorization, CustomSectionRequirement.remove, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Section (custom)']
  // #swagger.description = 'Remove custom section'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["id"],
        "properties": {
          "id": { "type": "number" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/SectionCustomGet' } }

  const { body, userdata } = req
  const { id: sectionId } = body
  return Custom.remove(userdata, sectionId)
    .then(result => res.json(result))
    .catch(error => next(error))
})

module.exports = router
