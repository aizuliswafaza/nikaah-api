'use strict'

const express = require('express')
const router = express.Router()

const Tool = require('../lib/tool')

router.get('/province', (req, res, next) => {
  // #swagger.tags = ['Tools']
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ToolsProvince' } }

  return Tool.getProv()
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/city/:prov_id', (req, res, next) => {
  // #swagger.tags = ['Tools']
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ToolsCity' } }

  const { prov_id: provId } = req.params
  return Tool.getCity(provId)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/bank', (req, res, next) => {
  // #swagger.tags = ['Tools']
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ToolsBank' } }

  return Tool.getBank()
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/source', (req, res, next) => {
  // #swagger.tags = ['Tools']
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ToolsSource' } }

  return res.json(Tool.getSource())
})

router.get('/default/music', (req, res, next) => {
  // #swagger.tags = ['Tools']
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ToolsDefaultMusic' } }

  return Tool.getMusics()
    .then(result => res.json(result))
    .catch(error => next(error))
})

module.exports = router
