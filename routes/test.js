'use strict'

const express = require('express')
const router = express.Router()

const SES = require('../lib/third-party/aws/SES')

router.post('/ses', (req, res, next) => {
    return SES.send()
      .then(result => res.json(result))
      .catch(error => next(error))
})

module.exports = router
