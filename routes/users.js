'use strict'

const express = require('express')
const router = express.Router()
const User = require('../lib/user')
const CheckAuthorization = require('../middleware/authentication')
const UserRequirement = require('../middleware/form_requirement').User

// router.post('/card/submit', (req, res, next) => {
//   const { domain } = req.body
//   return User.exportCard(domain)
//     .then(result => res.json(result))
//     .catch(error => next(error))
// })

router.post('/syncis3', (req, res, next) => {
  const { id } = req.body
  return User.syncIs3(id)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/syncis3/recreate', (req, res, next) => {
  const { id } = req.body
  return User.reCreateSimulation(id)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/remove', (req, res, next) => {
  const { order_id: orderId } = req.body
  return User.removeUnusedFile(orderId)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/profile/complete', CheckAuthorization, UserRequirement.complete_register, (req, res, next) => {
  // #swagger.tags = ['User Information']
  // #swagger.description = 'Choose domain user on registration'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["code", "phone", "source", "province", "city", "religion"],
        "properties": {
          "code": { "type": "string", "example": "62" },
          "phone": { "type": "string", "example": "81230080111" },
          "source": { "type": "number" },
          "source_other": { "type": "string", "example": "Lainnya" },
          "province": { "type": "number" },
          "city": { "type": "number" },
          "religion": { "type": "number" },
          "is_hijab": { "type": "boolean" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { body: payload, userdata } = req
  return User.completeProfile(userdata, payload)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/profile', CheckAuthorization, (req, res, next) => {
  // #swagger.tags = ['User Information']
  // #swagger.description = 'Get detail user information'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientProfile' } }

  const { userdata } = req
  const { id } = userdata
  return User.getProfile(id)
    .then(result => res.json(result))
    .catch(error => next(error))
})

module.exports = router
