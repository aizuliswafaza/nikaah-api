'use strict'

const express = require('express')
const router = express.Router()

const AdminOrder = require('../lib/admin/order')
const { MasterBank, MasterMusic } = require('../lib/admin/master')

const MasterBankRequirement = require('../middleware/form_requirement').Admin.MasterBank
router.get('/send', (req, res, next) => {
    return AdminOrder.send()
      .then(result => res.json(result))
      .catch(error => next(error))
})

router.get('/master/bank/list', (req, res, next) => {
  return MasterBank.getList()
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/master/bank/submit', MasterBankRequirement.submit, (req, res, next) => {
  const { body } = req
  return MasterBank.submit(body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/master/bank/update', MasterBankRequirement.update, (req, res, next) => {
  const { body } = req
  return MasterBank.update(body)
  .then(result => res.json(result))
  .catch(error => next(error))
})

router.post('/master/music/submit', (req, res, next) => {
  const { body } = req
  return MasterMusic.submit(body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

// router.post('/master/bank/update-icon', (req, res, next) => {
//   const { body: { id, url, format } } = req
//   return MasterBank.updateImage(id, url, format)
//     .then(result => res.json(result))
//     .catch(error => next(error))
// })

// router.post('/master/bank/delete', MasterBankRequirement.delete, (req, res, next) => {
//   const { id } = req.body
//   return MasterBank.delete(id)
//     .then(result => res.json(result))
//     .catch(error => next(error))
// })

module.exports = router
