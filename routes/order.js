'use strict'

const express = require('express')
const router = express.Router()
const Order = require('../lib/order/main')
const OrderDomain = require('../lib/order/domain')
const OrderWallet = require('../lib/orderWallet')
const OrderUrl = require('../lib/orderUrl')
const OrderLoveStory = require('../lib/orderLoveStory')
const UserAuthorization = require('../middleware/authentication')
const AdminAuthorization = require('../middleware/authenticationAdmin')
const OrderWalletRequirement = require('../middleware/form_requirement').OrderWallet
const OrderUrlRequirement = require('../middleware/form_requirement').OrderUrl
const OrderLoveStoryRequirement = require('../middleware/form_requirement').OrderLoveStory
const { PlatinumOnly, GoldAndPlatinumOnly } = require('../middleware/restrictByPackage')

router.get('/index', (req, res, next) => {
  return res.json({ message: 'welcome to order API' })
})

router.get('/sync-expired', (req, res, next) => {
  return Order.syncExpiredOrder()
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/list', AdminAuthorization, (req, res, next) => {
  const { order, sort, search, event_date_start, event_date_end } = req.query
  return Order.getList(search, sort, order, event_date_start, event_date_end)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/details/:public_id', AdminAuthorization, UserAuthorization, (req, res, next) => {
  const { public_id: publicId } = req.params
  return Order.getPubDetails(publicId)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/greeting/:public_id', AdminAuthorization, (req, res, next) => {
  const { public_id: publicId } = req.params
  return Order.getGreetingCard(publicId, true)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.delete('/greeting/:public_id/remove/:id', (req, res, next) => {
  const { public_id: publicId, id } = req.params
  return Order.hideComment(publicId, id)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/check-domain-availability', (req, res, next) => {
  const { sub_domain: subDomain } = req.body
  return Order.checkAvailableSubDomain(subDomain)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/domain/check', (req, res, next) => {
  const { sub_domain: subDomain } = req.body
  return Order.checkAvailableSubDomain(subDomain)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/register', AdminAuthorization, (req, res, next) => {
  const { body } = req
  return Order.register(body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/register/:id', AdminAuthorization, (req, res, next) => {
  const { body } = req
  const { id: orderId } = req.params
  return Order.register(body, orderId)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/add-wedding-detail', AdminAuthorization, UserAuthorization, (req, res, next) => {
  const { body } = req
  const { order_id: orderId, bride, groom } = body
  const brideInfo = {
    male: bride,
    female: groom
  }

  return Order.addWeddingDetail(orderId, brideInfo, body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/add-expired-date', AdminAuthorization, (req, res, next) => {
  const { body } = req
  const { order_id: orderId, expired_date: expiredDate } = body

  return Order.setExtentionExpiredDate(orderId, expiredDate)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/gallery/remove', AdminAuthorization, UserAuthorization, (req, res, next) => {
  const { body, userdata } = req
  const { order_id: orderId = null, filename } = body
  return Order.removeGallery(userdata, filename, orderId)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/domain/check', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Order - Domain']
  // #swagger.description = 'Check domain on user registration'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["sub_domain"],
        "properties": {
          "sub_domain": { "type": "number" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { body, userdata } = req
  const { sub_domain: subDomain } = body
  return OrderDomain.check(userdata, subDomain)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/domain/submit', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Order - Registration']
  // #swagger.description = 'Choose domain user on registration'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["sub_domain"],
        "properties": {
          "sub_domain": { "type": "number" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { body, userdata } = req
  const { sub_domain: subDomain } = body
  return Order.submitDomain(userdata, subDomain)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/bride/submit', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Order - Registration']
  // #swagger.description = 'Submit groom and bride information on registration'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["fullname"],
        "properties": {
          "fullname": { "type": "boolean" },
          "description": { "type": "boolean" },
          "instagram_url": { "type": "boolean" },
          "instagram_username": { "type": "boolean" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { body, userdata } = req
  return Order.submitBride(userdata, body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/event/submit', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Order - Registration']
  // #swagger.description = 'Submit event informations on registration'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      schema: {
        "name": "Resepsi",
        "date": "2021-09-18",
        "start_time": "08:00",
        "end_time": "10:00",
        "tz": "wib",
        "location": "lokasi",
        "address": "address",
        "location_url": ""
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { body, userdata } = req
  return Order.submitEvent(userdata, body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/event/submit/all', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Order - Registration']
  // #swagger.description = 'Submit event informations on registration'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      schema: {
        events: [
          {
            "name": "Resepsi",
            "date": "2021-09-18",
            "start_time": "08:00",
            "end_time": "10:00",
            "tz": "wib",
            "location": "lokasi",
            "address": "address",
            "location_url": ""
          }
        ]
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { body, userdata } = req
  const { events } = body
  return Order.submitEventAll(userdata, events)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/theme/submit', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Order - Registration']
  // #swagger.description = 'Choose theme on registration'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["theme", "theme_color"],
        "properties": {
          "theme": { "type": "number" },
          "theme_color": { "type": "number" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { body, userdata } = req
  const { theme, theme_color: themeColor } = body
  return Order.submitTheme(userdata, theme, themeColor)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/package/submit', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Order - Registration']
  // #swagger.description = 'Choose package on registration'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["package"],
        "properties": {
          "package": { "type": "number" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { body, userdata } = req
  const { package: packet } = body
  return Order.submitPackage(userdata, packet)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/wallet', UserAuthorization, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Digital Wallet']
  // #swagger.description = 'Get list user digital wallet'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientGetDigitalWallet' } }

  const { userdata } = req
  return OrderWallet.getList(userdata)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/wallet/submit', UserAuthorization, OrderWalletRequirement.submit, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Digital Wallet']
  // #swagger.description = 'Submit a user digital wallet'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["bank_id", "account_name", "order"],
        "properties": {
          "bank_id": { "type": "number" },
          "account_name": { "type": "string" },
          "account_number": { "type": "string" },
          "qr_code": { "type": "string", "example": "Value of cloudinary" },
          "qr_code_format": { "type": "string", "example": "Can be png, jpeg, jpg, etc." },
          "order": { "type": "number" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientSubmitDigitalWallet' } }

  const { body, userdata } = req
  return OrderWallet.submit(userdata, body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/wallet/update', UserAuthorization, OrderWalletRequirement.update, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Digital Wallet']
  // #swagger.description = 'Update a user digital wallet'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["id", "bank_id", "account_name", "order"],
        "properties": {
          "id": { "type": "number" },
          "bank_id": { "type": "number" },
          "account_name": { "type": "string" },
          "account_number": { "type": "string" },
          "qr_code": { "type": "string", "example": "Value of cloudinary" },
          "qr_code_format": { "type": "string", "example": "Can be png, jpeg, jpg, etc." }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientUpdateDigitalWallet' } }

  const { body, userdata } = req
  return OrderWallet.update(userdata, body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/wallet/order/update', UserAuthorization, OrderWalletRequirement.updateOrder, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Digital Wallet']
  // #swagger.description = 'Update a user digital wallet'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      schema: {
        wallets: [
          {
            id: 0,
            order: 0
          }
        ]
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientUpdateOrderDigitalWallet' } }

  const { body, userdata } = req
  const { wallets } = body
  return OrderWallet.updateOrder(userdata, wallets)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/wallet/delete', UserAuthorization, OrderWalletRequirement.delete, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Digital Wallet']
  // #swagger.description = 'Remove a user digital wallet'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["id"],
        "properties": {
          "id": { "type": "number" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientRemoveDigitalWallet' } }

  const { body, userdata } = req
  const { id } = body
  return OrderWallet.delete(userdata, id)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/story', UserAuthorization, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Love Story']
  // #swagger.description = 'Get list of user love story'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientGetOrderLoveStory' } }

  const { userdata } = req
  return OrderLoveStory.getList(userdata)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/story/submit', UserAuthorization, OrderLoveStoryRequirement.submit, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Love Story']
  // #swagger.description = 'Submit user love story'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["order"],
        "properties": {
          "title": { "type": "string" },
          "image_url": { "type": "string" },
          "description": { "type": "string" },
          "order": { "type": "number" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientSubmitOrderLoveStory' } }

  const { body, userdata } = req
  return OrderLoveStory.submit(userdata, body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/story/update', UserAuthorization, OrderLoveStoryRequirement.update, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Love Story']
  // #swagger.description = 'Update user love story'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["id"],
        "properties": {
          "id": { "type": "number" },
          "title": { "type": "string" },
          "image_url": { "type": "string" },
          "description": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientUpdateOrderLoveStory' } }

  const { body, userdata } = req
  return OrderLoveStory.update(userdata, body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/story/order/update', UserAuthorization, OrderLoveStoryRequirement.updateOrder, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Love Story']
  // #swagger.description = 'Update order of love story'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      schema: {
        stories: [
          {
            id: 0,
            order: 0
          }
        ]
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientUpdateOrderOrderLoveStory' } }

  const { body, userdata } = req
  const { stories } = body
  return OrderLoveStory.updateOrder(userdata, stories)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/story/delete', UserAuthorization, OrderLoveStoryRequirement.delete, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Love Story']
  // #swagger.description = 'Delete user love story'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["id"],
        "properties": { "id": { "type": "number" } }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientDeleteOrderLoveStory' } }

  const { body, userdata } = req
  const { id } = body
  return OrderLoveStory.delete(userdata, id)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/image/upload', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Order - Image Upload']
  // #swagger.description = 'Upload user an image'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["public_id", "secure_url", "height", "width", "format"],
        "properties": {
          "public_id": { "type": "string" },
          "secure_url": { "type": "string" },
          "height": { "type": "number" },
          "width": { "type": "number" },
          "format": { "type": "string" },
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { userdata, body } = req
  return Order.uploadImage(userdata, body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/image/remove', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Order - Image Upload']
  // #swagger.description = 'Remove user an image'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["filename"],
        "properties": {
          "filename": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { userdata, body } = req
  const { filename } = body
  return Order.removeImage(userdata, filename)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/music', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Order - Background Music']
  // #swagger.description = 'Get details info background music'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientGetBackgroundMusic' } }

  const { userdata } = req
  return OrderUrl.getMusicDetail(userdata)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/music/set', UserAuthorization, OrderUrlRequirement.music.set, (req, res, next) => {
  // #swagger.tags = ['Order - Background Music']
  // #swagger.description = 'Set active user background music'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["is_active"],
        "properties": { "is_active": { "type": "boolean" } }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientSetBackgroundMusic' } }

  const { userdata, body } = req
  const { is_active: isActive } = body
  return OrderUrl.setBackgroundMusic(userdata, isActive)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/music/submit', UserAuthorization, OrderUrlRequirement.music.submit, (req, res, next) => {
  // #swagger.tags = ['Order - Background Music']
  // #swagger.description = 'Submit background music url and title'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["url", "title"],
        "properties": {
          "title": { "type": "string" },
          "url": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientSubmitBackgroundMusic' } }

  const { userdata, body } = req
  const { url, title } = body
  return OrderUrl.submitBackgroundMusic(userdata, title, url)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/youtube', UserAuthorization, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Youtube Url']
  // #swagger.description = 'Get detail youtube url'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientGetYoutubeUrl' } }

  const { userdata } = req
  return OrderUrl.getYoutubeDetail(userdata)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/youtube/submit', UserAuthorization, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Youtube Url']
  // #swagger.description = 'Submit link youtube'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["url"],
        "properties": {
          "title": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientSubmitYoutubeUrl' } }

  const { userdata, body } = req
  const { url } = body
  return OrderUrl.submitYoutubeUrl(userdata, url)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/youtube/remove', UserAuthorization, PlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Youtube Url']
  // #swagger.description = 'Remove youtube url'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["id"],
        "properties": {
          "id": { "type": "number" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { userdata } = req
  return OrderUrl.removeYoutubeUrl(userdata)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/livestream', UserAuthorization, GoldAndPlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Livesteam']
  // #swagger.description = 'Get detail livestream'
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/LivestreamResponses' } }

  const { userdata } = req
  return OrderUrl.getLiveStream(userdata)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/livestream/submit', UserAuthorization, GoldAndPlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Livesteam']
  // #swagger.description = 'Submit livestream data'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["url"],
        "properties": {
          "url": { "type": "string" },
          "icon": { "type": "string" },
          "title": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/LivestreamResponses' } }

  const { userdata, body } = req
  const { url } = body
  return OrderUrl.submitLiveStream(userdata, url)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/livestream/update', UserAuthorization, GoldAndPlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Livesteam']
  // #swagger.description = 'Update livestream data'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["url"],
        "properties": {
          "id": { "type": "number" },
          "url": { "type": "string" },
          "icon": { "type": "string" },
          "title": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/LivestreamResponses' } }

  const { userdata, body } = req
  const { id: livestreamId, url, icon, title } = body
  return OrderUrl.updateLiveStream(userdata, livestreamId, url, icon, title)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/livestream/remove', UserAuthorization, GoldAndPlatinumOnly, (req, res, next) => {
  // #swagger.tags = ['Order - Livesteam']
  // #swagger.description = 'Remove Livesteam data'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["id"],
        "properties": {
          "id": { "type": "number" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/LivestreamResponses' } }

  const { userdata, body } = req
  const { id: livestreamId } = body
  return OrderUrl.removeLiveStream(userdata, livestreamId)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/package/upgrade', UserAuthorization, (req, res, next) => {
  // #swagger.tags = ['Order']
  // #swagger.description = 'Upgrade package'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["id"],
        "properties": {
          "package_id": { "type": "number" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { userdata, body } = req
  const { package_id: packageId } = body
  return Order.packageUpgrade(userdata, packageId)
    .then(result => res.json(result))
    .catch(error => next(error))
})

module.exports = router
