'use strict'

const express = require('express')
const Storage = require('../lib/third-party/storage')
const router = express.Router() 

router.post('/stats', (req, res, next) => {
  const { param1, param2 } = req.body
  return Storage.checkStatFile(param1, param2)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/signed', (req, res, next) => {
  const { param1, param2 } = req.body
  return Storage.signedUrl(param1, param2)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/remove', (req, res, next) => {
  const { param1, param2 } = req.body
  return Storage.removeGallery(param1, param2)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/upload', (req, res, next) => {
  const { param1, param2 } = req.body
  return Storage.upload(param1, param2)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.get('/pre-signed-upload', (req, res, next) => {
  return Storage.preSignedUpload()
    .then(result => res.json(result))
    .catch(error => next(error))
})

module.exports = router
