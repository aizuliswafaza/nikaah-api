'use strict'

const express = require('express')
const router = express.Router()

const Payment = require('../lib/payment')
const UserAuthorization = require('../middleware/authentication')
const PaymentRequirement = require('../middleware/form_requirement').Payment

router.post('/manual/submit', UserAuthorization, PaymentRequirement.submitManual, (req, res, next) => {
    const { userdata, body } = req
    const { url } = body
    return Payment.submitManual(userdata, url)
      .then(result => res.json(result))
      .catch(error => next(error))
})

module.exports = router
