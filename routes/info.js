'use strict'

const express = require('express')
const router = express.Router()

const Info = require('../lib/info')

router.get('/:sub_domain', (req, res, next) => {
    const { sub_domain: subDomain } = req.params
    return Info.getWeddingLetter(subDomain)
      .then(result => res.json(result))
      .catch(error => next(error))
})

router.get('/greeting/:sub_domain', (req, res, next) => {
  const { sub_domain: subDomain } = req.params
  return Info.getGreetingCard(subDomain)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/greeting/:sub_domain/submit', (req, res, next) => {
  const { sub_domain: subDomain } = req.params
  const { userHadir, userMsg, userName } = req.body
  return Info.submitGreeting(subDomain, userName, userHadir, userMsg)
    .then(result => res.json(result))
    .catch(error => next(error))
})

module.exports = router
