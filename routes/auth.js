'use strict'

const express = require('express')
const router = express.Router()

const Auth = require('../lib/auth')
const AuthRequirement = require('../middleware/form_requirement').Auth

router.post('/login', (req, res, next) => {
  const { username, password } = req.body
  return Auth.login(username, password)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/admin/login', (req, res, next) => {
  const { username, password } = req.body
  return Auth.adminLogin(username, password)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/register/submit', AuthRequirement.register, (req, res, next) => {
  // #swagger.tags = ['Auth Login and Registration']
  // #swagger.description = 'Client registration API'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["fullname", "email", "password"],
        "properties": {
          "fullname": { "type": "string" },
          "email": { "type": "string" },
          "password": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { fullname, email, password } = req.body
  return Auth.register(fullname, email, password)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/register/verify', AuthRequirement.verify, (req, res, next) => {
  // #swagger.tags = ['Auth Login and Registration']
  // #swagger.description = 'Client registration API'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["key", "token"],
        "properties": {
          "key": { "type": "string" },
          "token": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { key, token } = req.body
  return Auth.verify(key, token)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/client/login', AuthRequirement.login, (req, res, next) => {
  // #swagger.tags = ['Auth Login and Registration']
  // #swagger.description = 'Client login API'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["email", "password", "remember_me"],
        "properties": {
          "email": { "type": "string" },
          "password": { "type": "string" },
          "remember_me": { "type": "boolean" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/ClientLogin' } }

  const { email, password, remember_me: isRemembered = false } = req.body
  return Auth.clientLogin(email, password, isRemembered)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/forgot/password/request', AuthRequirement.requestForgotPassword, (req, res, next) => {
  // #swagger.tags = ['Auth Login and Registration']
  // #swagger.description = 'Client request reset password'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["email"],
        "properties": {
          "email": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { email } = req.body
  return Auth.requestForgetPassword(email)
    .then(result => res.json(result))
    .catch(error => next(error))
})

router.post('/forgot/password/verify', AuthRequirement.verifyForgotPassword, (req, res, next) => {
  // #swagger.tags = ['Auth Login and Registration']
  // #swagger.description = 'Client confirmation and reset password'
  /*
    #swagger.parameters['Object'] = {
      in: 'body',
      '@schema': {
        "required": ["key", "token", "password"],
        "properties": {
          "key": { "type": "string" },
          "token": { "type": "string" },
          "password": { "type": "string" }
        }
      }
    }
  */
  // #swagger.responses[200] = { schema: { $ref: '#/definitions/DefaultResult' } }

  const { key, token, password } = req.body
  return Auth.verifyForgetPassword(key, token, password)
    .then(result => res.json(result))
    .catch(error => next(error))
})

module.exports = router
