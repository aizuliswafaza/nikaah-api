'use strict'

const express = require('express')
const router = express.Router()

router.get('/check', (req, res, next) => {
  return res.json({
    version: '1.0.0',
    env: process.env.NODE_ENV,
    'BASE_URL': process.env.BUCKET_BASE_URL,
    'HOST': process.env.BUCKET_HOST,
    'NAME': process.env.BUCKET_NAME,
    'PORT': parseInt(process.env.BUCKET_PORT),
    'ACCESS_KEY': process.env.BUCKET_ACCESS_KEY,
    'SECRET_KEY': process.env.BUCKET_SECRET_KEY,
  })
})

router.use('/admin', require('./admin'))
router.use('/info', require('./info'))
router.use('/users', require('./users'))
router.use('/order', require('./order'))
router.use('/auth', require('./auth'))
router.use('/notification', require('./notification'))
router.use('/storage', require('./storage'))
router.use('/tools', require('./tool'))
router.use('/payment', require('./payment'))
router.use('/test', require('./test'))
router.use('/section', require('./section'))

module.exports = router
