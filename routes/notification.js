'use strict'

const express = require('express')
const router = express.Router()
const Notification = require('../lib/notification')
const NotificationRequirement = require('../middleware/form_requirement').Notification

/* GET users listing. */
router.post('/cloudinary', NotificationRequirement.cloudinary, (req, res, next) => {
  return Notification.cloudinary(req.body)
    .then(result => res.json(result))
    .catch(error => next(error))
})

module.exports = router
