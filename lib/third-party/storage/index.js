'use strict'

const Minio = require('minio')
const got = require('got')
const ServiceError = require('../../../handler/wrapper/ServiceError')

const {
  bucket: {
    BASE_URL: BUCKET_BASE_URL,
    HOST: BUCKET_HOST,
    NAME: BUCKET_NAME,
    ACCESS_KEY: BUCKET_ACCESS_KEY,
    SECRET_KEY: BUCKET_SECRET_KEY,
    PORT: BUCKET_PORT
  }
} = require('../../../config/constants')

const MinioClient = new Minio.Client({
  endPoint: BUCKET_HOST,
  accessKey: BUCKET_ACCESS_KEY,
  secretKey: BUCKET_SECRET_KEY,
  port: BUCKET_PORT,
  useSSL: true
})

class Storage {
  constructor () {}

  async signedUrl (filename) {
    const statFile = await this.checkStatFile(filename)
    if (!statFile) return null
    return MinioClient.presignedGetObject(BUCKET_NAME, filename, 1*60*60)
  }

  async upload (url, filename, metadata = null) {
    try {
      const { body: responseBody } = await got(url, { responseType: 'buffer' })
      await MinioClient.putObject(BUCKET_NAME, filename, responseBody, metadata)
      return this.getFileUrl(filename)
    } catch (error) {
      throw new ServiceError('invalid upload', { meta: error })
    }
  }

  async preSignedUpload (filename = 'hello.txt') {
    return new Promise((resolve, reject) => {
      return MinioClient.presignedPutObject(BUCKET_NAME, filename, 24*60*60, function(err, presignedUrl) {
        if (err) return reject(err)
        console.log(presignedUrl)
        return resolve(presignedUrl)
      })
    })
  }

  async removeGallery (filename) {
    const isExist = await this.checkStatFile(filename)
    if (!isExist) return false

    await MinioClient.removeObject(BUCKET_NAME, filename).catch(() => { throw new ServiceError('failed_delete_is3') })
    return true
  }

  async removeAllGallery (dirname) {
    try {
        const galleries = await this.getGalleries(dirname)
        await MinioClient.removeObjects(BUCKET_NAME, galleries).catch((err) => { throw err })
        return true
    } catch (error) {
        console.log(error)
        throw new ServiceError('failed_delete_all_is3')
    }
  }

  async checkStatFile (filename) {
    return MinioClient.statObject(BUCKET_NAME, filename)
      .catch(() => { return false })
  }

  getGalleries (dirname) {
    return new Promise((resolve, reject) => {
        const streamObjects = MinioClient.listObjects(BUCKET_NAME, dirname, true)
        const listData = []
        streamObjects.on('data', (obj) => listData.push(obj))
        streamObjects.on('end', () => resolve(listData))
        streamObjects.on('error', err => reject(err))
    })
  }

  getFileUrl (filename) {
    return `${BUCKET_BASE_URL}/${filename}`
  }

  getFilePathOnly (url) {
    return url.replace(`${BUCKET_BASE_URL}/`, '')
  }
}

module.exports = new Storage()