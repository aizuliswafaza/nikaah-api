'use strict'

const fs = require('fs')
const ejs = require('ejs')
const SendInBlue = require('sib-api-v3-sdk')
const NodeMailer = require('nodemailer')

const SUBJECT = {
  email_verify: 'Aktivasi Akun Kamu 😊',
  forgot_password: 'Request Atur Ulang Password'
}

const { env: ENV, smtp_service: { SENDER_EMAIL, SENDER_NAME, API_KEY } } = require('../../../config/constants')
const ServiceError = require('../../../handler/wrapper/ServiceError')

class EmailClient {
  constructor () {
    this.SendInBlueClient = SendInBlue.ApiClient.instance
  }

  async __sendProd (subject, target, content) {
    let apiKey = SendInBlueClient.authentications['api-key']
    apiKey.apiKey = API_KEY
    let apiInstance = new SendInBlue.TransactionalEmailsApi()
    let sendSmtpEmail = new SendInBlue.SendSmtpEmail()

    sendSmtpEmail.subject = SUBJECT[subject]
    sendSmtpEmail.htmlContent = content
    sendSmtpEmail.sender = {
      name: SENDER_NAME,
      email: SENDER_EMAIL
    }
    sendSmtpEmail.to = [{ email: target }]

    return apiInstance.sendTransacEmail(sendSmtpEmail).then(function(data) {
      console.log('API called successfully. Returned data: ' + JSON.stringify(data))
      return true
    }, function(error) {
      console.error(error)
      return false
    })
  }

  async __sendDev (subject, target, content) {
    let testAccount = await NodeMailer.createTestAccount()
    let transporter = NodeMailer.createTransport({
      host: "smtp.ethereal.email",
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: testAccount.user, // generated ethereal user
        pass: testAccount.pass, // generated ethereal password
      },
    })

    let info = await transporter.sendMail({
      from: `"${SENDER_NAME} <${SENDER_EMAIL}>"`,
      to: target,
      subject: SUBJECT[subject],
      html: content
    })

    return { data: NodeMailer.getTestMessageUrl(info) }
  }

  compose (subject, data) {
    const htmlTemplate = fs.readFileSync(`./template/${subject}.html`, 'utf8')
    return ejs.render(htmlTemplate, data)
  }

  async send (subject, target, data) {
    console.log('SendEmail::', subject, target, data)
    if (!SUBJECT[subject]) throw new ServiceError('subject_not_found')
    const content = this.compose(subject, data)
    if (ENV === 'production') {
      return this.__sendProd(subject, target, content)
    } else {
      return this.__sendDev(subject, target, content)
    }
  }
}

module.exports = new EmailClient()
