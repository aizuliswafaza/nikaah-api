'use strict'

const moment = require('moment')

const { bucket: { BASE_URL: BUCKET_BASE_URL } } = require('../../config/constants')
const { getFilenameImage } = require('../../helper/getImagePath')
const ServiceError = require('../../handler/wrapper/ServiceError')

const { Sequelize, sequelize } = require('../../models')
const { Op } = Sequelize
const instanceModels = require('../../models/init-models').initModels(sequelize)

const Storage = require('../third-party/storage')
const e = require('express')
const { del } = require('request')

const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'
const FORMAT_DATE_UNIQUE = 'YYYYMMDDHHmmss'

class OrderLoveStory {
  constructor () {
    instanceModels.nk_order_love_story.removeAttribute('id')

    this.OrderLoveStoryRepo = instanceModels.nk_order_love_story
  }

  async getList (userdata) {
    const { order: { order_id: orderId } } = userdata
    const stories = await this.OrderLoveStoryRepo.findAll({
      where: { order_id: orderId },
      attributes: [
        ['ols_id', 'id'],
        ['ols_title', 'title'],
        ['ols_image_url', 'image_url'],
        ['ols_description', 'description'],
        ['ols_order', 'order']
      ],
      order: [['ols_order', 'ASC']],
      raw: true
    })

    const process = stories.map(async story => {
      if (!story.image_url) return story

      const imageUrl = Storage.getFilePathOnly(story.image_url)
      const imageUrlSigned = await Storage.signedUrl(imageUrl)
      return {
        ...story,
        image_url_signed: imageUrlSigned
      }
    })

    const data = await Promise.all(process)
    return { message: 'Berhasil mendapatkan daftar love story', data }
  }

  async __findByOne (whereClause) {
    const story = await this.OrderLoveStoryRepo.findOne({
      where: whereClause,
      attributes: [
        ['ols_id', 'id'],
        ['ols_title', 'title'],
        ['ols_image_url', 'image_url'],
        ['ols_description', 'description'],
        ['ols_order', 'order'],
      ],
      raw: true
    })

    if (!story) throw new ServiceError('love_story_not_found')

    return story
  }

  async count (id) {
    return this.OrderLoveStoryRepo.count({ where: { order_id: id } })
  }

  async submit (userdata, story) {
    const { order: { order_id: orderId } } = userdata
    const currentTime = moment().format(FORMAT_DATE)
    const currentTimeUnique = moment().format(FORMAT_DATE_UNIQUE)
    let imageUrl = null
    if (story.image_url) {
      const filename = getFilenameImage('story', `image/${orderId}/story-${currentTimeUnique}`, story.format)
      imageUrl = await Storage.upload(story.image_url, filename)
    }

    const payload = {
      order_id: orderId,
      ols_title: story.title,
      ols_image_url: imageUrl,
      ols_description: story.description,
      ols_order: story.order,
      ols_created_at: currentTime,
      ols_updated_at: currentTime,
    }

    await this.OrderLoveStoryRepo.create(payload)
    const data = await this.getList(userdata)

    return { ...data, message: `Berhasil menambahkan love story` }
  }

  async update (userdata, story) {
    const { order: { order_id: orderId } } = userdata
    const currentTime = moment().format(FORMAT_DATE)
    const currentTimeUnique = moment().format(FORMAT_DATE_UNIQUE)

    const currentStory = await this.__findByOne({ order_id: orderId, ols_id: story.id })

    let { image_url: imageUrl } = currentStory
    if (story.image_url && story.image_url.includes('cloudinary')) {
      const filename = getFilenameImage('story', `image/${orderId}/story-${currentTimeUnique}`, story.format)
      const [uploadedUrl] = await Promise.all([
        Storage.upload(story.image_url, filename),
        imageUrl && Storage.removeGallery(Storage.getFilePathOnly(imageUrl))
      ])
      imageUrl = uploadedUrl
    }

    if (!story.image_url && imageUrl) {
      await Storage.removeGallery(Storage.getFilePathOnly(imageUrl))
      imageUrl = null
    }

    const payload = {
      ols_title: story.title,
      ols_image_url: imageUrl,
      ols_description: story.description,
      ols_order: story.order,
      ols_updated_at: currentTime,
    }

    await this.OrderLoveStoryRepo.update(payload, { where: { order_id: orderId, ols_id: story.id } })
    const data = await this.getList(userdata)

    return { ...data, message: `Berhasil memperbarui love story` }
  }

  async updateOrder (userdata, stories) {
    const { order: { order_id: orderId } } = userdata
    const currentTime = moment().format(FORMAT_DATE)

    const process = stories.map(async story => {
      return this.OrderLoveStoryRepo.update({ ols_order: story.order, ols_updated_at: currentTime }, { where: { order_id: orderId, ols_id: story.id } })
    })

    await Promise.all(process)
    const data = await this.getList(userdata)

    return { ...data, message: 'Berhasil memperbarui urutan love story' }
  }

  async delete (userdata, id) {
    const { order: { order_id: orderId } } = userdata
    const currentStory = await this.__findByOne({ order_id: orderId, ols_id: id })

    let { image_url: imageUrl } = currentStory
    await Promise.all([
      imageUrl && Storage.removeGallery(Storage.getFilePathOnly(imageUrl)),
      this.OrderLoveStoryRepo.destroy({ where: { order_id: orderId, ols_id: id } })
    ])
    const data = await this.getList(userdata)

    return { ...data, message: 'Berhasil menghapus love story' }
  }
}

module.exports = new OrderLoveStory()