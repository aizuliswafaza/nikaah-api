'use strict'

module.exports = {
  Main: require('./main'),
  Custom: require('./custom')
}
