'use strict'

const moment = require('moment')
const { Sequelize, sequelize } = require('../../models')
const instanceModels = require('../../models/init-models').initModels(sequelize)

const ServiceError = require('../../handler/wrapper/ServiceError')

const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'
const SECTION_TYPE = ['wallet', 'love_story', 'youtube']
const SECTION_TYPE_STR = {
  gallery: 'gallery',
  wallet: 'dompet digital',
  story: 'love story',
  youtube: 'tautan youtube',
  livestream: 'livestream'
}

class SectionCustom {
  constructor () {
    instanceModels.nk_section_custom.removeAttribute('id')

    this.SectionCustomRepo = instanceModels.nk_section_custom
  }

  async getAll (userdata) {
    const { order: { order_id: orderId } } = userdata
    const data = await this.SectionCustomRepo.findAll({
      attributes: [
        ['sccm_id', 'id'],
        ['sccm_title', 'title'],
        ['sccm_description', 'description']
      ],
      where: { order_id: orderId, sccm_type: 'custom' },
      order: [['sccm_id', 'ASC']],
      raw: true
    })

    return { message: 'Berhasil mendapatkan konfigurasi section custom', data }
  }

  async getDetail (orderId, sectionId) {
    const data = await this.SectionCustomRepo.findOne({
      attributes: [
        ['sccm_id', 'id'],
        ['sccm_title', 'title'],
        ['sccm_description', 'description']
      ],
      where: { order_id: orderId, sccm_type: 'custom', sccm_id: sectionId },
      raw: true
    })

    if (!data) throw new ServiceError('custom_section_not_found')

    return data
  }

  async submit (userdata, title, description) {
    const { order: { order_id: orderId } } = userdata
    const currentTime = moment().format(FORMAT_DATE)
    const payload = {
      order_id: orderId,
      sccm_type: 'custom',
      sccm_title: title,
      sccm_description: description,
      sccm_is_active: 1,
      sccm_created_at: currentTime,
      sccm_updated_at: currentTime
    }

    await this.SectionCustomRepo.create(payload)
    const data = await this.getAllCustomSection(userdata)

    return { ...data, message: 'Berhasil menambah konfigurasi section custom' }
  }

  async update (userdata, sectionId, title, description) {
    const { order: { order_id: orderId } } = userdata
    await this.getDetail(orderId, sectionId)

    const currentTime = moment().format(FORMAT_DATE)
    const payload = {
      sccm_title: title,
      sccm_description: description,
      sccm_updated_at: currentTime
    }

    await this.SectionCustomRepo.update(payload, { where: { sccm_id: sectionId, order_id: orderId } })
    const data = await this.getAllCustomSection(userdata)

    return { ...data, message: 'Berhasil mengubah konfigurasi section custom' }
  }

  async remove (userdata, sectionId) {
    const { order: { order_id: orderId } } = userdata
    await this.getDetail(orderId, sectionId)

    await this.SectionCustomRepo.destory({ where: { order_id: orderId, sccm_id: sectionId } })
    const data = await this.getAllCustomSection(userdata)

    return { ...data, message: 'Berhasil menghapus section custom' }
  }
}

module.exports = new SectionCustom()