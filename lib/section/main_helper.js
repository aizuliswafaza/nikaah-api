'use strict'

const moment = require('moment')
const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'

class SectionMainHelper {
  getformated (data, sectionType) {
    const defaultData = {
      title: data.title,
      description: data.description,
      is_active: data.is_active
    }
    let metadata = {}
    switch (sectionType) {
      case 'homepage':
        metadata = JSON.parse(data.metadata)
        return {
          ...defaultData,
          ...metadata,
          sub_title: data.sub_title,
          datetime: data.datetime,
        }
      case 'greeting':
        return {
          ...defaultData,
          total_sessions: data.total_items
        }
      case 'countdown':
        return {
          ...defaultData,
          datetime: data.datetime
        }
      case 'event':
        return {
          ...defaultData,
          total_events: data.total_items
        }
      case 'gallery':
        return {
          ...defaultData,
          total_images: data.total_items
        }
      case 'felicitation':
        metadata = JSON.parse(data.metadata)
        return {
          ...defaultData,
          ...metadata
        }
      case 'bride':
        metadata = JSON.parse(data.metadata)
        return {
          ...defaultData,
          ...metadata
        }
      case 'wallet':
        return {
          ...defaultData,
          total_wallets: data.total_items
        }
      case 'story':
        return {
          ...defaultData,
          total_stories: data.total_items
        }
      // case 'livestream':
      //   return {
      //     ...defaultData,
      //   }
      // case 'youtube':
      //   return {
      //     ...defaultData,
      //   }
      default:
        return {
          ...defaultData,
        }
    }
  }

  submitformated (data, sectionType) {
    const currentTime = moment().format(FORMAT_DATE)
    const defaultData = {
      scm_title: data.title,
      scm_description: data.description,
      scm_updated_at: currentTime
    }
    switch (sectionType) {
      case 'homepage':
        return {
          ...defaultData,
          scm_sub_title: data.sub_title,
          scm_datetime: data.datetime,
          scm_metadata: JSON.stringify({
            padding_top: data.padding_top,
            padding_bottom: data.padding_bottom
          })
        }
      case 'greeting':
        return {
          ...defaultData,
          scm_total_items: data.total_sessions
        }
      case 'countdown':
        return {
          ...defaultData,
          scm_datetime: data.datetime
        }
      case 'felicitation':
        return {
          ...defaultData,
          scm_metadata: JSON.stringify({
            show_result: true,
            show_confirmation: data.show_confirmation
          })
        }
      case 'bride':
        return {
          ...defaultData,
          scm_metadata: JSON.stringify({ reverse_order: data.reverse_order, hide_image: data.hide_image })
        }
      // case 'gallery':
      //   return {
      //     ...defaultData
      //     // scm_total_items: data.total_gallery
      //   }
      // case 'event':
      //   return {
      //     ...defaultData
      //     // scm_total_items: data.total_events
      //   }
      // case 'livestream':
      //   return {
      //     ...defaultData,
      //   }
      // case 'wallet':
      //   return {
      //     ...defaultData,
      //   }
      // case 'story':
      //   return {
      //     ...defaultData,
      //   }
      // case 'youtube':
      //   return {
      //     ...defaultData,
      //   }
      default:
        return {
          ...defaultData,
        }
    }
  }
}

module.exports = new SectionMainHelper()