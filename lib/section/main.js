'use strict'

const moment = require('moment')
const { Sequelize, sequelize } = require('../../models')
const instanceModels = require('../../models/init-models').initModels(sequelize)

const SectionMainHelper = require('./main_helper')

const ServiceError = require('../../handler/wrapper/ServiceError')

const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'
const SECTION_TYPE = {
  homepage: 'halaman utama',
  greeting: 'halaman sapaan',
  bride: 'halaman mempelai',
  countdown: 'halaman countdown',
  event: 'daftar acara',
  gallery: 'halaman galeri mempelai',
  felicitation: 'halaman ucapan tamu undangan',
  livestream: 'tautan livestream',
  wallet: 'dompet digital',
  story: 'cerita cintaku',
  youtube: 'tautan youtube',
}

const SECTION_TYPE_ORDER = {
  greeting: 1,
  homepage: 2,
  countdown: 3,
  bride: 4,
  event: 5,
  gallery: 6,
  felicitation: 7,
  livestream: 8,
  wallet: 9,
  story: 10,
  youtube: 11,
}

class SectionMain {
  constructor () {
    instanceModels.nk_section_main.removeAttribute('id')

    this.SectionMainRepo = instanceModels.nk_section_main
  }

  async getAllSection (userdata) {
    const { order: { order_id: orderId } } = userdata
    const allUserSections = await this.SectionMainRepo.findAll({
      attributes: [
        ['scm_type', 'type'],
        ['scm_title', 'title'],
        ['scm_sub_title', 'sub_title'],
        ['scm_description', 'description'],
        ['scm_sub_description', 'sub_description'],
        ['scm_is_active', 'is_active'],
        ['scm_total_items', 'total_items'],
        ['scm_metadata', 'metadata'],
        ['scm_datetime', 'datetime'],
      ],
      where: { order_id: orderId },
      order: [['scm_order', 'ASC']],
      raw: true
    })

    const data = {}
    allUserSections.forEach(config => {
      const { type: sectionType } = config
      data[sectionType] = SectionMainHelper.getformated(config, sectionType)
    })

    return { message: `Berhasil mendapatkan semua konfigurasi section`, data }
  }

  async getSection (userdata, sectionType) {
    const { order: { order_id: orderId } } = userdata
    const selectedSectionType = SECTION_TYPE[sectionType]
    if (!selectedSectionType) throw new ServiceError('section_not_found')

    const selectedSection = await this.SectionMainRepo.findOne({
      attributes: [
        ['scm_title', 'title'],
        ['scm_sub_title', 'sub_title'],
        ['scm_description', 'description'],
        ['scm_sub_description', 'sub_description'],
        ['scm_is_active', 'is_active'],
        ['scm_total_items', 'total_items'],
        ['scm_metadata', 'metadata'],
        ['scm_datetime', 'datetime'],
      ],
      where: {
        order_id: orderId,
        scm_type: sectionType
      },
      raw: true
    })

    if (!selectedSection) throw new ServiceError('section_configuration_not_available')

    const data = SectionMainHelper.getformated(selectedSection, sectionType)

    return { message: `Berhasil mendapatkan konfigurasi section ${selectedSectionType}`, data }
  }

  async initiate (userdata, sectionType, { isAutoActive = false, additionalAttribute = {} } = {}) {
    const { order: { order_id: orderId } } = userdata
    const selectedSectionType = SECTION_TYPE[sectionType]
    if (!selectedSectionType) throw new ServiceError('section_not_found')

    const currentTime = moment().format(FORMAT_DATE)
    await this.SectionMainRepo.create({
      ...additionalAttribute,
      order_id: orderId,
      scm_type: sectionType,
      scm_is_active: isAutoActive,
      scm_order: SECTION_TYPE_ORDER[sectionType],
      scm_created_at: currentTime,
      scm_updated_at: currentTime
    })
    const { data } = await this.getSection(userdata, sectionType)

    return data
  }

  async setActivation (userdata, sectionType, isActive = false) {
    const { order: { order_id: orderId } } = userdata
    await this.getSection(userdata, sectionType)

    const currentTime = moment().format(FORMAT_DATE)
    await this.SectionMainRepo.update({
      scm_is_active: isAutoActive,
      scm_updated_at: currentTime
    }, { where: { order_id: orderId, scm_type: sectionType } })
    const { data } = await this.getSection(userdata, sectionType)

    return { message: `Berhasil ${isActive ? 'mengaktifkan' : 'menonaktifkan'} section ${SECTION_TYPE[sectionType]}`, data }
  }

  async submit (userdata, sectionType, body) {
    const { order: { order_id: orderId } } = userdata
    await this.getSection(userdata, sectionType)

    const payload = SectionMainHelper.submitformated(body, sectionType)

    await this.SectionMainRepo.update(payload, { where: { order_id: orderId, scm_type: sectionType } })
    const { data } = await this.getSection(userdata, sectionType)

    return { message: `Berhasil mengubah konfigurasi section ${SECTION_TYPE[sectionType]}`, data }
  }

  async update (userdata, sectionType, payload) {
    const { order: { order_id: orderId } } = userdata
    return this.SectionEventRepo.update(payload, { where: { order_id: orderId, scm_type: sectionType } })
  }
}

module.exports = new SectionMain()