'use strict'

const moment = require('moment')

const { bucket: { BASE_URL: BUCKET_BASE_URL } } = require('../../config/constants')
const { getFilenameImage } = require('../../helper/getImagePath')
const ServiceError = require('../../handler/wrapper/ServiceError')

const { Sequelize, sequelize } = require('../../models')
const { Op } = Sequelize
const instanceModels = require('../../models/init-models').initModels(sequelize)

const ToolsBank = require('../tool')
const Storage = require('../third-party/storage')
const e = require('express')
const { del } = require('request')

const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'
const FORMAT_DATE_UNIQUE = 'YYYYMMDDHHmmss'

class OrderUrl {
  constructor () {
    instanceModels.nk_order_url.removeAttribute('id')

    this.OrderUrlRepo = instanceModels.nk_order_url
  }

  async getMusicDetail (userdata) {
    const { order: { order_id: orderId } } = userdata
    const orderUrl = await this.__findByOne({ order_id: orderId, ou_type: 'BACKGROUND_MUSIC' })
    let data = null
    if (orderUrl) {
      const musicConfig = JSON.parse(orderUrl.config)
      if (orderUrl.url && musicConfig.is_active) {
        const musicPath = Storage.getFilePathOnly(orderUrl.url)
        orderUrl.url_signed = await Storage.signedUrl(musicPath)
      }

      delete orderUrl.id
      delete orderUrl.type
      delete orderUrl.config

      data = {
        ...orderUrl,
        ...musicConfig
      }
    }

    return { message: 'Berhasil mendapatkan detail musik', data }
  }

  async __findByOne (whereClause) {
    const music = await this.OrderUrlRepo.findOne({
      where: whereClause,
      attributes: [
        ['ou_id', 'id'],
        ['ou_type', 'type'],
        ['ou_url', 'url'],
        ['ou_config', 'config']
      ],
      raw: true
    })

    return music
  }

  async setBackgroundMusic (userdata, isActive = false) {
    const { order: { order_id: orderId } } = userdata
    const currentTime = moment().format(FORMAT_DATE)
    const music = await this.__findByOne({ order_id: orderId, ou_type: 'BACKGROUND_MUSIC' })
    const payload = {
      order_id: orderId,
      ou_type: 'BACKGROUND_MUSIC',
      ou_config: JSON.stringify({
        is_active: true
      }),
      ou_created_at: currentTime,
      ou_updated_at: currentTime
    }

    if (music) {
      delete payload.order_id
      delete payload.ou_type
      delete payload.ou_created_at

      const musicConfig = JSON.parse(music.config)
      payload.ou_config = JSON.stringify({
        ...musicConfig,
        is_active: isActive
      })

      await this.OrderUrlRepo.update(payload, { where: { ou_id: music.id } })
    } else {
      await this.OrderUrlRepo.create(payload)
    }

    const data = await this.getMusicDetail(userdata)

    return { ...data, message: `Berhasil ${isActive ? 'mengaktifkan' : 'menonaktifkan'} musik latar` }
  }

  async submitBackgroundMusic (userdata, title, url) {
    const { order: { order_id: orderId } } = userdata
    const music = await this.__findByOne({ order_id: orderId, ou_type: 'BACKGROUND_MUSIC' })
    let musicConfig = music ? JSON.parse(music.config) : { is_active: false }

    if (!music || !musicConfig.is_active ) throw new ServiceError('background_music_not_active')
    if (!url.includes(BUCKET_BASE_URL)) {
      const filename = getFilenameImage('icon', `audio/${orderId}`, 'mp3')
      url = await Storage.upload(url, filename)
    }
    if (url.includes(BUCKET_BASE_URL) && url.includes('asset/music/default-')) {
      const pathStorage = `${BUCKET_BASE_URL}/asset/music/default-`
      const musicExt = '.mp3'
      musicConfig.default_music_id = Number(url.replace(pathStorage, '').replace(musicExt, ''))
    } else {
      delete musicConfig.default_music_id
    }

    if (music.url && url !== music.url && !music.url.includes('asset/music/default-')) {
      const previousMusic = Storage.getFilePathOnly(music.url)
      await Storage.removeGallery(previousMusic)
    }
    musicConfig.title = title

    const currentTime = moment().format(FORMAT_DATE)
    const payload = {
      ou_url: url,
      ou_config: JSON.stringify(musicConfig),
      ou_updated_at: currentTime
    }

    await this.OrderUrlRepo.update(payload, { where: { ou_id: music.id } })
    const data = await this.getMusicDetail(userdata)

    return { ...data, message: `Berhasil menyimpan musik latar` }
  }

  async getYoutubeDetail (userdata) {
    const { order: { order_id: orderId } } = userdata
    const orderUrl = await this.__findByOne({ order_id: orderId, ou_type: 'YOUTUBE' })

    if (orderUrl) {
      delete orderUrl.id
      delete orderUrl.type
      delete orderUrl.config
    }

    return { message: 'Berhasil mendapatkan detail youtube', data: orderUrl }
  }

  async submitYoutubeUrl (userdata, url) {
    const { order: { order_id: orderId } } = userdata
    const youtube = await this.__findByOne({ order_id: orderId, ou_type: 'YOUTUBE' })
    const currentTime = moment().format(FORMAT_DATE)
    const payload = {
      order_id: orderId,
      ou_type: 'YOUTUBE',
      ou_url: url,
      ou_created_at: currentTime,
      ou_updated_at: currentTime
    }
    if (youtube) {
      delete payload.order_id
      delete payload.ou_type
      delete payload.ou_created_at

      await this.OrderUrlRepo.update(payload, { where: { ou_id: youtube.id } })
    } else {
      await this.OrderUrlRepo.create(payload)
    }

    const data = await this.getYoutubeDetail(userdata)

    return { ...data, message: `Berhasil ${!youtube ? 'menambahkan' : 'memperbarui'} tautan youtube` }
  }

  async removeYoutubeUrl (userdata) {
    const { order: { order_id: orderId } } = userdata
    const youtube = await this.__findByOne({ order_id: orderId, ou_type: 'YOUTUBE' })
    if (!youtube) throw new ServiceError('youtub_url_not_found')
    const currentTime = moment().format(FORMAT_DATE)
    const payload = {
      ou_url: null,
      ou_updated_at: currentTime
    }
    await this.OrderUrlRepo.update(payload, { where: { ou_id: youtube.id } })

    return { message: `Berhasil menghapus tautan youtube` }
  }

  async getLiveStream (userdata) {
    const { order: { order_id: orderId } } = userdata
    const liveStreams = await this.OrderUrlRepo.findAll({
      attributes: [
        ['ou_id', 'id'],
        ['ou_url', 'url'],
        ['ou_config', 'config'],
      ],
      where: {
        order_id: orderId,
        ou_type: 'LIVESTREAM'
      },
      order: [['ou_id', 'ASC']],
      raw: true
    })

    const data = liveStreams.map(ls => {
      const config = JSON.parse(ls.config)
      delete ls.config
      return {
        ...ls,
        ...config
      }
    })

    return { message: 'Berhasil mendapatkan data livestream', data }
  }

  async submitLiveStream (userdata, url, icon, title) {
    const { order: { order_id: orderId } } = userdata
    const totalLiveStream = await this.OrderUrlRepo.count({ where: { order_id: orderId, ou_type: 'livestream'  } })

    if (totalLiveStream >= 3) throw new ServiceError('livestream_reach_limit')

    const currentTime = moment().format(FORMAT_DATE)
    const payload = {
      order_id: orderId,
      ou_type: 'LIVESTREAM',
      ou_url: url,
      ou_config: JSON.stringify({
        title,
        icon
      }),
      ou_created_at: currentTime,
      ou_updated_at: currentTime
    }

    await this.OrderUrlRepo.create(payload)
    const data = await this.getLiveStream(userdata)

    return { ...data, message: 'Berhasil menambahkan tautan livestream' }
  }

  async updateLiveStream (userdata, livestreamId, url, icon, title) {
    const { order: { order_id: orderId } } = userdata
    const liveStreamData = await this.__findByOne({ ou_id: livestreamId, order_id: orderId })

    if (!liveStreamData) throw new ServiceError('livestream_not_found')

    const currentTime = moment().format(FORMAT_DATE)
    const payload = {
      ou_url: url,
      ou_config: JSON.stringify({
        title,
        icon
      }),
      ou_updated_at: currentTime
    }

    await this.OrderUrlRepo.update(payload, { where: { ou_id: livestreamId } })
    const data = await this.getLiveStream(userdata)

    return { ...data, message: 'Berhasil menambahkan tautan livestream' }
  }

  async removeLiveStream (userdata, livestreamId) {
    const { order: { order_id: orderId } } = userdata
    const liveStreamData = await this.__findByOne({ ou_id: livestreamId, order_id: orderId })

    if (!liveStreamData) throw new ServiceError('livestream_not_found')

    await this.OrderUrlRepo.destroy({ where: { ou_id: livestreamId } })
    const data = await this.getLiveStream(userdata)

    return { ...data, message: 'Berhasil menambahkan tautan livestream' }
  }
}

module.exports = new OrderUrl()