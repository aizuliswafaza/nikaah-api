'use strict'

const moment = require('moment')

const { getFilenameImage } = require('../../helper/getImagePath')
const ServiceError = require('../../handler/wrapper/ServiceError')

const { Sequelize, sequelize } = require('../../models')
const { Op } = Sequelize
const instanceModels = require('../../models/init-models').initModels(sequelize)

const ToolsBank = require('../tool')
const Storage = require('../third-party/storage')

const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'
const FORMAT_DATE_UNIQUE = 'YYYYMMDDHHmmss'

class OrderWallet {
  constructor () {
    instanceModels.nk_order_wallet.removeAttribute('id')

    this.BankRepo = instanceModels.nk_banks
    this.OrderRepo = instanceModels.nk_order
    this.OrderWalletRepo = instanceModels.nk_order_wallet
  }

  async getList (userdata) {
    const { order: { order_id: orderId } } = userdata
    const orderWallets = await this.OrderWalletRepo.findAll({
      where: { order_id: orderId },
      attributes: [
        ['ow_id', 'id'],
        ['ow_account_number', 'account_number'],
        ['ow_qr_code', 'qr_code'],
        ['ow_account_name', 'account_name'],
        ['ow_order', 'order']
      ],
      include: [
        {
          attributes: [
            ['bank_id', 'id'],
            ['bank_name', 'name'],
            ['bank_type', 'type'],
            ['bank_icon_url', 'icon_url'],
            ['bank_config', 'config']
          ],
          model: this.BankRepo,
          as: 'ow_bank'
        }
      ],
      order: [['ow_order', 'ASC']]
    })

    const process = orderWallets.map(async wallet => {
      const walletStringed = wallet.toJSON()
      const { ow_bank: bank, qr_code: qrCode } = walletStringed
      const { icon_url: iconUrl, config } = bank
      const { allow_qr: allowQr, allow_account_number: allowAccountNumber } = JSON.parse(config)
      const qrCodePathUrl = qrCode ? Storage.getFilePathOnly(qrCode) : null
      const iconPathUrl = Storage.getFilePathOnly(iconUrl)

      const [qrCodeSigned, iconUrlSigned] = await Promise.all([
        !!qrCodePathUrl ? Storage.signedUrl(qrCodePathUrl) : null,
        Storage.signedUrl(iconPathUrl)
      ])

      bank.allow_qr = allowQr
      bank.allow_account_number = allowAccountNumber
      bank.icon_url_signed = iconUrlSigned

      delete walletStringed.ow_bank
      delete bank.config
      return {
        ...walletStringed,
        bank,
        qr_code: qrCode,
        qr_code_signed: qrCodeSigned
      }
    })

    const data = await Promise.all(process)

    return { message: 'Berhasil mendapatkan daftar wallet', data }
  }

  async __findByOne (whereClause) {
    const bank = await this.OrderWalletRepo.findOne({
      where: whereClause,
      attributes: [
        ['ow_id', 'id'],
        ['ow_bank_id', 'bank_id'],
        ['ow_account_number', 'account_number'],
        ['ow_qr_code', 'qr_code'],
        ['ow_account_name', 'account_name']
      ],
      raw: true
    })

    if (!bank) throw new ServiceError('wallet_not_found')
    return bank
  }

  async submit (userdata, body) {
    const currentTime = moment().format(FORMAT_DATE)
    const { order: { order_id: orderId } } = userdata
    const bankData = await ToolsBank.findOneBank(body.bank_id)
    const bankConfig = JSON.parse(bankData.config)

    const { qr_code: qrCode, qr_code_format: qrCodeFormat } = body
    let qrCodeUrl = null
    if (bankConfig.allow_qr && qrCode && qrCode.includes('cloudinary')) {
      const timestamp = moment().format(FORMAT_DATE_UNIQUE)
      const filename = getFilenameImage('icon', `wallet/${orderId}/${bankData.id}-qr-code-${timestamp}`, qrCodeFormat)
      qrCodeUrl = await Storage.upload(qrCode, filename)
    }

    const payload = {
      ow_bank_id: body.bank_id,
      order_id: orderId,
      ow_account_name: body.account_name,
      ow_account_number: bankConfig.allow_account_number ? body.account_number: null,
      ow_qr_code: qrCodeUrl,
      ow_order: body.order,
      ow_created_at: currentTime,
      ow_updated_at: currentTime
    }

    await this.OrderWalletRepo.create(payload)
    const data = await this.getList(userdata)

    return { ...data, message: `Berhasil menambah dompet digital` }
  }

  async update (userdata, body) {
    const currentTime = moment().format(FORMAT_DATE)
    const { order: { order_id: orderId } } = userdata
    const { id: walletId, bank_id: newBankId, qr_code: newQrCode, qr_code_format: qrCodeFormat } = body
    const [bankData, walletData] = await Promise.all([
      ToolsBank.findOneBank(newBankId),
      this.__findByOne({ ow_id: walletId })
    ])

    const { allow_qr: newAllowQr, allow_account_number: newAllowAccountNumber } = JSON.parse(bankData.config)
    const { qr_code: currentQrCode } = walletData
    let qrCodeUrl = currentQrCode

    if ((!newAllowQr && currentQrCode) || (newAllowQr && currentQrCode && currentQrCode !== newQrCode)) {
      const currentQrCodePath = Storage.getFilePathOnly(currentQrCode)
      await Storage.removeGallery(currentQrCodePath)
    }

    if (newAllowQr && newQrCode?.includes('cloudinary')) {
      const timestamp = moment().format(FORMAT_DATE_UNIQUE)
      const filename = getFilenameImage('icon', `wallet/${orderId}/${newBankId}-qr-code-${timestamp}`, qrCodeFormat)
      qrCodeUrl = await Storage.upload(newQrCode, filename)
    }

    const payload = {
      ow_bank_id: newBankId,
      order_id: orderId,
      ow_account_name: body.account_name,
      ow_account_number: newAllowAccountNumber ? body.account_number: null,
      ow_qr_code: qrCodeUrl,
      ow_updated_at: currentTime
    }

    await this.OrderWalletRepo.update(payload, { where: { ow_id: walletId } })
    const data = await this.getList(userdata)

    return { ...data, message: `Berhasil memperbarui dompet digital` }
  }

  async updateOrder (userdata, wallets) {
    const currentTime = moment().format(FORMAT_DATE)
    const { order: { order_id: orderId } } = userdata

    const process = wallets.map(wallet => {
      return this.OrderWalletRepo.update({ ow_order: wallet.order, ow_updated_at: currentTime }, { where: { order_id: orderId, ow_id: wallet.id } })
    })

    await Promise.all(process)
    const data = await this.getList(userdata)

    return { ...data, message: 'Berhasil memperbarui urutan dompet digital' }
  }

  async delete (userdata, id) {
    const { order: { order_id: orderId } } = userdata
    const wallet = await this.__findByOne({ ow_id: id, order_id: orderId })
    const currentBankName = Storage.getFilePathOnly(wallet.qr_code ?? '')
    console.log(currentBankName, wallet)
    await Promise.all([
      currentBankName && Storage.removeGallery(currentBankName),
      this.OrderWalletRepo.destroy({ where: { ow_id: id } })
    ])

    const data = await this.getList(userdata)

    return { ...data, message: `Berhasil menghapus dompet digital` }
  }
}

module.exports = new OrderWallet()