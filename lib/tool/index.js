'use strict'

const _ = require('lodash')
const ServiceError = require('../../handler/wrapper/ServiceError')

const { Sequelize, sequelize } = require('../../models')
const { Op } = Sequelize
const instanceModels = require('../../models/init-models').initModels(sequelize)

const Storage = require('../third-party/storage')

class Tool {
  constructor () {
    this.ProvRepo = instanceModels.nk_provinces
    this.CityRepo = instanceModels.nk_cities
    this.BankRepo = instanceModels.nk_banks
    this.MusicRepo = instanceModels.nk_musics
  }

  async getMusics () {
    const musics = await this.MusicRepo.findAll({
      attributes: [
        ['music_id', 'id'],
        ['music_title', 'title'],
        ['music_url', 'url']
      ],
      raw: true
    })

    const data = await Promise.all(
      musics.map(async music => {
        const musicPath = Storage.getFilePathOnly(music.url)
        music.url_signed = await Storage.signedUrl(musicPath)
        return music
      })
    )

    return { message: 'Berhasil mendapatkan daftar default musik latar', data }
  }

  async getBank () {
    const banks = await this.BankRepo.findAll({
      where: {
        bank_status: 1
      },
      attributes: [
        ['bank_id', 'id'],
        ['bank_type', 'type'],
        ['bank_name', 'name'],
        ['bank_icon_url', 'icon_url'],
        ['bank_config', 'config'],
      ],
      raw: true
    })

    const bankList = []
    const ewalletList = []

    await Promise.all(
      banks.map(async (bank) => {
        const config = JSON.parse(bank.config)
        const iconPath = Storage.getFilePathOnly(bank.icon_url)
        const iconUrl = await Storage.signedUrl(iconPath)
        delete bank.config
        const bankFormatted = {
          ...bank,
          ...config,
          icon_url: iconUrl,
        }

        if (bank.type === 'bank') bankList.push(bankFormatted)
        else ewalletList.push(bankFormatted)

        return bankFormatted
      })
    )

    const data = {
      bank: _.sortBy(bankList, 'name'),
      ewallet: _.sortBy(ewalletList, 'name')
    }

    return { message: 'Berhasil mendapatkan daftar bank', data }
  }

  async findOneBank (id) {
    const bank = await this.BankRepo.findOne({
      where: { bank_id: id },
      attributes: [
        ['bank_id', 'id'],
        ['bank_name', 'name'],
        ['bank_status', 'status'],
        ['bank_icon_url', 'icon_url'],
        ['bank_config', 'config'],
      ],
      raw: true
    })

    if (!bank) throw new ServiceError('bank_not_found')

    return bank
  }

  async getProv (whereClause = null) {
    const provs = await this.ProvRepo.findAll({
      where: whereClause,
      attributes: [
        ['prov_id', 'id'],
        ['prov_name', 'name']
      ]
    })
    return { message: 'Berhasil mendapatkan daftar provinsi', data: provs }
  }

  async getCity (provId, cityId = null) {
    const whereClause = { prov_id: provId }
    if (cityId) whereClause.city_id = cityId

    const cities = await this.CityRepo.findAll({
      where: whereClause,
      attributes: [
        ['city_id', 'id'],
        ['city_name', 'name']
      ]
    })
    return { message: 'Berhasil mendapatkan daftar kota/kabupaten', data: cities }
  }

  getSource () {
    return {
      message: 'Berhasil mendapatkan daftar sumber informasi',
      data: [
        {
          id: 1,
          name: 'Teman/Keluarga'
        },
        {
          id: 2,
          name: 'Instagram'
        },
        {
          id: 3,
          name: 'Facebook'
        },
        {
          id: 4,
          name: 'Google'
        },
        {
          id: 0,
          name: 'Lainnya'
        }
      ]
    }
  }
}

module.exports = new Tool()