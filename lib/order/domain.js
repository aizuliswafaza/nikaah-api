'use strict'

const { Sequelize, sequelize } = require('../../models')
const { Op } = Sequelize
const instanceModels = require('../../models/init-models').initModels(sequelize)
const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'
const FORMAT_START_DATE = 'YYYY-MM-DD 00:00:00'
const FORMAT_END_DATE = 'YYYY-MM-DD 23:59:59'
const moment = require('moment')
const ServiceError = require('../../handler/wrapper/ServiceError')
const { Dayname, Monthname, based_url: basedUrl } = require('../../config/constants')
const jsonParser = require('../../helper/jsonSafeParser')
const jsonStringify = require('../../helper/jsonSafeStringify')
const { v4: uuidv4, v1: uuidv1 } = require('uuid')
const Storage = require('../third-party/storage')
const { getSectionImage, getFilenameImage } = require('../../helper/getImagePath')
const { bucket: {
    BASE_URL: BUCKET_BASE_URL
} } = require('../../config/constants')

class OrderDomain {
  constructor () {
    instanceModels.nk_order.removeAttribute('id')

    this.OrderRepo = instanceModels.nk_order
  }

  async getUserSubDomain (userdata) {
    const { order: { order_id: orderId }, id: userId } = userdata
    const data = await this.OrderRepo.findOne({
      attributes: [['order_sub_domain', 'sub_domain']],
      where: { user_id: userId, order_id: orderId },
      raw: true
    })

    return data
  }

  async check (userdata, subDomain) {
    const { order: { order_id: orderId }, id: userId } = userdata
    const currentTime = moment().format(FORMAT_DATE)

    const order = await this.OrderRepo.findOne({
      attributes: [['order_id', 'id']],
      where: {
        [Op.or]: [
          {
            user_id: userId,
            order_id: { [Op.ne]: orderId }
          },
          {
            user_id: { [Op.ne]: userId },
          }
        ],
        order_sub_domain: subDomain,
        order_expired_date: { [Op.gte]: currentTime }
      },
      raw: true
    })

    if (order) throw new ServiceError('unavailable_sub_domain')

    return { message: `${subDomain} belum tersedia dan bisa digunakan` }
  }

  async submit (userdata, domain) {
    const { order: { order_id: orderId } } = userdata
    const currentTime = moment().format(FORMAT_DATE)

    await this.checkAvailableSubDomain(domain, orderId)
    await this.OrderRepo.update({ order_sub_domain: domain, order_updated_at: currentTime }, { where: { order_id: orderId } })
    const data = await this.getUserSubDomain(userdata)

    return { message: `Berhasil menyimpan domain ${domain}`, data }
  }
}

module.exports = new OrderDomain()