'use strict'

const { Sequelize, sequelize } = require('../../models')
const { Op } = Sequelize
const instanceModels = require('../../models/init-models').initModels(sequelize)
const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'
const FORMAT_START_DATE = 'YYYY-MM-DD 00:00:00'
const FORMAT_END_DATE = 'YYYY-MM-DD 23:59:59'
const moment = require('moment')
const ServiceError = require('../../handler/wrapper/ServiceError')
const { Dayname, Monthname, based_url: basedUrl } = require('../../config/constants')
const jsonParser = require('../../helper/jsonSafeParser')
const jsonStringify = require('../../helper/jsonSafeStringify')
const { v4: uuidv4, v1: uuidv1 } = require('uuid')
const Storage = require('../third-party/storage')
const { getSectionImage, getFilenameImage } = require('../../helper/getImagePath')
const { bucket: {
    BASE_URL: BUCKET_BASE_URL
} } = require('../../config/constants')

class Order {
    constructor () {
        instanceModels.nk_order_detail.removeAttribute('id')
        instanceModels.nk_order_bride.removeAttribute('id')
        instanceModels.nk_order_event.removeAttribute('id')
        instanceModels.nk_order_gallery.removeAttribute('id')
        instanceModels.nk_order_wallet.removeAttribute('id')
        instanceModels.nk_order_greeting_card.removeAttribute('id')

        this.OrderRepo = instanceModels.nk_order
        this.OrderDetailRepo = instanceModels.nk_order_detail
        this.OrderBrideRepo = instanceModels.nk_order_bride
        this.OrderEventRepo = instanceModels.nk_order_event
        this.OrderGalleryRepo = instanceModels.nk_order_gallery
        this.OrderWalletRepo = instanceModels.nk_order_wallet
        this.OrderGreetingCardRepo = instanceModels.nk_order_greeting_card
        this.CloudinaryLogRepo = instanceModels.nk_cloudinary_log
    }

    async getList (search, sort, order, startDate, endDate) {
        let whereClause = {}
        let orderClause = []
        let eventRangeClause = []

        if (!!search) {
            whereClause[Sequelize.Op.or] =  [
                {
                    'order_sub_domain': {[Sequelize.Op.like]: '%' + search + '%'}
                },
                {
                    'order_cp': {[Sequelize.Op.like]: '%' + search + '%'}
                },
                {
                    'order_code': {[Sequelize.Op.like]: '%' + search + '%'}
                }
            ]
        }

        if (startDate || endDate) {
            eventRangeClause.push({ 'order_event_date': { [Sequelize.Op.gte]: moment(startDate).format(FORMAT_START_DATE) } })
        }

        if (endDate) {
            eventRangeClause.push({ 'order_event_date': { [Sequelize.Op.lte]: moment(startDate).format(FORMAT_END_DATE) } })
        }

        whereClause[Sequelize.Op.and] = eventRangeClause

        if (!!sort && !!order) {
            const sortType = {
                event_date: 'order_event_date',
                created_at: 'order_date'
            }
            orderClause.push([sortType[sort], order])
        }

        const orderList = await this.OrderRepo.findAndCountAll({
            attributes: [
                ['order_public_id', 'id'],
                ['order_code', 'code'],
                ['order_sub_domain', 'sub_domain'],
                ['order_package', 'package'],
                ['order_cp', 'phone_number'],
                ['order_email', 'email'],
                ['order_status', 'status'],
                ['order_payment_status', 'payment_status'],
                ['order_event_date', 'event_date']
            ],
            where: whereClause,
            order: orderClause,
            raw: true
        })

        return {
            message: 'Berhasil mendapatkan daftar pesanan',
            data: orderList
        }
    }

    async getPubDetails (publicId) {
        const order = await this.OrderRepo.findOne({
            where: { order_public_id: publicId },
            raw: true
        })

        if (!order) throw new ServiceError('order_not_found')

        const {
            order_id: orderId,
            order_code: orderCode,
            order_public_id: orderPublicId,
            order_name: orderName,
            order_sub_domain: orderSubDomain,
            order_package: orderPackage,
            order_theme: orderTheme,
            order_theme_color: orderThemeColor,
            order_cp: orderCp,
            order_email: orderEmail,
            order_event_date: orderEventDate,
            order_religion: orderReligion,
            order_main_caption: orderMainCaption,
            order_bride_caption: orderBrideCaption,
            order_metadata: orderMetadata,
            order_status: orderStatus,
            order_payment_status: orderPaymentStatus,
            order_expired_date: orderExpiredDate
        } = order

        const [details, bride, groom, events, wallets, gallery] = await Promise.all([
            this.getDetails(orderId),
            this.getBride(orderId, { isDetail: 1 }),
            this.getBride(orderId, { isMale: 0, isDetail: 1 }),
            this.getEvent(orderId),
            this.getWallet(orderId),
            this.getGallery(orderId)
        ])

        const result = {
            id: orderPublicId,
            code: orderCode,
            package: orderPackage,
            theme: orderTheme,
            theme_color: orderThemeColor,
            phone_number: orderCp,
            religion: orderReligion,
            email: orderEmail,
            event_date: orderEventDate,
            main_caption: orderMainCaption,
            bride_caption: orderBrideCaption,
            event_date: orderEventDate,
            sub_domain: orderSubDomain,
            name: orderName,
            status: orderStatus,
            payment_status: orderPaymentStatus,
            details,
            bride,
            groom,
            events,
            wallets,
            gallery,
            metadata: jsonParser(orderMetadata),
            is_expired: moment().isAfter(orderExpiredDate),
            expired_date: orderExpiredDate
        }

        return {
            message: 'Berhasil mendapatkan data detail',
            data: result
        }
    }

    async syncExpiredOrder () {
        const orders = await this.OrderRepo.findAll({ raw: true })
        const process = orders.map(async data => {
            const { order_id: orderId, order_date: orderDate, order_package: orderPackage } = data
            const expiredDate = this.getExpiredDateByPackage(orderDate, orderPackage)
            return this.OrderRepo.update({ order_expired_date: expiredDate }, { where: { order_id: orderId } })
        })

        await Promise.all(process)

        return { message: 'berhasil' }
    }

    getExpiredDateByPackage (orderDate, orderPackage) {
        let expiredDate = moment(orderDate)
        switch (orderPackage) {
            case 1:
                expiredDate.add(3, 'month')
                break
            case 2:
                expiredDate.add(6, 'month')
                break
            case 3:
                expiredDate.add(12, 'month')
                break
            default:
                expiredDate.add(-1, 'day')
        }

        return expiredDate.add(1, 'day').format(FORMAT_END_DATE)
    }

    async checkAvailableSubDomain (subDomain, orderId = null) {
        const whereClause = {
            order_id: { [Op.ne]: orderId },
            order_sub_domain: subDomain,
            order_expired_date: { [Op.gte]: moment().format(FORMAT_DATE) }
        }

        if (!orderId) delete whereClause.order_id
        const order = await this.OrderRepo.findOne({ where: whereClause })

        if (order) throw new ServiceError('unavailable_sub_domain')

        return { message: `${subDomain} belum tersedia dan bisa digunakan` }
    }

    async register (body, orderId = null) {
        const {
            code,
            theme: orderTheme,
            theme_color: orderThemeColor,
            event_date: orderEventDate,
            sub_domain: orderSubDomain,
            package: orderPackage,
            name: customerName,
            status: orderStatus,
            payment_status: customerPaymentStatus,
            phone_number: customerPhone,
            email: customerEmail,
            religion: customerReligion,
            main_caption: customerMainCaption,
            bride_caption: customerBrideCaption,
            metadata
        } = body

        let orderData = {
            order_code: code,
            order_package: orderPackage,
            order_theme: orderTheme,
            order_theme_color: orderThemeColor,
            order_name: customerName,
            order_religion: customerReligion,
            order_cp: customerPhone,
            order_status: orderStatus,
            order_payment_status: customerPaymentStatus,
            order_email: customerEmail,
            order_sub_domain: orderSubDomain,
            order_event_date: orderEventDate,
            order_main_caption: customerMainCaption,
            order_bride_caption: customerBrideCaption,
            order_metadata: jsonStringify(metadata)
        }

        if (orderId) {
            const isSubDomainExist = await this.OrderRepo.findOne({
                where: {
                    order_sub_domain: orderSubDomain,
                    order_id: { [Op.ne]: orderId },
                    order_expired_date: { [Op.gte]: moment().format(FORMAT_DATE) }
                }
            })

            if (isSubDomainExist) throw new ServiceError('unavailable_sub_domain')

            await this.OrderRepo.update(orderData, { where: { order_public_id: orderId } })
            orderData.order_public_id = orderId
        } else {
            const isSubDomainExist = await this.OrderRepo.findOne({
                where: { order_sub_domain: orderSubDomain }
            })

            if (isSubDomainExist) throw new ServiceError('unavailable_sub_domain')

            const customerDate = moment().format(FORMAT_DATE)
            orderData.order_public_id = uuidv4()
            orderData.order_date = customerDate

            await this.OrderRepo.create(orderData)
        }

        return {
            message: `Berhasil ${ (orderId? "memperbarui" : "menambahkan" )} pesanan undangan`,
            data: {
                orderId: orderData.order_public_id
            }
        }
    }

    async setExtentionExpiredDate (orderId, expiredDate) {
        await this.getOrder({ order_id: orderId })
        await this.OrderRepo.update({ order_status: 4, order_expired_date: expiredDate }, { where: { order_id: orderId } })
        return {
            message: 'Berhasil memperpanjang pesanan'
        }
    }

    async addWeddingDetail (publicId, brideInfo, { details = null, events = [], wallets = [] }) {
        const order = await this.getOrder({ order_public_id: publicId })
        const { order_id: orderId, order_expired_date: orderExpiredDate } = order
        const isExpired = moment().isAfter(orderExpiredDate)

        if(isExpired) throw new ServiceError('order_expired')

        const transaction = await sequelize.transaction()
        await Promise.all([
            this.OrderDetailRepo.destroy({ where: { order_id: orderId }, transaction }),
            this.OrderBrideRepo.destroy({ where: { order_id: orderId }, transaction }),
            this.OrderWalletRepo.destroy({ where: { order_id: orderId }, transaction }),
            this.OrderEventRepo.destroy({ where: { order_id: orderId }, transaction })
        ])

        await Promise.all([
            this.__addDetail(orderId, details, transaction),
            this.__addEvent(orderId, events, transaction),
            this.__addWallet(orderId, wallets, transaction),
            this.__addBride(orderId, brideInfo, transaction)
        ]).catch(async error => {
            await transaction.rollback()
            throw new ServiceError('failed_add_detail_order', { meta: error })
        })

        await transaction.commit()
        return { message: 'Berhasil menambahkan detail informasi undangan' }
    }

    async getBride (orderId, { isMale = 1, isDetail = 0 } = {}) {
        const bride = await this.OrderBrideRepo.findOne({
            attributes: [
                ['ob_nickname', 'name'],
                ['ob_fullname', 'fullname'],
                ['ob_father_name', 'father_name'],
                ['ob_mother_name', 'mother_name'],
                ['ob_birth_order', 'birth_order'],
                ['ob_order', 'order'],
            ],
            where: { order_id: orderId, ob_type: isMale }
        })
        if (!bride && !isDetail) throw new ServiceError('failed_get_bride')

        return bride?.toJSON() ?? {}
    }

    async __addBride (orderId, brideInfo, transaction = null) {
        const { male: maleInfo, female: femaleInfo } = brideInfo
        const couple = [ this.__brideFormat(orderId, maleInfo), this.__brideFormat(orderId, femaleInfo, 0)]
        await this.OrderBrideRepo.bulkCreate(couple, { transaction })

        return true
    }

    async addBride (orderId, brideInfo, transaction = null) {
        const { bride, groom } = brideInfo
        const couple = [
            this.__brideFormat(orderId, bride),
            this.__brideFormat(orderId, groom, 0)
        ]
        await this.OrderBrideRepo.bulkCreate(couple, { transaction })

        return true
    }

    __brideFormat (orderId, bride, isMale = 1) {
        return {
            order_id: orderId,
            ob_type: isMale,
            ob_nickname: bride.name,
            ob_fullname: bride.fullname,
            ob_father_name: bride.father_name,
            ob_mother_name: bride.mother_name,
            ob_birth_order: bride.birth_order,
            ob_order: bride.order
        }
    }

    async getDetails (orderId) {
        let details = await this.OrderDetailRepo.findOne({
            attributes: [
                ['od_location_url', 'location_url'],
                ['od_calendar_url', 'calendar_url'],
                ['od_stream_url', 'stream_url'],
                ['od_audio_url', 'audio_url'],
                ['od_audio_title', 'audio_title'],
                ['od_youtube_url', 'youtube_url']
            ],
            where: { order_id: orderId },
            raw: true
        })

        if (!details) return {}

        details.audio_url = details.audio_url.includes(BUCKET_BASE_URL) ? await this.__getAudioSigned(orderId) : details.audio_url

        return details
    }

    async getEvent (orderId) {
        const events = await this.OrderEventRepo.findAll({
            attributes: [
                ['oe_name', 'name'],
                ['oe_date', 'date'],
                ['oe_start_time', 'start_time'],
                ['oe_end_time', 'end_time'],
                ['oe_timezone_local', 'tz'],
                ['oe_time', 'time'],
                ['oe_location', 'location'],
                ['oe_location_url', 'location_url'],
                ['oe_address', 'address'],
                [sequelize.fn('IFNULL', sequelize.col('oe_slot'), 0), 'slot']
            ],
            where: { order_id: orderId },
            order: [['oe_slot', 'ASC']],
            raw: true
        })

        return events.map(data => {
            const dt = moment(data.date)
            data.start_time = data.start_time?.slice(0, 5)
            data.end_time = data.end_time?.slice(0, 5)
            data.time = data.time?.toUpperCase()
            data.tz = data.tz?.toUpperCase()
            data.dayname = Dayname[(dt.format('ddd')).toLowerCase()] ?? '-'
            data.monthname = Monthname[(dt.format('MMM')).toLowerCase()] ?? '-'
            return data
        })
    }

    async getWallet (orderId) {
        const wallets = await this.OrderWalletRepo.findAll({
            attributes: [
                ['ow_bank_id', 'bank_id'],
                ['ow_account_number', 'account_number'],
                ['ow_account_name', 'account_name'],
                [sequelize.fn('IFNULL', sequelize.col('ow_order'), 0), 'slot']
            ],
            where: { order_id: orderId },
            order: [['ow_order', 'ASC']]
        })

        return wallets ?? []
    }

    async __addDetail (orderId, details, transaction = null) {
        const audioUrl = await this.__uploadAudio(orderId, details.audio_url)
        await this.OrderDetailRepo.create({
            order_id: orderId,
            od_location_url: details.location_url,
            od_calendar_url: details.calendar_url,
            od_stream_url: details.stream_url,
            od_audio_url: audioUrl,
            od_audio_title: details.audio_title,
            od_youtube_url: details.youtube_url,
        }, { transaction })

        return true
    }

    async __addEvent (orderId, events, transaction = null) {
        if (!events.length) return false

        const eventsModified = events.map(data => {
            return {
                order_id: orderId,
                oe_name: data.name,
                oe_date: data.date,
                oe_time: data.time,
                oe_location: data.location,
                oe_location_url: data.location_url,
                oe_address: data.address,
                oe_slot: data.slot
            }
        })

        try {
            const process = await this.OrderEventRepo.bulkCreate(eventsModified, { transaction })

            if (process.length !== events.length) throw new ServiceError('failed_create_event')
        } catch (error) {
            throw new ServiceError('failed_create_event', { meta: error })
        }

        return true
    }

    async __addWallet (orderId, wallets, transaction = null) {
        if (!wallets.length) return false

        const currentTime = moment().format(FORMAT_DATE)
        const walletsModified = wallets.map(data => {
            const {
                bank_id: ow_bank_id,
                account_number: ow_account_number,
                account_name: ow_account_name,
                slot: ow_order
            } = data

            return {
                order_id: orderId,
                ow_bank_id,
                ow_account_name,
                ow_account_number,
                ow_order,
                ow_created_at: currentTime,
                ow_updated_at: currentTime
            }
        })

        try {
            const process = await this.OrderWalletRepo.bulkCreate(walletsModified, { transaction })

            if (process.length !== walletsModified.length) throw new ServiceError('some_wallet_not_saved')
        } catch (error) {
            throw new ServiceError('failed_save_wallet', { meta: error })
        }

        return true
    }

    async getOrder(whereClause, attributes = null) {
        console.log('WW', whereClause, attributes)
        const order = await this.OrderRepo.findOne({
            attributes,
            where: whereClause
        })
        if (!order) throw new ServiceError('order_not_found')

        return order.toJSON()
    }

    async getGallery (orderId) {
        const gallery = await this.OrderGalleryRepo.findAll({
            attributes: [
                ['og_filename', 'filename'],
                ['og_section', 'section'],
                ['og_url', 'url'],
                ['og_width', 'width'],
                ['og_height', 'height']
            ],
            where: { order_id: orderId },
            raw: true
        })
        const process = gallery.map(async data => {
            const filePath = `image/${orderId}/${data.filename}`
            const signedUrl = await Storage.signedUrl(filePath)
            if (signedUrl) data.url = signedUrl
            return data
        })

        return Promise.all(process)
    }

    async getGreetingCard (subDomain, isPrivate = false) {
        let whereOrderClause = null
        if (isPrivate) {
            whereOrderClause = { order_public_id: subDomain }
        } else {
            whereOrderClause = { order_sub_domain: subDomain }
        }

        const { order_id: orderId } = await this.getOrder(whereOrderClause)
        const greetingCard = await this.OrderGreetingCardRepo.findAll({
            where: {
                order_id: orderId,
                ogc_deleted_at: { [Op.eq]: null }
            },
            order: [['ogc_created_at', 'desc']],
            raw: true
        })

        const result = greetingCard.map(data => {
            const obj = {
                userHadir: data.ogc_is_attend,
                userMsg: data.ogc_message,
                userName: data.ogc_author
            }

            if (isPrivate) {
                obj.greet_id = data.ogc_id
            }

            return obj
        })

        return {
            message: "Berhasil mendapatkan kartu ucapan",
            data: result
        }
    }

    // async getImage (directory, filename) {
    //     const dest = getImageDestination(directory, filename)
    //     var bitmap = fs.readFileSync(dest)
    //     return new Buffer.from(bitmap)
    // }

    async __uploadAudio (orderId, url) {
        if (url.includes(BUCKET_BASE_URL)) return url
        const filename = `audio/${orderId}.mp3`
        return Storage.upload(url, filename)
    }

    async __getAudioSigned (orderId) {
        const filename = `audio/${orderId}.mp3`
        return Storage.signedUrl(filename)
    }

    async hideComment (publicId, id) {
        const { order_id: orderId } = await this.getOrder({ order_public_id: publicId })
        const greetsCard = await this.OrderGreetingCardRepo.findOne({ order_id: orderId, ogc_id: id })

        if (!greetsCard) throw new ServiceError('greets_card_not_found')

        await this.OrderGreetingCardRepo.update({ ogc_deleted_at: moment().format(FORMAT_DATE) }, { where: { order_id: orderId, ogc_id: id } })
        return { message: 'Berhasil menghapus pesan' }
    }

    async removeGallery (userdata, filename, orderId = null) {
        const { id: publicId, isAdmin = false } = userdata
        let orderWhere = null
        if (!isAdmin) {
            orderWhere = { order_public_id: publicId }
        } else {
            orderWhere = { order_public_id: orderId }
        }
        const orderData = await this.getOrder(orderWhere)
        orderId = orderData.order_id

        const orderGallery = await this.OrderGalleryRepo.findOne({
            where: {
                order_id: orderId,
                og_filename: filename,
                og_url: { [Op.like]: '%' + filename }
            }
        })

        if (!orderGallery) throw new ServiceError('image_not_found')

        try {
            const filePath = `image/${orderId}/${filename}`
            const [isSuccess] = await Promise.all([
                Storage.removeGallery(filePath),
                this.OrderGalleryRepo.destroy({ where: {
                    order_id: orderGallery.order_id,
                    og_section: orderGallery.og_section,
                    og_filename: orderGallery.og_filename,
                    og_url: orderGallery.og_url
                } })
            ])
            if (!isSuccess) return { message: `Gambar ${orderGallery.og_section} tidak ditemukan (sudah terhapus)` }
            return { message: `Berhasil menghapus gambar ${orderGallery.og_section}` }
        } catch (error) {
            console.log(error)
            throw new ServiceError('failed_delete_is3')
        }
    }

    async submitCouple (userdata, body) {
        const { order: { order_id: orderId } } = userdata
        const { bride, groom } = body
        bride.type = 1
        const coupleData = [bride, groom].map(data => {
            return {
                order_id: orderId,
                ob_type: data.type,
                ob_nickname: data.name,
                ob_fullname: data.fullname,
                ob_father_name: data.father_name,
                ob_mother_name: data.mother_name,
                ob_birth_order: data.birth_order,
                ob_order: data.order
            }
        })
        await this.OrderBrideRepo.destroy({ where: { order_id: orderId } })
        await this.OrderBrideRepo.bulkCreate(coupleData)

        return { message: 'Berhasil menyimpan informasi mempelai pria dan wanita' }
    }

    async submitBride (userdata, body) {
        const { order: { order_id: orderId } } = userdata
        const { bride, groom } = body
        bride.type = 1
        groom.type = 0
        const coupleData = [bride, groom].map(data => {
            return {
                order_id: orderId,
                ob_type: data.type,
                ob_fullname: data.fullname,
                ob_description: data.description,
                ob_instagram_url: data.instagram_url,
                ob_instagram_username: data.instagram_username
            }
        })

        await this.OrderBrideRepo.destroy({ where: { order_id: orderId } })
        await this.OrderBrideRepo.bulkCreate(coupleData)

        return { message: 'Berhasil menyimpan informasi mempelai pria dan wanita' }
    }

    async submitEvent (userdata, body) {
        const { order: { order_id: orderId } } = userdata
        const { start_time: startTime, end_time: endTime } = body
        const getLastEvent = await this.OrderEventRepo.findOne({
            attributes: [
                ['oe_slot', 'slot'],
            ],
            order: [['oe_slot', 'DESC']],
            raw: true
        })
        let { tz } = body
        let tzLocal = tz, time = null
        switch (tz) {
            case 'wib':
                tz = '+07:00'
                break
            case 'wita':
                tz = '+08:00'
                break
            case 'wit':
                tz = '+09:00'
                break
        }

        if (startTime) {
            time = `${startTime}`
        }

        if (endTime) {
            time = `${time} - ${endTime}`
        } else {
            time = `${time} - <%end%>`
        }

        if (time) time = `${time} ${tzLocal}`

        const payload = {
            order_id: orderId,
            oe_name: body.name,
            oe_date: body.date,
            oe_start_time: body.start_time,
            oe_end_time: body.end_time,
            oe_timezone: tz,
            oe_timezone_local: tzLocal,
            oe_time: time,
            oe_location: body.location,
            oe_location_url: body.location_url,
            oe_address: body.address,
            oe_slot: getLastEvent ? getLastEvent.slot + 1 : 1
        }

        await this.OrderEventRepo.create(payload)

        return { message: 'Berhasil menyimpan daftar acara' }
    }

    async submitEventAll (userdata, events = []) {
        const { order: { order_id: orderId } } = userdata
        let eventFormatted = []
        events.forEach((data, index) => {
            const { start_time: startTime, end_time: endTime } = data
            let { tz } = data
            let tzLocal = tz, time = null
            switch (tz) {
                case 'wib':
                    tz = '+07:00'
                    break
                case 'wita':
                    tz = '+08:00'
                    break
                case 'wit':
                    tz = '+09:00'
                    break
            }

            if (startTime) {
                time = `${startTime}`
            }

            if (endTime) {
                time = `${time} - ${endTime}`
            } else {
                time = `${time} - <%end%>`
            }

            if (time) time = `${time} ${tzLocal}`

            eventFormatted.push({
                order_id: orderId,
                oe_name: data.name,
                oe_date: data.date,
                oe_start_time: data.start_time,
                oe_end_time: data.end_time,
                oe_timezone: tz,
                oe_timezone_local: tzLocal,
                oe_time: time,
                oe_location: data.location,
                oe_location_url: data.location_url,
                oe_address: data.address,
                oe_slot: index
            })
        })

        await this.OrderEventRepo.destroy({ where: { order_id: orderId } })
        await this.OrderEventRepo.bulkCreate(eventFormatted)

        return { message: 'Berhasil menyimpan daftar acara' }
    }

    async submitTheme (userdata, theme, themeColor) {
        const { order: { order_id: orderId } } = userdata

        await this.OrderRepo.update({ order_theme: theme, order_theme_color: themeColor }, { where: { order_id: orderId } })

        return { message: 'Berhasil memilih tema undangan' }
    }

    async submitPackage (userdata, packet) {
        const { order: { order_id: orderId } } = userdata

        await this.OrderRepo.update({ order_package: packet }, { where: { order_id: orderId } })

        return { message: 'Berhasil memilih tema undangan' }
    }

    async createOrder (userId, data) {
        const customerDate = moment().format(FORMAT_DATE)
        const orderData = {
            user_id: userId,
            order_public_id: uuidv4(),
            order_date: customerDate,
            order_package: 1,
            order_religion: data.religion,
            order_wear_hijab: data.is_hijab
        }

        return this.OrderRepo.create(orderData)
    }

    async getOrderDetails (userId) {
        const orderData = await this.OrderRepo.findOne({
            where: { user_id: userId },
            attributes: [
                'order_id',
                'order_status',
                ['order_public_id', 'public_id'],
                ['order_sub_domain', 'sub_domain'],
                ['order_package', 'package'],
                ['order_theme', 'theme'],
                ['order_theme_color', 'theme_color'],
                ['order_event_date', 'event_date'],
                ['order_religion', 'religion'],
                ['order_wear_hijab', 'is_hijab'],
                ['order_main_caption', 'main_caption'],
                ['order_bride_caption', 'bride_caption'],
                ['order_metadata', 'metadata'],
                ['order_payment_status', 'payment_status'],
                ['order_expired_date', 'expired_date']
            ],
            raw: true
        })

        const { order_id: orderId } = orderData ?? {}
        const [details, bride, groom, events, wallets, gallery] = await Promise.all([
            orderId && this.getDetails(orderId),
            orderId && this.getBride(orderId, { isDetail: 1 }),
            orderId && this.getBride(orderId, { isMale: 0, isDetail: 1 }),
            orderId && this.getEvent(orderId),
            orderId && this.getWallet(orderId),
            orderId && this.getGallery(orderId)
        ])

        return {
            ...orderData,
            details,
            bride,
            groom,
            events,
            wallets,
            gallery,
            metadata: jsonParser(orderData?.metadata ?? ''),
            is_expired: moment().isAfter(orderData?.expired_date) ?? false
        }
    }

    async submitDomain (userdata, domain) {
        const { order: { order_id: orderId } } = userdata

        await this.checkAvailableSubDomain(domain, orderId)
        await this.OrderRepo.update({ order_sub_domain: domain }, { where: { order_id: orderId } })

        return { message: `Berhasil menyimpan domain ${domain}` }
    }

    async uploadImage (userdata, body) {
        const { id: userId, order: { order_id: orderId, public_id: userPublicId } } = userdata
        const { public_id: publicId, secure_url: fileUrl, height: fileHeight, width: fileWidth, format: fileFormat } = body
        const [orderPublicId, filename] = publicId.split('/')

        if (orderPublicId !== userPublicId) throw new ServiceError('order_not_found')

        const section = getSectionImage(filename)

        const orderGallery = await this.OrderGalleryRepo.findOne({
            where: {
                og_section: section,
                order_id: orderId
            }
        })

        const logBody = {
            cl_payload: JSON.stringify(body),
            cl_created_at: moment().format(FORMAT_DATE),
        }
        let responseResult = null
        const generatedFilename = getFilenameImage(section, filename, fileFormat)
        try {
            if (section !== 'gallery' && orderGallery) throw new ServiceError('duplicate_image')
            const responseUpload = await Storage.upload(fileUrl, `image/${orderId}/${generatedFilename}`)
            await this.OrderGalleryRepo.create({
                order_id: orderId,
                og_section: section,
                og_filename: generatedFilename,
                og_url: responseUpload,
                og_url_cloudinary: fileUrl,
                og_width: fileWidth,
                og_height: fileHeight
            })

            responseResult = { message: 'Success receive notification callback' }
        } catch (error) {
            logBody.cl_error_webhook = JSON.stringify({ error })
            console.log("CloudinaryErr::", error)
            responseResult = { message: 'Failed receive notification callback' }
        }
        await this.CloudinaryLogRepo.create(logBody)
        responseResult.data = {
        filename: generatedFilename,
            height: fileHeight,
            width: fileWidth,
            section,
            url: await Storage.signedUrl(`image/${orderId}/${generatedFilename}`)
        }
        return responseResult
    }

    async removeImage (userdata, filename) {
        const { order: { order_id: orderId } } = userdata
        const orderGallery = await this.OrderGalleryRepo.findOne({
            where: {
                order_id: orderId,
                og_filename: filename,
                og_url: { [Op.like]: '%' + filename }
            },
            raw: true
        })

        if (!orderGallery) throw new ServiceError('image_not_found')

        try {
            const filePath = `image/${orderId}/${filename}`
            const [isSuccess] = await Promise.all([
                Storage.removeGallery(filePath),
                this.OrderGalleryRepo.destroy({ where: {
                    order_id: orderGallery.order_id,
                    og_section: orderGallery.og_section,
                    og_filename: orderGallery.og_filename,
                    og_url: orderGallery.og_url
                } })
            ])
            if (!isSuccess) return { message: `Gambar ${orderGallery.og_section} tidak ditemukan (sudah terhapus)` }
            return { message: `Berhasil menghapus gambar ${orderGallery.og_section}` }
        } catch (error) {
            console.log(error)
            throw new ServiceError('failed_delete_is3')
        }
    }

    async packageUpgrade (userdata, selectedPackageId) {
        const { id: userId, order: { order_id: orderId, package: currentPackage } } = userdata
        const packageIds = [1, 2, 3] // 1: silver, 2: gold, 3: platinum
        const availablePackage = packageIds.filter(id => id > Number(currentPackage))

        if (!availablePackage.includes(selectedPackageId)) {
            // throw new ServiceError('package_not_available')
            return { message: `Gagal upgrade package, current package (${currentPackage})` }
        }

        await this.OrderRepo.update({ order_package: selectedPackageId }, { where: { order_id: orderId, user_id: userId } })

        const { Main: SectionMain } = require('../section')
        if (selectedPackageId === 2) {
            await Promise.all([
                SectionMain.initiate(userdata, 'countdown'),
                SectionMain.initiate(userdata, 'livestream', { additionalAttribute: { scm_total_items: 3 } }),
                SectionMain.update(userdata, 'gallery', { scm_total_items: 5 }),
                SectionMain.update(userdata, 'felicitation', { scm_metadata: JSON.stringify({ show_result: true, show_confirmation: true }) })
            ])
        }

        if (selectedPackageId === 3) {
            await Promise.all([
                currentPackage !== 2 && SectionMain.initiate(userdata, 'countdown'),
                currentPackage !== 2 && SectionMain.initiate(userdata, 'livestream', { additionalAttribute: { scm_total_items: 3 } }),
                currentPackage !== 2 && SectionMain.update(userdata, 'felicitation', { scm_metadata: JSON.stringify({ show_result: true, show_confirmation: true }) }),
                SectionMain.initiate(userdata, 'wallet', { additionalAttribute: { scm_total_items: 2 } }),
                SectionMain.initiate(userdata, 'story', { additionalAttribute: { scm_total_items: 5 } }),
                SectionMain.initiate(userdata, 'youtube'),
                SectionMain.update(userdata, 'event', { scm_total_items: 3 }),
                SectionMain.update(userdata, 'gallery', { scm_total_items: 10 })
            ])
        }

        return { message: 'Berhasil upgrade package' }
    }
}

module.exports = new Order()