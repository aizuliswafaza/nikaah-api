'use strict'

const moment = require('moment')

const { Sequelize, sequelize } = require('../../models')
const { Op } = Sequelize
const instanceModels = require('../../models/init-models').initModels(sequelize)
const ServiceError = require('../../handler/wrapper/ServiceError')
const RandomString = require('../../helper/randomString')

const Storage = require('../third-party/storage')
const randomString = require('../../helper/randomString')

const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'
const PAY_STATUS = {
    WAITING_PAYMENT: 0,
    WAITING_VALIDATION: 1,
    APPROVED: 2,
    REJECTED: 9
}

class Payment {
    constructor () {
        instanceModels.nk_payments.removeAttribute('id')

        this.PaymentRepo = instanceModels.nk_payments
    }

    async submitManual (userdata, url) {
        const { id: userId } = userdata

        const userPay = await this.PaymentRepo.findOne({ where: {
            user_id: userId,
            pay_type: 'settled',
            pay_status: { [Op.in]: [PAY_STATUS.APPROVED] }
        } })

        if (userPay) throw new ServiceError('payment_has_settled')

        const currentTime = moment().format(FORMAT_DATE)
        const currentTimeUnix = moment(currentTime).unix()
        const filename = `receipt/${userId}/manual-${currentTimeUnix}`
        const fileUrl = await Storage.upload(url, filename)

        await this.PaymentRepo.insert({
            user_id: userId,
            pay_code: 'P123',
            pay_method: 'manual',
            pay_status: PAY_STATUS.WAITING_VALIDATION,
            pay_receipt_url: fileUrl,
            pay_paid_at: currentTime,
            pay_created_at: currentTime
        })
        return { message: 'Berhasil mengirim bukti pembayaranmu' }
    }
}

module.exports = new Payment()