'use strict'

const moment = require('moment')

const { Sequelize, sequelize } = require('../../../models')
const { Op } = Sequelize
const instanceModels = require('../../../models/init-models').initModels(sequelize)

const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'

class AdminOrder {
    constructor () {}
}

module.exports = new AdminOrder()