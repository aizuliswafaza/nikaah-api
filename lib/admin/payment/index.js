'use strict'

const moment = require('moment')

const { Sequelize, sequelize } = require('../../models')
const { Op } = Sequelize
const instanceModels = require('../../models/init-models').initModels(sequelize)
const ServiceError = require('../../handler/wrapper/ServiceError')
const RandomString = require('../../helper/randomString')

const Storage = require('../third-party/storage')

const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'
const PAY_STATUS = {
    WAITING_PAYMENT: 0,
    WAITING_VALIDATION: 1,
    APPROVED: 2,
    REJECTED: 9
}

class AdminPayment {
    constructor () {
        instanceModels.nk_payments.removeAttribute('id')

        this.PaymentRepo = instanceModels.nk_payments
    }

    async getListPayment () {
        const data = await this.PaymentRepo.findAndCountAll({
            attributes: [
                ['pay_id', 'id'],
                ['user_id'],
                ['pay_code', 'code'],
                ['pay_type', 'type'],
                ['pay_method', 'method'],
                ['pay_status', 'status']
            ],
            where: {}
        })

        return data
    }

    async validationPayment (payId, type, total) {
        const currentTime = moment().format(FORMAT_DATE)
        const paymentData = await this.PaymentRepo.findOne({
            where: {
                pay_id: payId,
                pay_status: PAY_STATUS.WAITING_VALIDATION
            }
        })

        if (!paymentData) throw new ServiceError('payment_not_exist')
        const { user_id: userId } = paymentData
        await this.PaymentRepo.update({
            pay_type: type,
            pay_total: total,
            pay_approved_by: 1,
            pay_status: PAY_STATUS.APPROVED,
            pay_updated_at: currentTime,
            pay_approved_at: currentTime
        }, {
            pay_id: payId
        })
        return { message: 'Berhasil melakukan validasi pembayaran' }
    }
}

module.exports = new AdminPayment()