'use strict'

const moment = require('moment')

const { getFilenameImage } = require('../../../../helper/getImagePath')
const ServiceError = require('../../../../handler/wrapper/ServiceError')

const { Sequelize, sequelize } = require('../../../../models')
const { Op } = Sequelize
const instanceModels = require('../../../../models/init-models').initModels(sequelize)

const Storage = require('../../../third-party/storage')

const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'

class MasterBank {
  constructor () {
    this.MusicRepo = instanceModels.nk_musics
  }

  async getList () {
    const banks = await this.BankRepo.findAll({
      attributes: [
        ['bank_id', 'id'],
        ['bank_name', 'name'],
        ['bank_type', 'type'],
        ['bank_status', 'status'],
        ['bank_icon_url', 'icon_url'],
        ['bank_config', 'config'],
      ],
      raw: true
    })
    return { message: 'Berhasil mendapatkan daftar bank', data }
  }

  async submit (body) {
    let { url, format, name, name_origin: nameOrigin } = body
    const filename = getFilenameImage('icon', `asset/music/${name}`, format)
    url = await Storage.upload(url, filename)

    const currentTime = moment().format(FORMAT_DATE)
    const payload = {
      music_title: nameOrigin,
      music_url: url,
      music_created_at: currentTime,
      music_updated_at: currentTime
    }

    await this.MusicRepo.create(payload)

    return {
      message: `Success insert data bank ${name}`
    }
  }
}

module.exports = new MasterBank()