'use strict'

const moment = require('moment')

const { getFilenameImage } = require('../../../../helper/getImagePath')
const ServiceError = require('../../../../handler/wrapper/ServiceError')

const { Sequelize, sequelize } = require('../../../../models')
const { Op } = Sequelize
const instanceModels = require('../../../../models/init-models').initModels(sequelize)

const Storage = require('../../../third-party/storage')

const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'

class MasterBank {
  constructor () {
    this.BankRepo = instanceModels.nk_banks
  }

  async getList () {
    const banks = await this.BankRepo.findAll({
      attributes: [
        ['bank_id', 'id'],
        ['bank_name', 'name'],
        ['bank_type', 'type'],
        ['bank_status', 'status'],
        ['bank_icon_url', 'icon_url'],
        ['bank_config', 'config'],
      ],
      raw: true
    })

    const data = await Promise.all(
      banks.map(async (bank) => {
        const config = JSON.parse(bank.config)
        const iconPath = Storage.getFilePathOnly(bank.icon_url)
        const iconUrl = await Storage.signedUrl(iconPath)
        delete bank.config
        return {
          ...bank,
          ...config,
          icon_url_signed: iconUrl
        }
      })
    )
    return { message: 'Berhasil mendapatkan daftar bank', data }
  }

  async __findByOne (whereClause) {
    const bank = await this.BankRepo.findOne({
      where: whereClause,
      attributes: [
        ['bank_id', 'id'],
        ['bank_name', 'name'],
        ['bank_status', 'status'],
        ['bank_icon_url', 'icon_url'],
        ['bank_config', 'config'],
      ],
      raw: true
    })

    return bank
  }

  async submit (body) {
    const bank = await this.__findByOne({ bank_name: body.name })
    if (bank) throw new ServiceError('duplicate_entry')

    let { icon_url: iconUrl, icon_format: iconFormat } = body
    const bankNameIcon = body.name.toLowerCase().replace(/\ /g, '-')
    const filename = getFilenameImage('icon', `asset/bank/${bankNameIcon}-icon`, iconFormat)
    iconUrl = await Storage.upload(iconUrl, filename, { 'Content-Type': 'image/svg+xml' })

    const currentTime = moment().format(FORMAT_DATE)
    const payload = {
      bank_name: body.name,
      bank_type: body.type,
      bank_icon_url: iconUrl,
      bank_status: body.status,
      bank_config: JSON.stringify({
        allow_qr: body.allow_qr ?? false,
        allow_account_number: body.allow_account_number ?? false,
      }),
      bank_created_at: currentTime,
      bank_updated_at: currentTime
    }

    await this.BankRepo.create(payload)

    return {
      message: `Success insert data bank ${body.name}`
    }
  }

  async update (body) {
    const bank = await this.__findByOne({ bank_name: body.name, bank_id: { [Op.ne]: body.id } })
    if (bank) throw new ServiceError('duplicate_entry')

    let { icon_url: iconUrl, icon_format: iconFormat } = body
    const currentTime = moment().format(FORMAT_DATE)

    if (iconUrl.includes('cloudinary')) {
      const currentBankIconPath = Storage.getFilePathOnly(body.icon_url)
      await Storage.removeGallery(currentBankIconPath)
      const bankNameIcon = body.name.toLowerCase().replace(/\ /g, '-')
      const filename = getFilenameImage('icon', `asset/bank/${bankNameIcon}-icon`, iconFormat)
      iconUrl = await Storage.upload(iconUrl, filename, { 'Content-Type': 'image/svg+xml' })
    } else {
      delete payload.bank_icon
    }

    let payload = {
      bank_name: body.name,
      bank_type: body.type,
      bank_status: body.status,
      bank_icon_url: iconUrl,
      bank_config: JSON.stringify({
        allow_qr: body.allow_qr ?? false,
        allow_account_number: body.allow_account_number ?? false,
      }),
      bank_updated_at: currentTime
    }

    await this.BankRepo.update(payload, { where: { bank_id: body.id } })

    return { message: `Success update data bank ${body.name}` }
  }

  async updateImage (id, url, format) {
    const bank = await this.__findByOne({ bank_id: id })
    const currentBankName = Storage.getFilePathOnly(bank.icon_url)
    const bankNameIcon = bank.name.toLowerCase().replace(/\ /g, '-')
    const filename = getFilenameImage('icon', `asset/bank/${bankNameIcon}-icon`, format)

    await Storage.removeGallery(currentBankName)
    const iconUrl = await Storage.upload(url, filename, { 'Content-Type': 'image/svg+xml' })
    await this.BankRepo.update({ bank_icon_url: iconUrl }, { where: { bank_id: id } })

    return { message: `Success update data bank` }
  }

  async delete (id) {
    const bank = await this.__findByOne({ bank_id: id })
    if (bank) {
      const currentBankName = Storage.getFilePathOnly(bank.icon_url)
      await Promise.all([
        Storage.removeGallery(currentBankName),
        this.BankRepo.destroy({ where: { bank_id: id } })
      ])
    }

    return { message: `Success delete data bank` }
  }
}

module.exports = new MasterBank()