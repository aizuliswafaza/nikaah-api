'use strict'

const MasterBank = require('./bank')
const MasterMusic = require('./music')

module.exports = { MasterBank, MasterMusic }
