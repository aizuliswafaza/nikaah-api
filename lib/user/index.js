'use strict'

const { Sequelize, sequelize } = require('../../models')
const instanceModels = require('../../models/init-models').initModels(sequelize)
const moment = require('moment')
const ServiceError = require('../../handler/wrapper/ServiceError')
const jsonParser = require('../../helper/jsonSafeParser')
const Order = require('../order/main')
const {
    // Greeting: GreetingSection,
    // Homepage: HomepageSection,
    // Bride: BrideSection,
    // Event: EventSection,
    // Felicitation: FelicitationSection,
    Main: SectionMain
} = require('../section')

const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'

class User {
    constructor () {
        instanceModels.nk_users.removeAttribute('id')
        instanceModels.nk_order.removeAttribute('id')

        this.UsersRepo = instanceModels.nk_users
        this.OrderRepo = instanceModels.nk_order
    }

    async setOnboardingState (userdata) {
        const { id: userId, onboarding_state: onboardingState } = userdata
        const STEPPER = ['empty', 'bride', 'event', 'gallery', 'theme', 'domain', 'finished']
        const stepNow = onboardingState + 1
        const stepNowStr = STEPPER[onboardingState]

        if (!stepNowStr) return { message: 'alur onboarding telah selesai' }

        const currentTime = moment().format(FORMAT_DATE)
        await this.UsersRepo.update({ user_onboarding_state: stepNow, user_updated_at: currentTime }, { where: { user_id: userId } })

        return { message: 'Berhasil memperbarui state onboarding' }
    }

    async completeProfile (userData, payload) {
        const now = moment().format(FORMAT_DATE)
        const { code, phone, source, source_other: sourceOther, province, city } = payload
        const { id: userId, status } = userData

        if (status !== 1) throw new ServiceError('forbidden_action')

        await this.UsersRepo.update({
            user_phone: `${code}${phone}`,
            user_source: source,
            user_source_other: source === 0? sourceOther: null,
            user_status: 2,
            city_id: city,
            prov_id: province,
            user_onboarding_state: 1,
            user_updated_at: now
        }, { where: { user_id: userId } })

        await Promise.all([
            Order.createOrder(userId, payload),
            SectionMain.initiate(userData, 'greeting', { isAutoActive: true }),
            SectionMain.initiate(userData, 'homepage', { isAutoActive: true }),
            SectionMain.initiate(userData, 'event', { isAutoActive: true, additionalAttribute: { scm_total_items: 2 } }),
            SectionMain.initiate(userData, 'gallery', { isAutoActive: true, additionalAttribute: { scm_total_items: 2 } }),
            SectionMain.initiate(userData, 'bride', { isAutoActive: true, additionalAttribute: { scm_metadata: JSON.stringify({ reverse_order: false }) } }),
            SectionMain.initiate(userData, 'felicitation', { isAutoActive: true, additionalAttribute: { scm_metadata: JSON.stringify({ show_result: true, show_confirmation: false }) } })
        ])

        return { message: 'Berhasil melengkapi data registrasi' }
    }

    async getProfile (id, bypass = false) {
        const userData = await this.UsersRepo.findOne({
            attributes: [
                ['user_id', 'id'],
                ['user_status', 'status'],
                ['user_fullname', 'fullname'],
                ['user_email', 'email'],
                ['user_phone', 'phone'],
                ['user_source', 'source'],
                ['user_onboarding_state', 'onboarding_state'],
                ['user_source_other', 'source_other'],
                ['user_created_at', 'registered_at'],
                ['user_verified_at', 'verified_at']
            ],
            where: { user_id: id }
        })
        if (!userData) throw new ServiceError('user_not_found')
        const order = await Order.getOrderDetails(id)
        const user = userData.toJSON()
        if (!bypass) {
            delete user.id
            delete order.order_id
        }
        const result = {
            ...user,
            order
        }

        return {
            message: 'Berhasil mendapatkan data profil',
            data: result
        }
    }
}

module.exports = new User()