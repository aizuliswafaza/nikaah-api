'use strict'

const moment = require('moment')

const { Sequelize, sequelize } = require('../../models')
const { Op } = Sequelize
const instanceModels = require('../../models/init-models').initModels(sequelize)
const jsonParser = require('../../helper/jsonSafeParser')
const Order = require('../order/main')

const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'

class Info {
    constructor () {
        instanceModels.nk_order_greeting_card.removeAttribute('id')

        this.OrderGreetingCardRepo = instanceModels.nk_order_greeting_card
    }

    async getWeddingLetter (subDomain) {
        const currentTime = moment().format(FORMAT_DATE)
        const order = await Order.getOrder({
            order_sub_domain: subDomain,
            order_expired_date: { [Op.gte]: currentTime }
        })

        const {
            order_id: orderId,
            order_package: orderPackage,
            order_theme: orderTheme,
            order_theme_color: orderThemeColor,
            order_religion: orderReligion,
            order_cp: orderCp,
            order_email: orderEmail,
            order_event_date: orderEventDate,
            order_main_caption: orderMainCaption,
            order_bride_caption: orderBrideCaption,
            order_metadata: orderMetadata
        } = order

        const [details, bride, groom, events, wallets, gallery] = await Promise.all([
            Order.getDetails(orderId),
            Order.getBride(orderId),
            Order.getBride(orderId, { isMale: 0 }),
            Order.getEvent(orderId),
            Order.getWallet(orderId),
            Order.getGallery(orderId)
        ])

        const result = {
            package: orderPackage,
            theme: orderTheme,
            theme_color: orderThemeColor,
            phone_number: orderCp,
            email: orderEmail,
            religion: orderReligion,
            event_date: orderEventDate,
            main_caption: orderMainCaption,
            bride_caption: orderBrideCaption,
            details,
            bride,
            groom,
            events,
            wallets,
            gallery,
            metadata: JSON.parse(orderMetadata)
        }

        return {
            message: 'Berhasil mendapatkan data pesanan',
            data: result
        }
    }

    async getGreetingCard (subDomain) {
        const { order_id: orderId } = await this.getOrder({ order_sub_domain: subDomain })
        const greetingCard = await this.OrderGreetingCardRepo.findAll({
            where: {
                order_id: orderId,
                ogc_deleted_at: { [Op.eq]: null }
            },
            order: [['ogc_created_at', 'desc']]
        })

        const result = greetingCard.map(data => {
            const obj = {
                userHadir: data.ogc_is_attend,
                userMsg: data.ogc_message,
                userName: data.ogc_author
            }

            if (isPrivate) {
                obj.greet_id = data.ogc_id
            }

            return obj
        })

        return {
            message: "Berhasil mendapatkan kartu ucapan",
            data: result
        }
    }

    async submitGreeting (subDomain, from, isAttend = null, message = '') {
        const { order_id: orderId, order_name: orderName } = await this.getOrder({ order_sub_domain: subDomain })
        const currentTime = moment().format(FORMAT_DATE)
        await this.OrderGreetingCardRepo.create({
            order_id: orderId,
            ogc_author: from,
            ogc_is_attend: isAttend,
            ogc_message: message,
            ogc_created_at: currentTime
        })

        return { message: `Berhasil menambahkan kartu ucapan kepada ${orderName}` }
    }
}

module.exports = new Info()