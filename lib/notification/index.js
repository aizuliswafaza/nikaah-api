'use strict'

const moment = require('moment')
const { Sequelize, sequelize } = require('../../models')
const instanceModels = require('../../models/init-models').initModels(sequelize)
const Order = require('../order/main')
const randomString = require('../../helper/randomString')
const ServiceError = require('../../handler/wrapper/ServiceError')
const Storage = require('../third-party/storage')

const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'

class Notification {
    constructor () {
        instanceModels.nk_order_gallery.removeAttribute('id')

        this.CloudinaryLogRepo = instanceModels.nk_cloudinary_log
        this.OrderGalleryRepo = instanceModels.nk_order_gallery
    }

    async cloudinary (body) {
        const { public_id: publicId, secure_url: fileUrl, height: fileHeight, width: fileWidth } = body
        const [orderPublicId, filename] = publicId.split('/')

        const { order_id: orderId } = await Order.getOrder({ order_public_id: orderPublicId })
        const section = this.__getSection(filename)
        const orderGallery = await this.OrderGalleryRepo.findOne({
            where: {
                og_section: section,
                order_id: orderId
            }
        })
        const logBody = {
            cl_payload: JSON.stringify(body),
            cl_created_at: moment().format(FORMAT_DATE),
        }
        let responseResult = null
        console.log('OrderId', orderId)

        try {
            if (section !== 'gallery' && orderGallery) throw new ServiceError('duplicate_image')
            const generatedFilename = this.__getFilename(section, filename)
            const responseUpload = await Storage.upload(fileUrl, `image/${orderId}/${generatedFilename}`)
            await this.OrderGalleryRepo.create({
                order_id: orderId,
                og_section: section,
                og_filename: generatedFilename,
                og_url: responseUpload,
                og_url_cloudinary: fileUrl,
                og_width: fileWidth,
                og_height: fileHeight
            })

            responseResult = { message: 'Success receive notification callback' }
        } catch (error) {
            logBody.cl_error_webhook = JSON.stringify({ error })
            console.log(error)
            responseResult = { message: 'Failed receive notification callback' }
        }
        await this.CloudinaryLogRepo.create(logBody)
        return responseResult
    }

    __getSection (filename) {
        if (filename.includes('galery') || filename.includes('gallery')) return 'gallery'
        const [trimmed] = filename.split('_photo')
        return trimmed
    }

    __getFilename (section, filename) {
        if (section === 'gallery') return `gallery-${randomString(10)}.jpg`
        return `${filename}.jpg`
    }
}

module.exports = new Notification()