'use strict'

const { v4: uuidv4 } = require('uuid')
const crypto = require('crypto')
const bcrypt = require('bcrypt')
const moment = require('moment')
const MD5 = require('md5')

const { Sequelize, sequelize } = require('../../models')
const instanceModels = require('../../models/init-models').initModels(sequelize)
const Token = require('./token')
const ServiceError = require("../../handler/wrapper/ServiceError")
const {
  user_private_key: USER_PVT_KEY,
  admin_private_key: ADMIN_PVT_KEY
} = require('../../config/app_config.json')
const { env: ENV, ui_based_url: UI_URL } = require('../../config/constants')

const EmailClient = require('../third-party/email/index')

const FORMAT_DATE = 'YYYY-MM-DD HH:mm:ss'

class Auth {
  constructor () {
      instanceModels.nk_user_admin.removeAttribute('id')
      instanceModels.nk_users.removeAttribute('id')

      this.OrderRepo = instanceModels.nk_order
      this.UsersRepo = instanceModels.nk_users
      this.UserVerifRepo = instanceModels.nk_user_verification
      this.UserAdminRepo = instanceModels.nk_user_admin
  }

  async adminLogin (username, password) {
      const admin = await this.UserAdminRepo.findOne({
          where: { user_admin_email: username }
      })

      if (!admin) throw new ServiceError('user_not_found')

      const { user_admin_id: userAdminId, user_admin_password: hashedPassword } = admin
      const isPasswordValid = await bcrypt.compareSync(password, hashedPassword)

      if (!isPasswordValid) throw new ServiceError('failed_login')

      const jwt = Token.generate({ id: uuidv4() }, ADMIN_PVT_KEY)
      return {
        message: 'Berhasil login sebagai admin',
        data: {
            ...jwt
        }
      }
  }

  async login (publicId, password) {
    const order = await this.OrderRepo.findOne({
      where: { order_public_id: publicId }
    })

    if (!order) throw new ServiceError('user_not_found')

    const { order_sub_domain: orderPassword } = order || {}
    const hashedOrderPassword = crypto.createHash('md5').update(orderPassword).digest('hex')
    const hashedPassword = crypto.createHash('md5').update(password).digest('hex')
    if (hashedOrderPassword !== hashedPassword) throw new ServiceError('failed_login')

    const jwt = Token.generate({ id: publicId }, USER_PVT_KEY)
    return {
      message: 'Berhasil login',
      data: {
          ...jwt
      }
    }
  }

  async clientLogin (email, password, isRemembered = false) {
    const userData = await this.UsersRepo.findOne({ where: { user_email: email }})
    if (!userData) throw new ServiceError('user_not_found')
    const { user_id: userId, user_status: userStatus, user_password: userPassword } = userData
    if (userStatus < 1) throw new ServiceError('user_not_verified')
    const isValid = bcrypt.compareSync(password, userPassword)
    if (!isValid) throw new ServiceError('failed_login')
    let expiredAt = null
    if (isRemembered) expiredAt = moment().add(1, 'month').unix()

    const jwt = Token.generate({ id: userId }, USER_PVT_KEY, expiredAt)

    return { message: 'Berhasil login', data: { ...jwt } }
  }

  async register (fullname, email, password) {
    const isExist = await this.UsersRepo.findOne({ where: { user_email: email } })
    if (isExist) throw new ServiceError('duplicate_entry')

    const token = uuidv4()
    const expiredVerifFormatted = moment().add(7, 'days').format(FORMAT_DATE)
    const nowFormatted = moment().format(FORMAT_DATE)
    password = bcrypt.hashSync(password, 10)
    const body = {
      user_fullname: fullname,
      user_email: email,
      user_password: password,
      user_created_at: nowFormatted
    }

    const transaction = await sequelize.transaction()
    try {
        const userdata = await this.UsersRepo.create(body, { transaction, returning: true })
        const { user_id: userId } = userdata
        await this.UserVerifRepo.create({
          user_id: userId,
          uvrf_type: 'regis_verif',
          uvrf_token: token,
          uvrf_status: 0,
          uvrf_created_at: nowFormatted,
          uvrf_expired_at: expiredVerifFormatted
        }, { transaction })

        // sending email with hashed token`
        const verifKey = `${userId}:${email}:verif_email`
        const verifKeyHashed = MD5(verifKey, 10)
        const responseEmail = await EmailClient.send('email_verify', email, { fullname, url: this.getUrlVerify(`register/verify/${verifKeyHashed}/${token}`) })

        await transaction.commit()
        return {
          message: `Berhasil melakukan pendaftaran pengguna ${fullname}`,
          data: ENV !== 'production' && typeof responseEmail === 'object' && responseEmail.data ? responseEmail.data : null
        }
    } catch (error) {
      await transaction.rollback()
      console.log('Check Failed', error)
      throw new ServiceError('failed_register')
    }
  }

  async verify (key, token) {
    const userVerifData = await this.UserVerifRepo.findOne({
      where: {
        uvrf_token: token,
        uvrf_type: 'regis_verif'
      },
      include: {
        attributes: ['user_email', 'user_created_at', 'user_status'],
        model: this.UsersRepo,
        as: 'user'
      },
      order: [['uvrf_id', 'ASC']]
    })

    if (!userVerifData) throw new ServiceError('user_not_found')
    const { user: userData, user_id: userId, uvrf_status: userVerifstatus } = userVerifData
    const { user_email: userEmail, user_status: userStatus } = userData
    if ( userVerifstatus !== 0 || userStatus !== 0) throw new ServiceError('no_longer_valid')

    const verifKey = `${userId}:${userEmail}:verif_email`
    const verifKeyHashed = MD5(verifKey)
    const isValid = key === verifKeyHashed
    if (!isValid) throw new ServiceError('key_invalid')

    const now = moment().format(FORMAT_DATE)
    const transaction = await sequelize.transaction()
    const updateUserVerifRepo = this.UserVerifRepo.update({ uvrf_status: 1, uvrf_used_at: now }, { where: { user_id: userId, uvrf_token: token, uvrf_type: 'regis_verif' }, transaction })
    const updateUsersRepo = this.UsersRepo.update({ user_status: 1, user_updated_at: now, user_verified_at: now }, { where: { user_id: userId }, transaction })
    await Promise.all([
        updateUserVerifRepo,
        updateUsersRepo
    ]).catch(async err => {
        console.error('FailedUpdateUserData::', err)
        await transaction.rollback()
        throw new ServiceError('failed_verification')
    })
    await transaction.commit()

    return { message: 'Berhasil melakukan verifikasi' }
  }

  async requestForgetPassword (email) {
    const userData = await this.UsersRepo.findOne({ where: { user_email: email } })
    if (!userData) throw new ServiceError('user_not_found')

    const userVerif = await this.UserVerifRepo.findOne({ where: { user_id: userData.user_id, uvrf_type: 'forgot_password', uvrf_status: 0 } })
    const now = moment().format(FORMAT_DATE)
    const expiredDate = moment().add(7, 'weeks').format(FORMAT_DATE)
    const token = uuidv4()
    let data = null

    if (userVerif) {
      await this.UserVerifRepo.update({ uvrf_expired_at: expiredDate }, { where: { uvrf_id: userVerif.uvrf_id } })
    } else {
      await this.UserVerifRepo.create({
        user_id: userData.user_id,
        uvrf_type: 'forgot_password',
        uvrf_token: token,
        uvrf_status: 0,
        uvrf_created_at: now,
        uvrf_expired_at: expiredDate
      })
    }

    const verifKey = `${userData.user_id}:forgot_password`
    const verifKeyHashed = MD5(verifKey)
    const responseEmail = await EmailClient.send('forgot_password', email, { fullname: userData.user_fullname, url: this.getUrlVerify(`reset-password/${verifKeyHashed}/${token}`) })
    data = (ENV !== 'production' && typeof responseEmail === 'object' && responseEmail.data) ? responseEmail.data : null

    return {
      message: 'Berhasil membuat permohonan lupa password',
      data
    }
  }

  async verifyForgetPassword (key, token, password) {
    const userVerifData = await this.UserVerifRepo.findOne({
        where: {
            uvrf_token: token,
            uvrf_type: 'forgot_password',
            uvrf_status: 0
        },
        order: [['uvrf_id', 'ASC']]
    })

    if (!userVerifData) throw new ServiceError('user_not_found')
    const { user_id: userId, uvrf_id: uvrfId } = userVerifData
    const verifKey = `${userId}:forgot_password`
    const verifKeyHashed = MD5(verifKey)
    const isValid = key === verifKeyHashed
    if (!isValid) throw new ServiceError('token_invalid')

    const now = moment().format(FORMAT_DATE)
    password = bcrypt.hashSync(password, 10)
    const transaction = await sequelize.transaction()
    await Promise.all([
      this.UserVerifRepo.update({ uvrf_status: 1, uvrf_used_at: now }, { where: { uvrf_id: uvrfId }, transaction }),
      this.UsersRepo.update({ user_password: password, user_updated_at: now }, { where: { user_id: userId }, transaction })
    ]).catch(async err => {
      await transaction.rollback()
      throw new ServiceError('failed_update_password')
    })
    await transaction.commit()

    return { message: 'Berhasil mengubah password' }
  }

  getUrlVerify (path, bypass = false) {
    return `${UI_URL[ENV]}/${path}`
    // if (ENV !== 'production' || bypass) {
    //   return `${UI_URL[ENV]}/${path}`
    // }

    // return null
  }

  async generatePassword () {
    return bcrypt.hashSync('Ridwansukses2020', 10)
  }
}

module.exports = new Auth()
