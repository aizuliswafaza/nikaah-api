'use strict'

const JWT = require('jsonwebtoken')
const moment = require('moment')
const { v4: uuidv4 } = require('uuid')
const ServiceError = require('../../handler/wrapper/ServiceError')
const APP_ISS = 'https://nikaah.id'
const APP_AUD = 'https://nikaah-api.herokuapp.com'

class Token {
    generate (user, signature, tokenExpiredAt = null) {
        if (!tokenExpiredAt) tokenExpiredAt = moment().add(4, 'hours').unix()
        const { id: userId } = user
        const currentTimestamp = moment().unix()
        const accessIdentity = userId + uuidv4()
        const payload = {
            iat: currentTimestamp,
            jti: accessIdentity,
            iss: APP_ISS,
            aud: APP_AUD,
            user: { id: userId },
            exp: tokenExpiredAt,
            type: 'access_token'
        }

        const accessToken = JWT.sign(payload, signature, {
            algorithm: 'HS256'
        })

        return {
            type: 'Bearer',
            issued_at: currentTimestamp,
            jti: accessIdentity,
            access_token: accessToken,
            access_token_expired_at: tokenExpiredAt
        }
    }

    decode (token) {
        const decoded = JWT.decode(token, { complete: true })
        return decoded
    }

    verify (token, key) {
        try {
            const tokenVerify = JWT.verify(token, key, {
                algorithms: ['HS256'],
                issuer: [APP_ISS],
                audience: [APP_AUD]
            })
            return tokenVerify
        } catch (error) {
            return null
        }
    }
}

module.exports = new Token()