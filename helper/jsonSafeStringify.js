'use strict'

module.exports = str => {
    if (!str) return null
    try {
        return JSON.stringify(str)
    } catch (error) {
        return null
    }
}