'use strict'

const env = process.env.NODE_ENV

const randomString = require('../helper/randomString')

function getUploadDestionation (orderId) {
    if (!orderId) return null
    switch (env) {
        case 'local':
            return `${process.cwd()}\\public\\${orderId}\\`
        case 'development':
            return 'cloudinary'
        case 'production':
            return `${process.cwd()}/public/${orderId}/`
        default:
            return null
    }
}

function getImageDestination (dirname, filename) {
    if (!dirname || !filename) return null
    switch (env) {
        case 'local':
            return `${process.cwd()}\\public\\${dirname}\\${filename}`
        case 'development':
            return null
        case 'production':
            return `${process.cwd()}/public/${dirname}/${filename}`
        default:
            return null
    }
}

function getFilenameFromUrl (url) {
    switch (env) {
        case 'local':
            return url.split('\\').pop()
        case 'development':
            return null
        case 'production':
            return url.split('/').pop()
        default:
            return null
    }
}

function getSectionImage (filename) {
    // if (filename.includes('galery') || filename.includes('gallery')) return 'gallery'
    const [trimmed] = filename.split('_photo')
    return trimmed
}

function getFilenameImage (section, filename, format = 'jpg') {
    if (section === 'gallery') filename = `gallery-${randomString(10)}`
    return `${filename}.${format}`
}

module.exports = { getUploadDestionation, getImageDestination, getFilenameFromUrl, getSectionImage, getFilenameImage }