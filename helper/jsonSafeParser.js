'use strict'

module.exports = str => {
    try {
        return JSON.parse(str)
    } catch (error) {
        return null
    }
}