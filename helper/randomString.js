'use strict'

module.exports = val => {
  if (val < 0 || typeof val !== 'number') return ''
  return Math.random().toString(36).substring(2,(2 + val))
}