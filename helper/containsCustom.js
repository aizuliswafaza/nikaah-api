'use strict'

function hasUppercase (str) {
    return /[A-Z]/.test(str)
}

function hasLowercase (str) {
    return /[a-z]/.test(str)
}

function hasNumeric (str) {
    return /\d/.test(str)
}

module.exports = { hasUppercase, hasLowercase, hasNumeric }