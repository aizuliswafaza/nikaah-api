'use strict'

module.exports = str => {
  let splitedString = null
  try {
    splitedString = str.split(' ')
  } catch (err) {
    splitedString = []
  }
  return splitedString
}