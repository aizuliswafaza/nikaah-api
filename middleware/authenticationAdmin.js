'use strict'

const moment = require('moment')
const spliter = require('../helper/tokenSpliter')
const Token = require('../lib/auth/token')
const ERR_FAIL_ACCESS = 'token incorrect'
const ALLOWED_TOKEN_TYPE = ['Bearer']
const ADMIN_PVT_KEY = require('../config/app_config.json')['admin_private_key']
const USER_PVT_KEY = require('../config/app_config.json')['user_private_key']

module.exports = (req, res, next) => {
    const Authorization = req.get('Authorization')
    const SplitAuthorization = spliter(Authorization)

    if (SplitAuthorization.length !== 2) return res.status(401).json({ message: ERR_FAIL_ACCESS })

    const [type, value] = SplitAuthorization
    if (ALLOWED_TOKEN_TYPE.indexOf(type) < 0) { return res.status(401).json({ message: ERR_FAIL_ACCESS }) }

    let verify = Token.verify(value ,ADMIN_PVT_KEY)
    const now = moment().unix()

    if (!verify) {
        verify = Token.verify(value, USER_PVT_KEY)
        if (verify) return next()
        return res.status(401).json({ message: ERR_FAIL_ACCESS })
    }

    const { exp: expAt, user } = verify
    if (now > expAt) {
        return res.status(401).json({ message: 'token expired' })
    }

    user.isAdmin = true
    req.userdata = user

    console.log('UserData::', req.userdata)

    next()
}