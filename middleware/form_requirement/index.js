'use strict'

const { validationMiddleware } = require('./util')

module.exports = validationMiddleware({
  Admin: require('./admin'),
  OrderLoveStory: require('./orderLoveStory'),
  OrderWallet: require('./orderWallet'),
  OrderUrl: require('./orderUrl'),
  Notification: require('./notification'),
  Auth: require('./auth'),
  User: require('./user'),
  Section: require('./section'),
  Payment: require('./payment')
})
