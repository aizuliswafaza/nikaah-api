'use strict'

const { body } = require('express-validator')

module.exports = {
  submit: [
    body('order')
      .exists().withMessage('Nomor urutan dompet digital tidak boleh kosong!')
      .isNumeric().withMessage('Format nomor urutan dompet digital tidak didukung!'),
    body('bank_id')
      .exists().withMessage('Nama bank tidak boleh kosong!')
      .not().isEmpty().withMessage('Nama bank harus diisi!'),
    body('account_name')
      .exists().withMessage('Nama pemilik rekening tidak boleh kosong!')
      .not().isEmpty().withMessage('Nama pemilik rekening harus diisi!'),
    body('account_number')
      .optional({ checkFalsy: true })
      .not().isEmpty().withMessage('Nama pemilik rekening harus diisi!')
      .isString().withMessage('Format nama pemilik rekening tidak didukung'),
    body('qr_code')
      .optional({ checkFalsy: true })
      .if((val, { req }) => val.includes('cloudinary'))
      .not().isEmpty().withMessage('QrCode harus diisi!')
      .isURL().withMessage('QrCode tidak didukung'),
    body('qr_code_format')
      .if((val, { req }) => req.body.qr_code.includes('cloudinary') && !!req.body.qr_code)
      .exists().withMessage('QrCode format tidak boleh kosong!')
      .not().isEmpty().withMessage('QrCode format harus diisi!')
  ],
  update: [
    body('id')
      .exists().withMessage('Dompet ID tidak boleh kosong!')
      .not().isEmpty().withMessage('Dompet ID harus diisi!'),
    body('bank_id')
      .exists().withMessage('Nama bank tidak boleh kosong!')
      .not().isEmpty().withMessage('Nama bank harus diisi!'),
    body('account_name')
      .exists().withMessage('Nama pemilik rekening tidak boleh kosong!')
      .not().isEmpty().withMessage('Nama pemilik rekening harus diisi!'),
    body('account_number')
      .optional({ checkFalsy: true })
      .if((val, { req }) => req.body.icon_url.includes('cloudinary'))
      .not().isEmpty().withMessage('Nama pemilik rekening harus diisi!')
      .isString().withMessage('Format nama pemilik rekening tidak didukung'),
    body('qr_code')
      .if((val, { req }) => req.body.icon_url.includes('cloudinary') && !!req.body.qr_code)
      .exists().withMessage('QrCode format tidak boleh kosong!')
      .not().isEmpty().withMessage('QrCode harus diisi!')
      .isURL().withMessage('QrCode tidak didukung')
  ],
  updateOrder: [
    body('wallets')
      .exists().withMessage('Urutan dompet digital tidak boleh kosong!')
      .isArray({ max: 5 }).withMessage('Format dompet digital tidak didukung!'),
    body('wallets.*.id')
      .exists().withMessage('Id dompet digital tidak boleh kosong!')
      .isNumeric().withMessage('Format id dompet digital tidak didukung!'),
    body('wallets.*.order')
      .exists().withMessage('Nomor urutan dompet digital tidak boleh kosong!')
      .isNumeric().withMessage('Format nomor urutan dompet digital tidak didukung!'),
  ],
  delete: [
    body('id')
      .exists().withMessage('Dompet ID tidak boleh kosong!')
      .not().isEmpty().withMessage('Dompet ID harus diisi!')
      .isNumeric().withMessage('Format Dompet ID tidak didukung!')
  ]
}