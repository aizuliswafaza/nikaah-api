'use strict'

const { body } = require('express-validator')

module.exports = {
  music: {
    set: [
      body('is_active')
        .exists().withMessage('Nilai aktif tidak boleh kosong!')
        .not().isEmpty().withMessage('Nilai aktig harus diisi!')
        .isBoolean().withMessage('Format nilai aktif tidak didukung!')
    ],
    submit: [
      body('title')
        .exists().withMessage('Judul lagu tidak boleh kosong!')
        .not().isEmpty().withMessage('Judul lagu wajib diisi!')
        .isString().withMessage('Format judul lagu tidak didukung!'),
      body('url')
        .exists().withMessage('Tautan lagu tidak boleh kosong!')
        .not().isEmpty().withMessage('Tautan lagu harus diisi!')
        .isURL().withMessage('Tautan lagu tidak didukung!')
    ]
  },
  youtube: {
    submit: [
      body('url')
        .exists().withMessage('Tautan youtube tidak boleh kosong!')
        .not().isEmpty().withMessage('Tautan youtube harus diisi!')
        .isURL().withMessage('Tautan youtube tidak didukung!')
    ]
  }
}