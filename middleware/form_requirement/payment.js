'use strict'

const { header, body } = require('express-validator')
const moment = require('moment')

const { hasUppercase, hasLowercase, hasNumeric} = require('../../helper/containsCustom')

module.exports = {
  submitManual: [
    body('type')
      .exists().withMessage('Tipe pembayaran tidak boleh kosong!')
      .not().isEmpty().withMessage('type harus diisi!')
      .isIn(['settled', 'installment']).withMessage('Nilai tidak tersedia [settled | installment]'),
    body('url')
      .exists().withMessage('Url tidak boleh kosong!')
      .not().isEmpty().withMessage('url harus diisi!')
      .isURL().withMessage('Format tidak didukung')
  ]
}