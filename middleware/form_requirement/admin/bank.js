'use strict'

const { body } = require('express-validator')

module.exports = {
  submit: [
    body('name')
      .exists().withMessage('Nama bank tidak boleh kosong!')
      .not().isEmpty().withMessage('Nama bank harus diisi!'),
    body('type')
      .exists().withMessage('Tipe bank tidak boleh kosong!')
      .not().isEmpty().withMessage('Tipe bank harus diisi!')
      .isIn(['bank', 'ewallet']).withMessage('Nilai tidak tersedia [bank | ewallet]'),
    body('icon_url')
      .exists().withMessage('Url ikon bank tidak boleh kosong!')
      .not().isEmpty().withMessage('Url ikon bank harus diisi!')
      .isURL().withMessage('Format url ikon bank tidak didukung'),
    body('icon_format')
      .exists().withMessage('Format ikon bank tidak boleh kosong!')
      .not().isEmpty().withMessage('Format ikon bank harus diisi!')
      .isIn(['png', 'svg']).withMessage('Nilai tidak tersedia [svg | png]'),
    body('status')
      .exists().withMessage('Status bank tidak boleh kosong!')
      .not().isEmpty().withMessage('Status bank harus diisi!')
      .isIn([0, 1]).withMessage('Nilai tidak tersedia [0 (zero) | 1 (one)]'),
    body('allow_qr')
      .exists().withMessage('Config QR tidak boleh kosong!')
      .not().isEmpty().withMessage('Config QR harus diisi!')
      .isBoolean().withMessage('Config QR harus berupa boolean!'),
    body('allow_account_number')
      .exists().withMessage('Config Nomor Rekening tidak boleh kosong!')
      .not().isEmpty().withMessage('Config Nomor Rekening harus diisi!')
      .isBoolean().withMessage('Config Nomor Rekening harus berupa boolean!')
  ],
  update: [
    body('id')
      .exists().withMessage('Bank ID tidak boleh kosong!')
      .not().isEmpty().withMessage('Bank ID harus diisi!'),
    body('name')
      .exists().withMessage('Nama bank tidak boleh kosong!')
      .not().isEmpty().withMessage('Nama bank harus diisi!'),
    body('type')
      .exists().withMessage('Tipe bank tidak boleh kosong!')
      .not().isEmpty().withMessage('Tipe bank harus diisi!')
      .isIn(['bank', 'ewallet']).withMessage('Nilai tidak tersedia [bank | ewallet]'),
    body('icon_url')
      .exists().withMessage('Url ikon bank tidak boleh kosong!')
      .not().isEmpty().withMessage('Url ikon bank harus diisi!')
      .isURL().withMessage('Format url ikon bank tidak didukung'),
    body('icon_format')
      .if((val, { req }) => req.body.icon_url.includes('cloudinary'))
      .exists().withMessage('Format ikon bank tidak boleh kosong!')
      .not().isEmpty().withMessage('Format ikon bank harus diisi!')
      .isIn(['png', 'svg']).withMessage('Nilai tidak tersedia [svg | png]'),
    body('status')
      .exists().withMessage('Status bank tidak boleh kosong!')
      .not().isEmpty().withMessage('Status bank harus diisi!')
      .isIn([0, 1]).withMessage('Nilai tidak tersedia [0 (zero) | 1 (one)]'),
    body('allow_qr')
      .exists().withMessage('Config QR tidak boleh kosong!')
      .not().isEmpty().withMessage('Config QR harus diisi!')
      .isBoolean().withMessage('Config QR harus berupa boolean!'),
    body('allow_account_number')
      .exists().withMessage('Config Nomor Rekening tidak boleh kosong!')
      .not().isEmpty().withMessage('Config Nomor Rekening harus diisi!')
      .isBoolean().withMessage('Config Nomor Rekening harus berupa boolean!')
  ],
  delete: [
    body('id')
      .exists().withMessage('Bank ID tidak boleh kosong!')
      .not().isEmpty().withMessage('Bank ID harus diisi!')
  ]
}