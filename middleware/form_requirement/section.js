'use strict'

const { body } = require('express-validator')
const generalSetActiveRequirement = [
  body('is_active')
    .exists().withMessage('Parameter ini tidak boleh kosong!')
    .notEmpty().withMessage('Parameter ini harus diisi!')
    .isBoolean().withMessage('Format tidak didukung! (boolean)')
]
const generalSubmitRequirement = [
  body('title')
    .optional({ checkFalsy: true })
    .notEmpty().withMessage('Parameter ini harus diisi!')
    .isString().withMessage('Format tidak didukung! (string)'),
  body('description')
    .optional({ checkFalsy: true })
    .notEmpty().withMessage('Parameter ini harus diisi!')
    .isString().withMessage('Format tidak didukung! (string)'),
]

module.exports = {
  general_set_activation: generalSetActiveRequirement,
  general_submit: generalSubmitRequirement,
  homepage: {
    submit: [
      ...generalSubmitRequirement,
      body('sub_title')
        .optional({ checkFalsy: true })
        .notEmpty().withMessage('Parameter ini harus diisi!')
        .isString().withMessage('Format tidak didukung! (string)'),
      body('datetime')
        .optional({ checkFalsy: true })
        .notEmpty().withMessage('Parameter ini harus diisi!')
        .isDate().withMessage('Format tidak didukung! (datetime)'),
    ]
  },
  greeting: {
    submit: [
      ...generalSubmitRequirement,
      body('total_sessions')
        .optional({ checkFalsy: true })
        .notEmpty().withMessage('Parameter ini harus diisi!')
        .isNumeric().withMessage('Format tidak didukung! (number)'),
    ]
  },
  bride: {
    submit: [
      ...generalSubmitRequirement,
      body('reverse_order')
        .exists().withMessage('Parameter ini tidak boleh kosong!')
        .notEmpty().withMessage('Parameter ini harus diisi!')
        .isBoolean().withMessage('Format tidak didukung! (boolean)'),
      body('hide_image')
        .exists().withMessage('Parameter ini tidak boleh kosong!')
        .notEmpty().withMessage('Parameter ini harus diisi!')
        .isBoolean().withMessage('Format tidak didukung! (boolean)')
    ]
  },
  countdown: {
    submit: [
      ...generalSubmitRequirement,
      body('datetime')
        .optional({ checkFalsy: true })
        .notEmpty().withMessage('Parameter ini harus diisi!')
        .isDate().withMessage('Format tidak didukung! (datetime)')
    ]
  },
  felicitation: {
    submit: [
      ...generalSubmitRequirement,
      body('show_confirmation')
        .exists().withMessage('Parameter ini tidak boleh kosong!')
        .notEmpty().withMessage('Parameter ini harus diisi!')
        .isBoolean().withMessage('Format tidak didukung! (boolean)')
    ]
  },
  custom: {
    update: [
      ...generalSubmitRequirement,
      body('id')
        .exists().withMessage('Parameter ini tidak boleh kosong!')
        .notEmpty().withMessage('Parameter ini harus diisi!')
        .isNumeric().withMessage('Format tidak didukung! (number)')
    ],
    remove: [
      body('id')
        .exists().withMessage('Parameter ini tidak boleh kosong!')
        .notEmpty().withMessage('Parameter ini harus diisi!')
        .isNumeric().withMessage('Format tidak didukung! (number)')
    ]
  }
}