'use strict'

const { body } = require('express-validator')

const OrderLoveStory = require('../../lib/orderLoveStory')

const {
  bucket: {
    BASE_URL: BUCKET_BASE_URL
  }
} = require('../../config/constants')

module.exports = {
  submit: [
    body('title')
      .optional({ checkFalsy: true })
      .isString().withMessage('Format judul love story tidak didukung!'),
    body('image_url')
      .optional({ checkFalsy: true })
      .isURL().withMessage('Format tautan gambar tidak didukung!')
      .custom(val => val.includes('cloudinary')).withMessage('Source tautan gambar tidak dikenal'),
    body('description')
      .optional({ checkFalsy: true })
      .isString().withMessage('Format deskripsi love story tidak didukung!'),
    body('order')
      .exists().withMessage('Nomor urutan love story tidak boleh kosong!')
      .isNumeric().withMessage('Format nomor urutan love story tidak didukung!')
  ],
  update: [
    body('id')
      .exists().withMessage('Id love story tidak boleh kosong!')
      .notEmpty().withMessage('Id love story harus diisi!')
      .isNumeric().withMessage('Format id love story tidak didukung!'),
    body('title')
      .optional({ checkFalsy: true })
      .isString().withMessage('Format judul love story tidak didukung!'),
    body('image_url')
      .optional({ checkFalsy: true })
      .isURL().withMessage('Format tautan gambar tidak didukung!')
      .if(val => !val.includes(BUCKET_BASE_URL))
      .custom(val => val.includes('cloudinary')).withMessage('Source tautan gambar tidak dikenal'),
    body('description')
      .optional({ checkFalsy: true })
      .isString().withMessage('Format deskripsi love story tidak didukung!')
  ],
  updateOrder: [
    body('stories')
      .exists().withMessage('Urutan love story tidak boleh kosong!')
      .isArray({ max: 5 }).withMessage('Format urutan love story tidak didukung!')
      .custom(async (val, { req }) => {
        const { order: { order_id: orderId } } = req.userdata
        const total = await OrderLoveStory.count(orderId)
        if (val.length !== total) throw new Error('Jumlah love story tidak sesuai dengan yang sudah didaftarkan!')
        return true
      }),
    body('stories.*.id')
      .exists().withMessage('Id love story tidak boleh kosong!')
      .isNumeric().withMessage('Format id love story tidak didukung!'),
    body('stories.*.order')
      .exists().withMessage('Nomor urutan love story tidak boleh kosong!')
      .isNumeric().withMessage('Format nomor urutan love story tidak didukung!'),
  ],
  delete: [
    body('id')
      .exists().withMessage('Id love story tidak boleh kosong!')
      .not().isEmpty().withMessage('Id love story harus diisi!')
      .isNumeric().withMessage('Id love story tidak didukung!')
  ]
}