'use strict'

const { header, body } = require('express-validator')
const moment = require('moment')

const Tool = require('../../lib/tool/index')
const { hasUppercase, hasLowercase, hasNumeric} = require('../../helper/containsCustom')
const { map } = require('lodash')
const availableRegion = ['62']

module.exports = {
  complete_register: [
    body('code')
      .exists().withMessage('Kode region tidak boleh kosong!')
      .not().isEmpty().withMessage('Kode region harus diisi!')
      .custom(val => {
        if (!availableRegion.includes(val)) return Promise.reject('Kode region tersebut belum didukung')
        return true
      }),
    body('phone')
      .exists().withMessage('Nomor telefon tidak boleh kosong!')
      .not().isEmpty().withMessage('Nomor telefon harus diisi!')
      .isLength({ max: 14, min: 10 }).withMessage('Nomor telefon minimal 8 karakter dan maksimal 20 karakter!'),
    body('source')
      .exists().withMessage('Sumber informasi tidak boleh kosong!')
      .not().isEmpty().withMessage('Sumber informasi harus diisi!')
      .custom(val => {
        const { data: sources } = Tool.getSource()
        const sourceMapped = sources.map(source => source.id)
        if (!sourceMapped.includes(val)) return Promise.reject('Sumber informasi tidak tersedia')
        return true
      }),
    body('source_other')
      .custom((val, { req }) => {
        const { source } = req.body
        if (source === 0 && !val) return Promise.reject('Keterangan sumber informasi lainnya harus diisi')
        return true
      }),
    body('province')
      .exists().withMessage('Provinsi tidak boleh kosong!')
      .not().isEmpty().withMessage('Provinsi harus diisi!')
      .isNumeric().withMessage('Format Provinsi tidak sesuai!')
      .custom(async val => {
        const provs = await Tool.getProv({ prov_id: val })
        if (provs.length < 1) return Promise.reject('Data Provinsi tidak ditemukan!')
        return true
      }),
    body('city')
      .exists().withMessage('Kota/Kabupaten tidak boleh kosong!')
      .not().isEmpty().withMessage('Kota/Kabupaten harus diisi!')
      .isNumeric().withMessage('Format Kota/Kabupaten tidak sesuai!')
      .custom(async (val, { req }) => {
        const { province } = req.body
        const cities = await Tool.getCity(province, val)
        if (cities.length < 1) return Promise.reject('Data Kota/Kabupaten tidak ditemukan!')
        return true
      }),
    body('religion')
      .exists().withMessage('Agama tidak boleh kosong!')
      .not().isEmpty().withMessage('Agama harus diisi!'),
    body('is_hijab')
      .optional({ checkFalsy: true })
      .isBoolean().withMessage('Format keterangan hijab tidak sesuai!')
  ]
}