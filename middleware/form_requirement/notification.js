'use strict'

const { header, body } = require('express-validator')
const moment = require('moment')

module.exports = {
    cloudinary: [
        header('X-Cld-Timestamp')
            .exists().withMessage('X-Cld-Timestamp harus diisi'),
        header('X-Cld-Signature')
            .exists().withMessage('X-Cld-Signature harus diisi')
            .custom((val, { req }) => {
                const userAgent = req.headers['user-agent']
                // if (userAgent !== 'Cloudinary') return Promise.reject({ errorType: 'Not Found' })
                return true
            })
    ]
}