'use strict'

const { header, body } = require('express-validator')
const moment = require('moment')

const { hasUppercase, hasLowercase, hasNumeric} = require('../../helper/containsCustom')

module.exports = {
  register: [
    body('fullname')
      .exists().withMessage('Nama Lengkap tidak boleh kosong!')
      .not().isEmpty().withMessage('Nama Lengkap harus diisi!'),
    body('email')
      .exists().withMessage('Email tidak boleh kosong!')
      .not().isEmpty().withMessage('Email harus diisi!')
      .isEmail().withMessage('Format email tidak sesuai, silahkan periksa kembali!')
      .custom(val => {
        if (val.includes('+')) return Promise.reject('Email tidak boleh mengandung tanda tambah (+)!')
        return true
      }),
    body('password')
      .exists().withMessage('Password harus diisi')
      .not().isEmpty().withMessage('Password harus diisi!')
      .isLength({ max: 20, min: 8 }).withMessage('Password minimal 8 karakter dan maksimal 20 karakter!')
      .custom(val => {
        if (!hasUppercase(val)) return Promise.reject('Password tidak mengandung huruf kapital!')
        if (!hasLowercase(val)) return Promise.reject('Password tidak mengandung huruf kecil!')
        if (!hasNumeric(val)) return Promise.reject('Password tidak mengandung angka!')
        return true
      })
  ],
  verify: [
    body('key')
      .trim()
      .exists().withMessage('Key tidak boleh kosong!')
      .not().isEmpty().withMessage('Key harus diisi!'),
    body('token')
      .trim()
      .exists().withMessage('Token tidak boleh kosong!')
      .not().isEmpty().withMessage('Token harus diisi!')
  ],
  login: [
    body('email')
      .trim()
      .exists().withMessage('Email tidak boleh kosong!')
      .not().isEmpty().withMessage('Email tidak boleh kosong!')
      .isEmail().withMessage('Format email tidak sesuai, silahkan periksa kembali'),
    body('password')
      .trim()
      .exists().withMessage('Password harus diisi!')
      .not().isEmpty().withMessage('Password harus diisi!')
  ],
  requestForgotPassword: [
    body('email')
      .trim()
      .exists().withMessage('Email tidak boleh kosong!')
      .not().isEmpty().withMessage('Email tidak boleh kosong!')
      .isEmail().withMessage('Format email tidak sesuai, silahkan periksa kembali'),
  ],
  verifyForgotPassword: [
    body('key')
      .trim()
      .exists().withMessage('Key tidak boleh kosong!')
      .not().isEmpty().withMessage('Key harus diisi!'),
    body('token')
      .trim()
      .exists().withMessage('Token tidak boleh kosong!')
      .not().isEmpty().withMessage('Token harus diisi!'),
    body('password')
      .exists().withMessage('Password harus diisi')
      .not().isEmpty().withMessage('Password harus diisi!')
      .isLength({ max: 20, min: 8 }).withMessage('Password minimal 8 karakter dan maksimal 20 karakter!')
      .custom(val => {
        if (!hasUppercase(val)) return Promise.reject('Password tidak mengandung huruf kapital!')
        if (!hasLowercase(val)) return Promise.reject('Password tidak mengandung huruf kecil!')
        if (!hasNumeric(val)) return Promise.reject('Password tidak mengandung angka!')
        return true
      })
  ]
}