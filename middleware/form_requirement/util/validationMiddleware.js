'use strict'

const { keys } = require('lodash')
const { validationResult } = require('express-validator')

const customErrorMessage = {
  register: 'Data yang kami perlukan belum lengkap. Silahkan periksa kembali.'
}

module.exports = function validationMiddleware (validations) {
  const objKeys = keys(validations)
  const obj = {}

  const each = objKeys.map((val) => {
    if (Array.isArray(validations[val])) {
      const rules = validations[val]

      function cek (val) {
        return (req, res, next) => {
          const errors = validationResult(req)
          const errorMessage = customErrorMessage[val]

          if (!errors.isEmpty()) {
            return next({ errorMessage, errorForm: errors.mapped() })
          }
          next()
        }
      }

      rules.push(cek(val))
      return rules
    } else {
      return validationMiddleware(validations[val])
    }
  })

  objKeys.map((val, idx) => {
    obj[val] = each[idx]
  })

  return obj
}
