'use strict'

const ServiceError = require('../handler/wrapper/ServiceError')

const PlatinumOnly = (req, res, next) => {
    const { order: { package: currentPackage } } = req.userdata
    if (currentPackage !== 3) throw new ServiceError('package_not_eligible')

    return next()
}

const GoldAndPlatinumOnly = (req, res, next) => {
    const { order: { package: currentPackage } } = req.userdata
    if (currentPackage < 2) throw new ServiceError('package_not_eligible')

    return next()
}

module.exports = { PlatinumOnly, GoldAndPlatinumOnly }
